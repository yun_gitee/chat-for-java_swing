# chat for java_swing

### 介绍
使用Swing+Socket+Maven+Mybatis开发的一个聊天系统，系统功能有待完善，截至22年7月26日，内部的很多BUG以及不合理的逻辑，都没有处理。TalkingLogin是客户端，TalkingServer是服务器

### 运行
项目导入Idea即可，但是服务器程序，需要配置邮箱信息：邮箱地址以及邮箱授权码。配置文件路径：TalkingServer/src/main/resources/mail-config.properties

### 程序入口
无论是客户端，还是服务器，程序入口都是main包下的Main类.

### 功能实现
邮箱注册、邮箱验证码、邮箱密码找回、私聊、群聊、表情包发送、聊天记录数据库存储以及本地化、聊天消息声音通知、搜索好友、好友添加、添加通知、头像上传、信息修改、修改密码。但是部分功能有问题，BUG并未解决。如果闲的话，那就。。。。

### 界面展示
#### 登录注册
![登录界面](image_show/login.png)
![注册界面](image_show/register.png)
#### 主界面
![主体框架](image_show/main1.png)
![聊天与表情包](image_show/main2.png)
![聊天与好友资料](image_show/main3.png)
![关于界面](image_show/main4.png)
![个人信息](image_show/main5.png)



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
