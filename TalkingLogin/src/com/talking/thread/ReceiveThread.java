package com.talking.thread;

import com.talking.tool.HeadImageToFile;
import com.talking.view.bodyframe.BodyFrame;
import com.talking.view.componentpojo.BodyFrameObj;
import com.talking.pojo.*;
import com.talking.service.SocketService;
import com.talking.tool.MsgFileInput;
import com.talking.tool.MsgFileOutput;
import com.talking.tool.NewMsgDispose;
import com.talking.view.bodyframe.SearchFriendFrame;
import com.talking.view.bodyframe.NoticeFrame;
import com.talking.view.component.JIMSendTextPane;
import com.talking.view.componentpojo.ChatPojo;
import com.talking.view.login.LoginFrame;

import javax.swing.*;
import java.io.ObjectInputStream;
import java.util.Hashtable;
import java.util.List;

/**
 * 用来监听被动消息，例如，
 * 服务器的好友消息转发
 * 好友添加申请
 */
public class ReceiveThread extends Thread{
    private ObjectInputStream in;
    private volatile boolean isRunning;
    Message inMSG;

    public ReceiveThread(){
        isRunning = true;
        in = SocketService.getClientInput();
    }
    public ObjectInputStream getIn(){
        return in;
    }
    public void run(){
        System.out.println("接收线程启动");
        try {
            while (isRunning){
                System.out.println("\n消息监听中---");
                inMSG =(Message) in.readObject();
                System.out.println("监听到请求："+inMSG.getMessageType());

                switch (inMSG.getMessageType()){

                    //消息接收功能
                    case MessageType.CHAT:
                        System.out.println("消息接收功能");
                        ChatMsg chatMsg =(ChatMsg) inMSG.getContent();

                        new MsgFileOutput(chatMsg).input();       //消息写入本地文件
                        new NewMsgDispose(chatMsg);
                        break;

                     //邮箱验证码返回
                    case MessageType.GET_MAIL_CODE:
                        if (inMSG.isResponseType())
                            JOptionPane.showMessageDialog(null, "邮件发送成功,请注意查收！", "邮件提示", JOptionPane.WARNING_MESSAGE);
                        else
                            JOptionPane.showMessageDialog(null, "邮箱发送失败！", "邮件提示", JOptionPane.WARNING_MESSAGE);
                        break;

                    //修改用户资料
                    case MessageType.UPDATE_USER_MSG:
                        System.out.println("接收到返回：用户信息");
                        if (inMSG.isResponseType()){
                            User mine = (User) inMSG.getContent();
                            System.out.println(mine.toString());
                            SystemVariable.setCurrentUser(mine);
                            HeadImageToFile.toFile(mine.getHead_source(),mine.getUser_id(),true);
                            BodyFrameObj.getBodyFrame().repaint();
                            BodyFrameObj.getUserMessFrame().updateContent(mine);
                        }else {
                            JOptionPane.showMessageDialog(null, inMSG.getResponseContent(), "系统提示", JOptionPane.WARNING_MESSAGE);
                        }
                        break;

                     //查找好友
                    case MessageType.SEARCH_FRI:
                        System.out.println("接收到返回：查找的好友信息");
                        SearchFriendFrame frame = BodyFrameObj.getSearchFriendFrame();
                        List<User> users = (List<User>) inMSG.getContent();

                        if (users.size()!=0) {
                            for (User user : users) {
                                System.out.println(user.toString());
                                HeadImageToFile.toFile(user.getHead_source(),user.getUser_id(),true);
                            }
                            frame.showResult(users);

                        }else{
                            frame.showResult(null);
                        }
                        break;


                    case MessageType.GE_OFFLINE_MSG:
                        break;

                    //接收好友申请消息
                    case  MessageType.NOTICE:
                        System.out.println("接收到：好友添加或群邀请信息");
                        Notice notice = (Notice) inMSG.getContent();
                        NoticeFrame noticeFrame = BodyFrameObj.getNoticeFrame();
                        noticeFrame.addBlock(notice);
                        noticeFrame.showFrame();
                        break;

                    //退出
                    case MessageType.OFFLINE:
                        System.out.println("线程监听：退出");
                        if (inMSG.isResponseType()){
                            JOptionPane.showMessageDialog(null, "成功退出系统！", "退出提示", JOptionPane.WARNING_MESSAGE);
                            isRunning = false;
                            System.exit(0);
                        }
                        break;


                    //注销
                    case MessageType.LOGOUT:
                        System.out.println("线程监听：注销");
                        if (inMSG.isResponseType()){
                            BodyFrameObj.getBodyFrame().dispose();
                            new LoginFrame();
                            isRunning = false;
                        }
                        break;

                        //确认添加好友
                    case MessageType.ADD_FRIEND:
                         System.out.println("更新好友列表监听");
                         List<User> list = (List<User>) inMSG.getContent();
                         for (User user1 : list) {
                             System.out.println(user1.toString());
                         }
                         SystemVariable.setFriendList(list);
                         ChatPojo.getLeftPanel().updateFriend();
                         break;

                    //删除功能
                    case MessageType.DELETE_FRI:
                        System.out.println("接收到删除好友消息");
                        List<User> userList = (List<User>) inMSG.getContent();
                        for (User user1 : userList) {
                            System.out.println(user1.toString());
                        }
                        SystemVariable.setFriendList(userList);
                        ChatPojo.getLeftPanel().updateFriend();

                        ChatPojo.getRightCard().show(ChatPojo.getRightPanel(),"0");
                        BodyFrameObj.getFriendMsgWindow().dispose();
                        JOptionPane.showMessageDialog(null, "好友删除成功", "删除提示", JOptionPane.WARNING_MESSAGE);

                        break;

                     //获取群成员
                     case MessageType.GET_GROUP_NUMBERS:
                         System.out.println("接收到群成员");
                         List<User> groupNumbers = (List<User>) inMSG.getContent();
                         Hashtable<Integer,User> temp = new Hashtable<>();

                         for (User g : groupNumbers) {
                             temp.put(g.getUser_id(),g);
                             System.out.println(g.toString());
                             HeadImageToFile.toFile(g.getHead_source(),g.getUser_id(),true);
                         }
                         SystemVariable.setCurrentGroupNumbers(temp);

                         if (!SystemVariable.getCurrentChatObj().isUser()) {
                             Group currentChat = (Group) SystemVariable.getCurrentChatObj().getCurrentChatObj();
                             if (currentChat.getGroup_id()==Integer.valueOf(inMSG.getResponseContent())){
//                                 JIMSendTextPane showPane = ChatPojo.getShowPane();
//                                 showPane.setText("");
                                 new MsgFileInput(Integer.valueOf(inMSG.getResponseContent())).read();
                                 System.out.println("重新更新页面数据"+inMSG.getResponseContent());
                             }
                         }
                         break;

                     //获取建群后的群列表
                    case MessageType.UPDATE_GROUP_LIST:
                        System.out.println("接收到返回：群列表");

                        if (inMSG.isResponseType()){
                            List<Group> groupList = (List<Group>) inMSG.getContent();

                            for (Group group : groupList) {
                                System.out.println(group.toString());
                            }

                            SystemVariable.setGroupList(groupList);
                            ChatPojo.getLeftPanel().updateGroupList();

                           ChatPojo.getRightCard().show(ChatPojo.getRightPanel(),"0");
                           BodyFrameObj.getBodyFrame().repaint();

                        }
                        break;

                    default:
                        System.out.println("其他功能监听到："+inMSG.getMessageType());
                        break;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
