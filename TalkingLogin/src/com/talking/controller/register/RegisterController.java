package com.talking.controller.register;

import com.talking.pojo.Message;
import com.talking.pojo.User;
import com.talking.service.MailService;
import com.talking.service.UserService;
import com.talking.view.componentpojo.LoginFrameObj;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.regex.Pattern;

/**
 * 验证面膜
 * 注册密码
 */
public class RegisterController {
    private UserService userService;

    private MailService mailService;

    public RegisterController(String name,String mail,String mailCode,String pwd){
        userService = new UserService();
        mailService = new MailService();

        boolean nameFlag = name.trim().length()>1&&name.trim().length()<11?true:false;
        boolean codeFlag = Pattern.matches("[0-9]{4}",mailCode);
        boolean mailFlag = Pattern.matches("^(\\w+([-.][A-Za-z0-9]+)*){3,18}@\\w+([-.][A-Za-z0-9]+)*\\.\\w+([-.][A-Za-z0-9]+)*$", mail);
        boolean mailCodeFlag = Pattern.matches("[0-9]{4}",mailCode);
        boolean pwdFlag = Pattern.matches("[A-Za-z0-9]{8,20}",pwd);;




        if (nameFlag&&codeFlag&&mailCodeFlag&&mailFlag&&pwdFlag) {

            boolean temp = this.verifyMailCode(mail, mailCode);

            if (temp) {
                this.register(name, mail, pwd);
            } else {
                JOptionPane.showMessageDialog(null, "验证码错误！", "验证码提示", JOptionPane.WARNING_MESSAGE);
            }

        }else {
            if (!nameFlag)
                JOptionPane.showMessageDialog(null, "昵称长度为2~10", "错误提示：", JOptionPane.WARNING_MESSAGE);
            else if (!mailFlag)
                JOptionPane.showMessageDialog(null, "邮箱输入非法", "错误提示：", JOptionPane.WARNING_MESSAGE);
            else if (!mailCodeFlag)
                JOptionPane.showMessageDialog(null, "验证码输如非法", "错误提示：", JOptionPane.WARNING_MESSAGE);
            else if (!pwdFlag)
                JOptionPane.showMessageDialog(null, "密码仅支持字母与数字，长度为：8~20", "错误提示：", JOptionPane.WARNING_MESSAGE);
        }

    }

    private boolean verifyMailCode(String mail,String code){
        User user = new User();
        user.setUser_mail(mail);
        user.setUser_pwd(code);

        boolean temp = mailService.verifyMailCode(user);
        return temp;
    }

    private void register(String name,String mail,String pwd){
        try {
            User user = new User();
            user.setUser_name(name);
            user.setUser_mail(mail);
            user.setUser_pwd(pwd);


            BufferedImage b1 = ImageIO.read(new File("image/register/user.png"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(b1, "png", baos);  //经测试转换的图片是格式这里就什么格式，否则会失真

            user.setHead_source(baos.toByteArray());

            Message response = userService.register(user);

            if (response.isResponseType()) {
                JOptionPane.showMessageDialog(null, "注册成功！", "注册提示", JOptionPane.WARNING_MESSAGE);
                LoginFrameObj.getCardLayout().show(LoginFrameObj.getContainer(),"0");
            } else {
                JOptionPane.showMessageDialog(null, response.getResponseContent(), "注册提示：", JOptionPane.WARNING_MESSAGE);
            }

        }catch (Exception e){e.printStackTrace();}
    }
}
