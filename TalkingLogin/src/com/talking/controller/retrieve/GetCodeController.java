package com.talking.controller.retrieve;

import com.talking.pojo.Message;
import com.talking.service.MailService;

import javax.swing.*;
import java.util.regex.Pattern;

public class GetCodeController {
    public GetCodeController(String user_mail ){

        MailService mailService = new MailService();

        boolean flag = Pattern.matches("^(\\w+([-.][A-Za-z0-9]+)*){3,18}@\\w+([-.][A-Za-z0-9]+)*\\.\\w+([-.][A-Za-z0-9]+)*$", user_mail);
        if (flag) {
            Message inMSG = mailService.createMailCode(user_mail, "retrieve");
            boolean temp = inMSG.isResponseType();
            if (temp)
                JOptionPane.showMessageDialog(null, inMSG.getResponseContent(), "邮箱提示", JOptionPane.WARNING_MESSAGE);
            else
                JOptionPane.showMessageDialog(null, inMSG.getResponseContent(), "邮箱提示", JOptionPane.WARNING_MESSAGE);
        }else
            JOptionPane.showMessageDialog(null, "邮箱输入非法", "邮箱提示", JOptionPane.WARNING_MESSAGE);

    }
}
