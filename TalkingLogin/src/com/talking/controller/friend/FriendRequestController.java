package com.talking.controller.friend;

import com.talking.pojo.*;
import com.talking.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yun
 * 发起一个好友申请
 */
public class FriendRequestController {
    private UserService userService;

    public FriendRequestController(int id){
        userService = new UserService();
        Notice notice = new Notice();
        User user = new User();
        user.setUser_id(id);
        List<User> users = new ArrayList<>();
        users.add(user);
        notice.setReceiver(users);
        notice.setSender(SystemVariable.getCurrentUser());
        notice.setUserNotice(true);

        Message message = new Message();
        message.setMessageType(MessageType.FRIEND_REQUEST);
        message.setContent(notice);

        userService.friendRequest(message);
    }
}
