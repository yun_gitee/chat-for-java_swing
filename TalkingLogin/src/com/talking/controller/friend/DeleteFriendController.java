package com.talking.controller.friend;

import com.talking.pojo.Notice;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.service.UserService;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DeleteFriendController {
    public DeleteFriendController(){
        User mine = SystemVariable.getCurrentUser();
        User friend = (User) SystemVariable.getCurrentChatObj().getCurrentChatObj();
        int n= JOptionPane.showConfirmDialog(null,"是否删除好友？","删除提示", JOptionPane.YES_NO_OPTION );
        if (n==0) {
            Notice notice = new Notice();
            notice.setSender(mine);
            List<User> temp = new ArrayList<>();
            temp.add(friend);
            notice.setReceiver(temp);

            //发送请求
            new UserService().deleteFriend(notice);

            //删除文件
            File file = new File("log/" + mine.getUser_id() + "/" + friend.getUser_id() + ".log");
            if (file.exists())
                file.delete();
        }
    }
}
