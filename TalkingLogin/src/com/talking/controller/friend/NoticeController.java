package com.talking.controller.friend;

import com.talking.pojo.Group;
import com.talking.pojo.Notice;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.service.GroupService;
import com.talking.service.NoticeService;
import com.talking.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class NoticeController {
    public NoticeController(Notice notice1){

        Notice notice = new Notice();
        User sender = new User();
        sender.setUser_id(SystemVariable.getCurrentUser().getUser_id());
        notice.setSender(sender);
        notice.setUserNotice(notice1.isUserNotice());


        if (notice1.isUserNotice()){
            List<User> list = new ArrayList<>();
            list.add(notice1.getSender());
            notice.setReceiver(list);

        }else {
            notice.setGroup(notice1.getGroup());
        }

        new NoticeService().noticeSend(notice);

    }
}
