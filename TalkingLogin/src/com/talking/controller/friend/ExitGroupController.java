package com.talking.controller.friend;

import com.talking.pojo.*;
import com.talking.service.GroupService;

import javax.swing.*;
import java.io.File;

public class ExitGroupController {
    public ExitGroupController(){
        Group temp = (Group) SystemVariable.getCurrentChatObj().getCurrentChatObj();
        Group group = new Group();
        group.setGroup_id(temp.getGroup_id());

        this.exit(group);

    }
    public ExitGroupController(Group group){
        this.exit(group);
    }
    private void exit(Group group){
        User user = SystemVariable.getCurrentUser();

        int n= JOptionPane.showConfirmDialog(null,"是否退出"+group.getGroup_name(),"群聊退出提示", JOptionPane.YES_NO_OPTION );
        //0为是，1为否
        if (n==0) {
            Message message = new Message();
            Notice notice = new Notice();
            notice.setSender(user);
            notice.setGroup(group);
            message.setMessageType(MessageType.EXIT_GROUP);
            message.setContent(notice);

            new GroupService().exitGroup(message);

            File file = new File("log/group/"+user.getUser_id()+"/"+group.getGroup_id()+".log");
            if (file.exists()){
                file.delete();
            }
        }
    }
}
