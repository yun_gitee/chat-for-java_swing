package com.talking.controller.usermsg;

import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.service.UserService;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class UpdateUserMsgController {
    public UpdateUserMsgController(String name,String sex,String age,String describe,String pwd,String head_source,String mailCode){

        User user = new User();
        user.setUser_name(name);
        user.setUser_sex(sex);
        user.setUser_age(age!=null?Integer.valueOf(age):SystemVariable.getCurrentUser().getUser_age());
        user.setUser_describe(describe);
        user.setUser_mail(SystemVariable.getCurrentUser().getUser_mail());
        user.setUser_id(SystemVariable.getCurrentUser().getUser_id());
//        if (pwd==null)
//            pwd = SystemVariable.getCurrentUser().getUser_pwd();
        user.setUser_pwd(pwd);
        user.setUser_pwd(pwd);
        if (head_source!=null){
            try {
                File file = new File(head_source);
                BufferedImage b1 = ImageIO.read(file);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(b1, "png", baos);  //经测试转换的图片是格式这里就什么格式，否则会失真

//                FileInputStream in = new FileInputStream(head_source);
                user.setHead_source(baos.toByteArray());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        new UserService().updateUserMSG(user,mailCode);
    }
}
