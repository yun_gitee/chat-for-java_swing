package com.talking.controller.usermsg;

import com.talking.pojo.SystemVariable;
import com.talking.service.MailService;

public class GetMailCodeController {
    public GetMailCodeController(){
        MailService mailService = new MailService();
        mailService.createMailCode(SystemVariable.getCurrentUser().getUser_mail(),null);
    }
}
