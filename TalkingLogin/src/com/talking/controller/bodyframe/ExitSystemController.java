package com.talking.controller.bodyframe;

import com.talking.service.SocketService;
import com.talking.service.UserService;

import javax.swing.*;

public class ExitSystemController {
    UserService userService;
    public ExitSystemController(){
        userService = new UserService();
    }
    public void exit(){
        int n= JOptionPane.showConfirmDialog(null,"是否退出系统","系统退出提示", JOptionPane.YES_NO_OPTION );
        //0为是，1为否
        if (n==0) {
            userService.exit();
            System.exit(0);
            SocketService.close();
        }
    }
}
