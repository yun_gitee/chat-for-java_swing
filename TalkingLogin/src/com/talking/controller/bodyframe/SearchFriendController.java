package com.talking.controller.bodyframe;

import com.talking.service.UserService;

public class SearchFriendController {
    private String content;
    private UserService userService;
    public SearchFriendController(String content){
        this.content = content;
        userService = new UserService();
    }
    public void search(){
        userService.searchFriend(content);
    }
}
