package com.talking.controller.bodyframe;

import com.talking.service.UserService;

public class LogoutController {
    UserService userService;

    public LogoutController(){
        userService = new UserService();
    }

    public void logout(){
        userService.logout();
    }
}
