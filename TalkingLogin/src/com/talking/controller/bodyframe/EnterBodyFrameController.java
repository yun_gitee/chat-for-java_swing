package com.talking.controller.bodyframe;

import com.talking.pojo.*;
import com.talking.service.GroupService;
import com.talking.service.RecordService;
import com.talking.service.SocketService;
import com.talking.service.UserService;
import com.talking.tool.HeadImageToFile;
import com.talking.tool.MsgFileInput;
import com.talking.tool.MsgFileOutput;
import com.talking.tool.NewMsgDispose;
import com.talking.view.bodyframe.BodyFrame;
import com.talking.view.component.ISystemTray;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yun
 * 获取好友列表
 * 获得群列表
 * 获得群成员列表
 * 获取最新消息
 * 获取最新通知
 * 加载界面
 * 创建通信线程
 * 实现系统托盘
 */
public class EnterBodyFrameController {
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private UserService userService;
    private GroupService groupService;
    private ISystemTray iSystemTray;

    public EnterBodyFrameController(){
        userService =new UserService();
        groupService = new GroupService();

        this.getAccountList();
        this.loadUi();

        //加载系统托盘
        iSystemTray = new ISystemTray();
        getOffLineChatMSG();
    }
    private void getAccountList(){
        List<User> friendList = userService.getFriendList();
        List<Group> groupList = groupService.getGroupList();

        for (User user : friendList) {
            System.out.println(user.toString());
            HeadImageToFile.toFile(user.getHead_source(),user.getUser_id(),true);
        }

        for (Group group : groupList) {
            System.out.println(group.toString());
        }

        SystemVariable.setFriendList(friendList);
        SystemVariable.setGroupList(groupList);
    }
    private void getOffLineChatMSG(){
        List<Record> offLineMSG = new RecordService().getOffLineRecord();
        if (offLineMSG!=null){
            for (Record record : offLineMSG) {
                ChatMsg chatMsg = new ChatMsg();

                new NewMsgDispose(chatMsg);
                new MsgFileOutput(chatMsg).input();
            }
        }

    }

    private void getOffLineNotice(){
    }
    private void loadUi(){
        new BodyFrame();
    }
}
