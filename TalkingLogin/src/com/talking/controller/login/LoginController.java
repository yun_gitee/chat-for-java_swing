package com.talking.controller.login;

import com.talking.controller.bodyframe.EnterBodyFrameController;
import com.talking.pojo.SystemVariable;
import com.talking.view.componentpojo.LoginFrameObj;
import com.talking.service.UserService;
import com.talking.thread.ReceiveThread;

import javax.swing.*;
import java.util.regex.Pattern;

/**
 * @author yun
 */
public class LoginController {
    private String id;
    private String pwd;

    public LoginController(String id,String pwd){

        this.id = id.trim();
        this.pwd = pwd.trim();

    }
    public void login(){
        boolean pwdFlag = Pattern.matches("[A-Za-z0-9]{8,20}",pwd);

        if (pwdFlag) {

            UserService userService = new UserService();
            System.out.println("账号是：" + id + "，密码是：" + pwd);

            if ((!(id.equals("") || id == null)) && (!(pwd.equals("") || pwd == null))) {
                if (userService.login(id, pwd)) {
                    LoginFrameObj.login_JFrame.dispose();
                    System.out.println("登录成功,进入准备控制");

                    //登录成功，准备进入页面
                    new EnterBodyFrameController();
                    ReceiveThread receiveThread = new ReceiveThread();
                    receiveThread.start();
                    SystemVariable.setReceiveThread(receiveThread);

                } else {
//                socketService.close();
                }
            } else {
                JOptionPane.showMessageDialog(null, "账号或者密码输入非法", "错误提示", JOptionPane.WARNING_MESSAGE);
            }
        }else
            JOptionPane.showMessageDialog(null, "密码仅支持字母与数字，长度为：8~20", "错误提示：", JOptionPane.WARNING_MESSAGE);

    }
}
