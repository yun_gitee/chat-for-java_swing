package com.talking.controller.chat;

import com.talking.pojo.Group;
import com.talking.pojo.SystemVariable;
import com.talking.service.GroupService;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;

public class CreateGroupController {
    public CreateGroupController(String groupName){
        if (groupName.trim().length()==0||groupName==null||groupName.trim().equals("群名称"))
            JOptionPane.showMessageDialog(null, "请输入正确的群名！", "错误提示", JOptionPane.WARNING_MESSAGE);
        else {
            try {
                Group group = new Group();
                group.setCreator_id(SystemVariable.getCurrentUser().getUser_id());
                group.setGroup_name(groupName);

                String[] headImages = new String[]{"g1.png", "g2.png", "g3.png"};

                File file = new File("image/register/" + headImages[(int) (Math.random() * 3)]);

                BufferedImage b1 = ImageIO.read(file);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(b1, "png", baos);  //经测试转换的图片是格式这里就什么格式，否则会失真

//                FileInputStream in = new FileInputStream(head_source);
                group.setHead_source(baos.toByteArray());

                new GroupService().createGroup(group);
            }catch (Exception e){e.printStackTrace();}
        }
    }
}
