package com.talking.controller.chat;

import com.talking.pojo.*;
import com.talking.service.GroupService;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class InviteGroupController {
    public InviteGroupController(Hashtable<Integer,User> inviter){
        Notice sendNotice = new Notice();
        Message outMSG = new Message();

        Set<Integer> keys = inviter.keySet();
        List<User> inviterList = new ArrayList<>();

        for (Integer key : keys) {
            inviterList.add(inviter.get(key));
        }

        sendNotice.setSender(SystemVariable.getCurrentUser());
        sendNotice.setUserNotice(false);
        sendNotice.setGroup((Group) SystemVariable.getCurrentChatObj().getCurrentChatObj());
        sendNotice.setReceiver(inviterList);

        outMSG.setMessageType(MessageType.INVITE_NUMS);
        outMSG.setContent(sendNotice);

        boolean flag = new GroupService().inviteNums(outMSG);

        if (flag)
            JOptionPane.showMessageDialog(null, "邀请成功！", "邀请提示", JOptionPane.WARNING_MESSAGE);
        else
            JOptionPane.showMessageDialog(null, "邀请失败！", "邀请提示", JOptionPane.WARNING_MESSAGE);

    }
}
