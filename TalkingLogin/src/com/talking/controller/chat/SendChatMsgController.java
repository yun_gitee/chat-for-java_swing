package com.talking.controller.chat;

import com.talking.pojo.ChatMsg;
import com.talking.pojo.Group;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.service.UserService;
import com.talking.view.chat.LeftPanel;
import com.talking.view.component.BlockPanel;
import com.talking.view.component.JIMSendTextPane;
import com.talking.view.componentpojo.ChatPojo;
import javafx.scene.control.ScrollBar;

import javax.swing.*;
import java.sql.Timestamp;

/**
 * 此类用于发送消息
 */
public class SendChatMsgController {

    public SendChatMsgController(String content, JIMSendTextPane sendPane, Object blockPanel){

        if (content.trim().length()==0||content==null){
//            JOptionPane.showMessageDialog(null, "好友删除成功", "错误提示", JOptionPane.WARNING_MESSAGE);

        }else {

            //消息类
            ChatMsg chatMsg = new ChatMsg();
            boolean isUser = SystemVariable.getCurrentChatObj().isUser();        //当前聊天对象是个人，还是群,同时作为群消息，或者个人消息
            Timestamp time = new Timestamp(System.currentTimeMillis());         //时间
            User sender = new User();                                           //发送方
            Object receiver = new Object();                                     //接收方

            User mine = SystemVariable.getCurrentUser();
            sender.setUser_id(mine.getUser_id());
            sender.setUser_name(mine.getUser_name());
            sender.setHead_source(mine.getHead_source());

            //获得朋友的ID，或者群聊的id
            receiver = SystemVariable.getCurrentChatObj().getCurrentChatObj();

            //设置消息内容
            chatMsg.setSignChat(isUser);
            chatMsg.setSendTime(time.toString());
            chatMsg.setSender(sender);
            chatMsg.setReceiver(receiver);
            chatMsg.setContent(content);

            new UserService().chat(chatMsg);

            //设置显示
            sendPane.setText("");
//            JScrollBar bar = ChatPojo.getMsgJScroll().getVerticalScrollBar();
//            int value = bar.getMaximum();
//            System.out.println("这发送个值是："+value);
//            bar.setValue(value);


            if (isUser){
                ((LeftPanel.FriendPanel)blockPanel).messLabel.setText("我:"+content);
            }else {
                    ((LeftPanel.GroupPanel)blockPanel).groupTips.setText("我:"+content);
            }
            System.out.println("这个东西时"+content+"dfd");
        }
    }
}
