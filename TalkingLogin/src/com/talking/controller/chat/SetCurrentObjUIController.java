package com.talking.controller.chat;

import com.talking.pojo.CurrentChatObj;
import com.talking.pojo.Group;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.service.GroupService;
import com.talking.view.chat.ChatPanel;
import com.talking.view.chat.RightPanel;
import com.talking.view.component.JIMSendTextPane;
import com.talking.view.componentpojo.ChatPojo;

import javax.swing.*;
import java.util.Hashtable;
import java.util.List;

public class SetCurrentObjUIController {
    private RightPanel.TopPanel topPanel;            //顶部名字面板
    private JPanel msgPane;                     //消息显示面板
    private JIMSendTextPane sendMSG;            //消息发送框
    private GroupService groupService;                  //群service

    //ChatPanel.UserNamePanel topPanel, JIMSendTextPane msgShow,JIMSendTextPane jimSendTextPane
    public SetCurrentObjUIController(){
        this.topPanel = ChatPojo.getTopPanel();
        this.msgPane = ChatPojo.getMsgShow();
        this.sendMSG = ChatPojo.getSendPane();
        groupService = new GroupService();
    }

    public void start(){
        CurrentChatObj currentChatObj = SystemVariable.getCurrentChatObj();
        if (currentChatObj.isUser())
            topPanel.setUserName(((User)currentChatObj.getCurrentChatObj()).getUser_name());
        else if (!currentChatObj.isUser()) {
            topPanel.setUserName(((Group) currentChatObj.getCurrentChatObj()).getGroup_name());

            Group group = (Group) SystemVariable.getCurrentChatObj().getCurrentChatObj();
            this.groupService.getGroupNumbers(group);
        }
        sendMSG.setText("");
        msgPane.removeAll();
        msgPane.updateUI();
    }
}
