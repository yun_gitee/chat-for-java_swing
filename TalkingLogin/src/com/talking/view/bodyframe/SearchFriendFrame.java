package com.talking.view.bodyframe;

import com.talking.controller.bodyframe.SearchFriendController;
import com.talking.view.component.*;
import com.talking.view.componentpojo.ColorObj;
import com.talking.pojo.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

public class SearchFriendFrame extends JFrame{
    private JPanel jPanelContent;          //内容面板

    private SearchPanel searchPanel;        //搜索框

    public JPanel resultContainer;           //账户内容面板用来承载提示、加载、用户面板
    public CardLayout cardLayout;

    private JLabel tipsLabel;               //提示标签面板
    private JPanel resultPanel;             //结果面板
    private JLabel loadingLabel;            //加载标签面板
    private JLabel noResultLabel;           //无结果面板

    private final int width = 600;          //窗体宽
    private final int height =560;          //窗体高
    private final int gap = 20;             //组件之间的间隙

    public SearchFriendFrame(){

        jPanelContent = new JPanel();
        searchPanel = new SearchPanel("账号/邮箱/昵称",width-50,50,new ShowFriendList(),ColorObj.buttonReleasedColor);
        resultContainer = new JPanel();

        cardLayout = new CardLayout();
        tipsLabel = new JLabel(new ImageIcon("image/bodyframe/add/tips.png"));
        resultPanel = new JPanel();
        noResultLabel = new JLabel(new ImageIcon("image/bodyframe/add/noResult.png"));
        loadingLabel = new JLabel();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setBounds((int)(screenSize.getWidth()-width)/2,180,width+10,height+40);
        this.setFocusable(true);
        this.setAlwaysOnTop(true);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("image/bodyframe/add2.png"));
        this.setResizable(false);
        this.setTitle("账号搜索");
        jPanelContent.setBackground(ColorObj.MAIN_BACKGROUND);
        jPanelContent.setLayout(null);
        resultPanel.setBackground(ColorObj.MAIN_BACKGROUND);

        searchPanel.setLocation(25,13);

        resultContainer.setBounds(25,80,width-50,height-120-gap);
        resultContainer.setBackground(ColorObj.MAIN_BACKGROUND);
        resultContainer.setLayout(cardLayout);
        resultContainer.add(tipsLabel,"0");
        resultContainer.add(resultPanel,"1");
        resultContainer.add(noResultLabel,"2");

        jPanelContent.add(searchPanel);
        jPanelContent.add(resultContainer);
        this.add(jPanelContent);

        this.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                dispose();
            }
        });
    }
    public class AccountCard extends JLabel{
        public void paintComponent(Graphics g){
            g.setColor(ColorObj.LIGHT_WHITE2);
            g.drawLine(20,2,this.getWidth()-20,2);
        }
    }
    public void showResult(List<User> friendList){
        resultPanel.removeAll();

        if (friendList!=null){
            int count = friendList.size();
            int row = count/4;
            int countSpace = 12;
            row++;

            resultPanel.setLayout(new GridLayout(3,4));
            if (row>3){
                resultPanel.setLayout(new GridLayout(row,4));
                countSpace = row*4;
            }
            for (User user : friendList) {
                UserBlock userBlock = new UserBlock(130,user,40,18);
                resultPanel.add(userBlock);
            }
            for (int i = 0; i < countSpace-friendList.size(); i++) {
                JLabel jLabel = new JLabel("");
                jLabel.setPreferredSize(new Dimension(130,130));
                resultPanel.add(jLabel);
            }
            cardLayout.show(resultContainer,"1");
        }else {
            System.out.println("无结果");
            cardLayout.show(resultContainer,"2");
        }
        resultPanel.updateUI();
    }
    public void showFrame(){
        this.dispose();
        cardLayout.show(resultContainer,"0");
        searchPanel.setContent("");
        this.setVisible(true);

    }

    private class ShowFriendList extends MouseAdapter{
        public void mousePressed(MouseEvent e) {
            JLabel source =(JLabel) e.getComponent();
        }

        public void mouseReleased(MouseEvent e) {
            new SearchFriendController(searchPanel.jTextField.getText()).search();        }
    }
}
