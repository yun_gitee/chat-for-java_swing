package com.talking.view.bodyframe;


import com.talking.controller.friend.NoticeController;
import com.talking.pojo.Group;
import com.talking.pojo.Notice;
import com.talking.pojo.User;
import com.talking.service.NoticeService;
import com.talking.view.component.*;
import com.talking.view.componentpojo.ColorObj;
import com.talking.view.componentpojo.JScrollPaneConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class NoticeFrame extends JFrame{
    private JScrollPane jScrollPane;        //滚动面板
    private JPanel jPanel;          //窗体的容器面板，为了能够设置背景颜色
    private BlockPanel tempBlock;           //空白面板

    private final int width = 700;
    private final int height =380;
    private List<NoticeBlock> noticeBlocks;

    public NoticeFrame(){

        jPanel = new JPanel();
        jScrollPane = new JScrollPane(jPanel);
        tempBlock = new BlockPanel("暂无任何系统通知！");
        noticeBlocks = new ArrayList<>();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setTitle("消息通知面板");
        this.setBounds((int)(screenSize.getWidth()-width)/2,150,width,height);
        this.setMinimumSize(new Dimension(700,405));
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("image/bodyframe/notice2.png"));
        this.add(jScrollPane);

        jPanel.setLayout(new VerticalFlowLayout());
        jPanel.setBackground(ColorObj.MAIN_BACKGROUND);
        JScrollPaneConfig.setUI(jScrollPane);


        this.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                dispose();
            }
        });

    }
    public void addBlock(Notice notice){
        NoticeBlock noticeBlock = new NoticeBlock(notice);


        noticeBlocks.add(new NoticeBlock());
        for (int i = noticeBlocks.size()-1; i >0 ; i--) {
            noticeBlocks.set(i, noticeBlocks.get(i - 1));
        }
        noticeBlocks.set(0,noticeBlock);

        jPanel.add(noticeBlock);
    }

    public void showFrame(){
        this.dispose();

        if (noticeBlocks.size()==0){
            jPanel.add(tempBlock);
            System.out.println("添加成功！");
        }else{
            jPanel.removeAll();
            for (NoticeBlock noticeBlock : noticeBlocks) {
                jPanel.add(noticeBlock);
            }
        }

        this.setVisible(true);
    }

    private class NoticeBlock extends BlockPanel {
        private SpringLayout springLayout;      //边沿布局
        private JLabel headImage;       //头像
        private JLabel name;            //名字
        private JLabel tips;            //消息
        private IButton receiveButton;      //接收按钮
        private IButton refuseButton;      //拒绝按钮
        private JLabel beRefusedLabel;          //已拒绝

        public NoticeBlock(){}

        public NoticeBlock(Notice notice){
            springLayout = new SpringLayout();
            User sender = notice.getSender();
            Group group = notice.getGroup();

            ImageIcon imageIcon = new ImageIcon("C:\\Users\\Administrator\\Desktop\\OIP-C.Iqr6Q1H_7dQV7mPywNMhhAAAAA.jpg");
            imageIcon.setImage(imageIcon.getImage().getScaledInstance(60, 60,Image.SCALE_DEFAULT ));
            headImage = new JLabel(imageIcon);

            name = new JLabel(sender.getUser_name());
            tips = new JLabel("\""+sender.getUser_name()+"\""+(notice.isUserNotice()?"申请添加好友":"邀请您加入群聊："+group.getGroup_name()));
            receiveButton = new IButton("接收",ColorObj.mainThemeColor,true);
            refuseButton = new IButton("拒绝",ColorObj.TEXT_TIPS,true);
            beRefusedLabel = new JLabel("已拒绝");

            this.setAllColor(ColorObj.LIGHT_WHITE2,ColorObj.LIGHT_WHITE2,ColorObj.LIGHT_WHITE2);
            this.setPreferredSize(new Dimension(1,100));
            this.setLayout(springLayout);

            headImage.setSize(60,60);
            name.setPreferredSize(new Dimension(400,40));
            name.setFont(new Font("宋体",Font.PLAIN,18));
            name.setForeground(ColorObj.LIGHT_WHITE5);

            tips.setFont(new Font("宋体",Font.PLAIN,14));
            tips.setForeground(ColorObj.LIGHT_WHITE5);

            beRefusedLabel.setFont(new Font("宋体",Font.PLAIN,16));
            beRefusedLabel.setForeground(ColorObj.LIGHT_WHITE5);

            refuseButton.setPreferredSize(new Dimension(60,30));
            receiveButton.setPreferredSize(new Dimension(60,30));

            springLayout.putConstraint(SpringLayout.NORTH,headImage,10,SpringLayout.NORTH,this);
            springLayout.putConstraint(SpringLayout.WEST,headImage,10,SpringLayout.WEST,this);

            springLayout.putConstraint(SpringLayout.NORTH,name,10,SpringLayout.NORTH,this);
            springLayout.putConstraint(SpringLayout.WEST,name,75,SpringLayout.WEST,this);

            springLayout.putConstraint(SpringLayout.NORTH,tips,(int)name.getPreferredSize().getHeight(),SpringLayout.NORTH,name);
            springLayout.putConstraint(SpringLayout.WEST,tips,75,SpringLayout.WEST,this);
            springLayout.putConstraint(SpringLayout.EAST,tips,-100,SpringLayout.EAST,this);

            springLayout.putConstraint(SpringLayout.NORTH,receiveButton,15,SpringLayout.NORTH,this);
            springLayout.putConstraint(SpringLayout.EAST,receiveButton,-20,SpringLayout.EAST,this);

            springLayout.putConstraint(SpringLayout.SOUTH,refuseButton,-15,SpringLayout.SOUTH,this);
            springLayout.putConstraint(SpringLayout.EAST,refuseButton,-20,SpringLayout.EAST,this);

            springLayout.putConstraint(SpringLayout.NORTH,beRefusedLabel,45,SpringLayout.NORTH,this);
            springLayout.putConstraint(SpringLayout.EAST,beRefusedLabel,-40,SpringLayout.EAST,this);

            this.add(headImage);
            this.add(name);
            this.add(tips);
            this.add(receiveButton);
            this.add(refuseButton);

            receiveButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    new NoticeController(notice);

                    NoticeBlock.this.remove(receiveButton);
                    NoticeBlock.this.remove(refuseButton);

                    beRefusedLabel.setText("已接收");
                    NoticeBlock.this.add(beRefusedLabel);
                    NoticeBlock.this.updateUI();
                }
            });
            refuseButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    NoticeBlock.this.remove(receiveButton);
                    NoticeBlock.this.remove(refuseButton);
                    NoticeBlock.this.add(beRefusedLabel);
                    NoticeBlock.this.updateUI();
                }
            });
        }
    }
}
