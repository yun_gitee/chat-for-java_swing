package com.talking.view.bodyframe;

import com.talking.controller.bodyframe.LogoutController;
import com.talking.controller.bodyframe.ExitSystemController;
import com.talking.view.component.BlockPanel;
import com.talking.view.component.ShadowBorder;
import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;


public class SettingWindow extends JWindow {
    private JLabel jLabelShadow;           //阴影面板
    private JPanel jPanelContent;          //内容面板
    private SpringLayout spring;            //边距布局

    private BlockPanel settingButton;
    private BlockPanel logOutButton;
    private BlockPanel exitButton;

    private SettingFrame settingFrame;

    private List<JPanel> buttonList;
    public SettingWindow(JFrame jFrame){
        super(jFrame);
        jLabelShadow = new JLabel();
        jPanelContent = new JPanel();
        spring = new SpringLayout();

        settingButton = new BlockPanel("设置");
        logOutButton = new BlockPanel("注销");
        exitButton = new BlockPanel("退出");

        settingFrame = new SettingFrame();
        buttonList = new ArrayList();


        this.setSize(new Dimension(150,180));
        this.setBackground(new Color(0,0,0,0));
        this.setFocusable(true);

        jLabelShadow.setBorder(new ShadowBorder(20));
        jLabelShadow.setLayout(null);
        jPanelContent.setBounds(23,23,106,134);
        jPanelContent.setBackground(ColorObj.MAIN_BACKGROUND);
        jPanelContent.setLayout(spring);

        this.addComponent();

        jLabelShadow.add(jPanelContent);

        this.add(jLabelShadow);
        settingButton.addMouseListener(new SettingListener(0));
        logOutButton.addMouseListener(new SettingListener(1));
        exitButton.addMouseListener(new SettingListener(2));

        this.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                jPanelContent.removeAll();
                addComponent();
                dispose();
            }
        });
    }

    private void addComponent(){
        this.setSpring(settingButton,jPanelContent,10,jPanelContent,10,jPanelContent,-10);
        settingButton.setAllColor(ColorObj.GENERIC3,ColorObj.lightThemeColor,ColorObj.MAIN_BACKGROUND);
        this.setSpring(logOutButton,settingButton,40,jPanelContent,10,jPanelContent,-10);
        logOutButton.setAllColor(ColorObj.GENERIC3,ColorObj.lightThemeColor,ColorObj.MAIN_BACKGROUND);
        this.setSpring(exitButton,logOutButton,40,jPanelContent,10,jPanelContent,-10);
        exitButton.setAllColor(ColorObj.GENERIC3,ColorObj.lightThemeColor,ColorObj.MAIN_BACKGROUND);

        jPanelContent.add(settingButton);
        jPanelContent.add(logOutButton);
        jPanelContent.add(exitButton);
    }

    private void setSpring(JComponent currentComponent,JComponent north,int northPad,JComponent west,int westPad,JComponent east,int eastPad){
        spring.putConstraint(SpringLayout.NORTH,currentComponent,northPad,SpringLayout.NORTH,north);
        spring.putConstraint(SpringLayout.WEST,currentComponent,westPad,SpringLayout.WEST,west);
        spring.putConstraint(SpringLayout.EAST,currentComponent,eastPad,SpringLayout.EAST,east);
    }

    private class SettingListener extends MouseAdapter {
        private int num;
        public SettingListener(int num){
            this.num = num;
        }
        @Override
        public void mouseClicked(MouseEvent e) {
            if (num == 0){
                settingFrame.showFrame();
            }else if (num == 1) {
                    new LogoutController().logout();
            }else if (num==2)
                    new ExitSystemController().exit();
        }
    }
}
