package com.talking.view.bodyframe;

import com.talking.view.componentpojo.ColorObj;
import com.talking.view.component.BlockPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SettingFrame {
    private JFrame jFrame;

    private LeftPanel leftPanel;           //左边导航栏
    private JPanel rightPanel;          //右边内容面板容器
    private CardLayout rightCard;       //右边的卡片布局
    private AboutPanel aboutPanel;          //软件信息页面
    private SettingPanel settingPanel;      //设置面板


    private BlockPanel aboutBlock;           //关于按钮
    private BlockPanel settingBlock;             //设置按钮

    private final int width= 600;
    private final int height= 500;
    private final int leftWidth = 150;
    private int chooseNum = 0;              //哪一个按钮被点击了

    public SettingFrame(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        jFrame = new JFrame("设置中心");
        leftPanel = new LeftPanel();
        rightPanel = new JPanel();
        rightCard = new CardLayout();
        aboutPanel = new AboutPanel();
        settingPanel = new SettingPanel();

        jFrame.setIconImage(new ImageIcon("image/bodyframe/setting2.png").getImage());
        jFrame.setResizable(false);
        jFrame.setBounds((int)(screenSize.getWidth()-width)/2,150,width,height);
        jFrame.setDefaultCloseOperation(jFrame.DISPOSE_ON_CLOSE);

        rightPanel.setPreferredSize(new Dimension(width-leftWidth,1));
        rightPanel.setBackground(ColorObj.MAIN_BACKGROUND);
        rightPanel.setLayout(rightCard);

        rightPanel.add(aboutPanel,"0");
        rightPanel.add(settingPanel,"1");

        jFrame.add(leftPanel,BorderLayout.WEST);
        jFrame.add(rightPanel,BorderLayout.CENTER);

    }

    public void showFrame(){
        this.jFrame.dispose();
        this.jFrame.setVisible(true);

    }

    private class LeftPanel extends JPanel{
        private SpringLayout leftSpring;

        public LeftPanel(){
            leftSpring = new SpringLayout();
            aboutBlock = new BlockPanel("关于");
            settingBlock = new BlockPanel("设置");


            this.setPreferredSize(new Dimension(leftWidth,1));
            this.setBackground(ColorObj.MAIN_BACKGROUND);
            this.setLayout(leftSpring);

            this.setSpring(aboutBlock,this,20,this,20,this,-20);
            aboutBlock.setAllColor(ColorObj.GENERIC3,ColorObj.lightThemeColor,ColorObj.splitLineColor);
            aboutBlock.beClicked(true);
            this.setSpring(settingBlock,aboutBlock,50,this,20,this,-20);
            settingBlock.setAllColor(ColorObj.GENERIC3,ColorObj.lightThemeColor,ColorObj.splitLineColor);

            this.add(aboutBlock);
            this.add(settingBlock);

            aboutBlock.addMouseListener(new ClickAdapter());
            settingBlock.addMouseListener(new ClickAdapter());
        }
        private void setSpring(JComponent currentComponent,JComponent north,int northPad,JComponent west,int westPad,JComponent east,int eastPad){
            leftSpring.putConstraint(SpringLayout.NORTH,currentComponent,northPad,SpringLayout.NORTH,north);
            leftSpring.putConstraint(SpringLayout.WEST,currentComponent,westPad,SpringLayout.WEST,west);
            leftSpring.putConstraint(SpringLayout.EAST,currentComponent,eastPad,SpringLayout.EAST,east);
        }
    }

    private class AboutPanel extends JPanel{
        private JLabel logo;
        private JLabel name;
        private JLabel version;
        private JLabel os;
        private JLabel author;
        private JTextArea describe;

        private SpringLayout aboutSpring;
        private final int headSize = 120;

        public AboutPanel(){
            logo  = new JLabel(new ImageIcon("image/logo/logo120x120.png"));
            name = new JLabel("讯·笺");
            version = new JLabel("版本：1.0 beta");
            os = new JLabel("操作系统："+System.getProperty("os.name").toLowerCase());
            author = new JLabel("开发人员：云无阳");
            describe = new JTextArea("SAME LETTER是一款基于Swing+Socket+Maven+Mybatis的即时聊天软件，系统开发的主要目的是因为无聊，值得一提的是，本系统的很多功能是不完整的，并且有很多BUG没有解决。");
            aboutSpring = new SpringLayout();


            this.setStyle(name,35);
            this.setStyle(version,14);
            this.setStyle(os,14);
            this.setStyle(describe,25);
            this.setStyle(author,14);

            this.setSpring(logo,this,20,this,20);
            this.setSpring(name,this,30,logo,headSize+20);
            this.setSpring(version,name,50,logo,headSize+20);
            this.setSpring(os,version,30,logo,headSize+20);
            this.setSpring(describe,os,60,this,20);
            aboutSpring.putConstraint(SpringLayout.SOUTH,author,-20,SpringLayout.SOUTH,this);
            aboutSpring.putConstraint(SpringLayout.EAST,author,-20,SpringLayout.EAST,this);

            aboutSpring.putConstraint(SpringLayout.EAST,describe,-40,SpringLayout.EAST,this);
            aboutSpring.putConstraint(SpringLayout.SOUTH,describe,-40,SpringLayout.SOUTH,this);

            describe.setBorder(null);
            describe.setBackground(ColorObj.leftPanelColor);
            describe.setLineWrap(true);
            describe.setEditable(false);

            this.setBackground(ColorObj.leftPanelColor);
            this.setLayout(aboutSpring);
            this.addComponent(logo,name,version,os,author,describe);
        }

        private void setStyle(JComponent component,int size){
            component.setFont(new Font("微软雅黑", Font.PLAIN,size));
            component.setForeground(new Color(139, 139, 139));
        }

        private void setSpring(JComponent currentComponent,JComponent north,int northPad,JComponent west,int westPad){
            aboutSpring.putConstraint(SpringLayout.NORTH,currentComponent,northPad,SpringLayout.NORTH,north);
            aboutSpring.putConstraint(SpringLayout.WEST,currentComponent,westPad,SpringLayout.WEST,west);
        }

        private void addComponent(JComponent... components){
            for (JComponent component : components) {
                this.add(component);
            }
        }
    }

    private class SettingPanel extends JPanel{
        public SettingPanel(){

        }
    }

    private class ClickAdapter extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getComponent()==aboutBlock){
                chooseNum = 0;
                rightCard.show(rightPanel,"0");
                aboutBlock.beClicked(true);
                settingBlock.beClicked(false);
            }else if (e.getComponent()==settingBlock){
                chooseNum = 1;
                rightCard.show(rightPanel,"1");
                aboutBlock.beClicked(false);
                settingBlock.beClicked(true);
            }
        }
    }
}
