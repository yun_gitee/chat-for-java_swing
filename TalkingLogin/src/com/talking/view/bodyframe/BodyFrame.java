package com.talking.view.bodyframe;

import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.view.chat.ChatPanel;
import com.talking.view.componentpojo.ColorObj;
import com.talking.view.componentpojo.BodyFrameObj;
import com.talking.view.component.HeadImage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

/*
* 软件主体的框架
*
* */
public class BodyFrame extends JFrame {
    private JPanel userPanel_JPanel;        //导航面板
    private JLabel topUserPanel;            //用户顶部面板
    private JLabel bottomUserPanel;         //用户底部面板
    private JPanel cardLayoutPanel_JPanel;      //卡片布局的容器
    private CardLayout body_CardLayout;      //卡片布局管理器
    private HeadImageLabel headImageLabel;
    private PanelButton chatButton_JLabel;             //聊天界面按钮
    private PanelButton worldButton_JLabel;             //世界界面按钮
    private PanelButton otherButton_JLabel;             //其他界面按钮
    private FunctionButton addAccount_JLabel;              //添加账户按钮
    private FunctionButton notice_JLabel;               //消息通知按钮
    private FunctionButton setting_JLabel;              //设置按钮
    private SearchFriendFrame searchFriendFrame;              //添加弹窗
    private NoticeFrame noticeFrame;                    //消息弹窗
    private SettingWindow settingWindow;                //设置弹窗
    private UserMsgFrame userMsgFrame;                  //个人信息面板
    private List<PanelButton> panelButtonList;           //界面按钮集合
    private ChatPanel chatPanel;                        //聊天面板


    private int whilePanelButton=0;                       //哪一个按钮
    private final int userPanelWidth = 80;         //用户面板宽度
    private final int width = 1000;                     //最小宽度
    private final int height = 600;                     //最小高度

    private User mine = SystemVariable.getCurrentUser();
//    SystemTrayListener systemTrayListener;



    public BodyFrame(){
        //窗体设置
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        this.setMinimumSize(new Dimension(width,height));
        this.setLocation((int)(screenSize.getWidth()-width)/2,100);
        this.setBackground(Color.black);
        this.setTitle(mine.getUser_name()+"("+mine.getUser_id()+")");
        this.setIconImage(new ImageIcon(this.getClass().getResource("/logo/logo120x120.png")).getImage());

        if (SystemTray.isSupported())
            this.setDefaultCloseOperation(this.HIDE_ON_CLOSE);
        else
            this.setDefaultCloseOperation(this.DO_NOTHING_ON_CLOSE);

        //用户面板设置
        userPanel_JPanel = new JPanel();
        topUserPanel = new JLabel();
        bottomUserPanel = new JLabel();
        userPanel_JPanel.setLayout(new BorderLayout());
        userPanel_JPanel.setPreferredSize(new Dimension(userPanelWidth,1));
        userPanel_JPanel.setBackground(ColorObj.LIGHT_WHITE7);
        topUserPanel.setPreferredSize(new Dimension(userPanelWidth,350));
        topUserPanel.setLayout(null);
        bottomUserPanel.setPreferredSize(new Dimension(userPanelWidth,180));
        bottomUserPanel.setLayout(new GridLayout(3,1));
        userPanel_JPanel.add(topUserPanel,BorderLayout.NORTH);
        userPanel_JPanel.add(new JLabel(),BorderLayout.CENTER);
        userPanel_JPanel.add(bottomUserPanel,BorderLayout.SOUTH);
        this.add(userPanel_JPanel,BorderLayout.WEST);

        //卡片布局面板设置
        cardLayoutPanel_JPanel = new JPanel();
        body_CardLayout = new CardLayout();
        cardLayoutPanel_JPanel.setBounds(userPanelWidth,0,this.getWidth()-userPanelWidth,this.getHeight());
        cardLayoutPanel_JPanel.setBackground(Color.green);
        cardLayoutPanel_JPanel.setLayout(body_CardLayout);
        this.add(cardLayoutPanel_JPanel,BorderLayout.CENTER);

        //用户面板的头像、点击切换按钮,聊天，世界
        headImageLabel = new HeadImageLabel();
        chatButton_JLabel = new PanelButton(0);
        worldButton_JLabel = new PanelButton(1);
        otherButton_JLabel = new PanelButton(2);
        panelButtonList = new ArrayList();
        userMsgFrame = new UserMsgFrame();

        topUserPanel.add(headImageLabel);
        topUserPanel.add(chatButton_JLabel);
//        topUserPanel.add(worldButton_JLabel);
//        topUserPanel.add(otherButton_JLabel);


        //功能点击按钮，设置
        addAccount_JLabel = new FunctionButton(0);
        searchFriendFrame = new SearchFriendFrame();
        notice_JLabel = new FunctionButton(1);
        noticeFrame = new NoticeFrame();
        setting_JLabel = new FunctionButton(2);
        settingWindow = new SettingWindow(this);
        bottomUserPanel.add(addAccount_JLabel);
        bottomUserPanel.add(notice_JLabel);
        bottomUserPanel.add(setting_JLabel);



        BodyFrameObj.setBodyFrame(this);
        BodyFrameObj.setUserMessFrame(userMsgFrame);

        chatPanel = new ChatPanel();

        cardLayoutPanel_JPanel.add(chatPanel,"0");
        BodyFrameObj.setSettingWindow(settingWindow);
        BodyFrameObj.setSearchFriendFrame(searchFriendFrame);
        BodyFrameObj.setNoticeFrame(noticeFrame);

        this.addComponentListener(new UIResized());
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                BodyFrameObj.setBodyFrame(BodyFrame.this);
                System.out.println("这是拖拽");
            }
        });

        //窗体的标题栏没有拖动监听
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentMoved(ComponentEvent e) {
                BodyFrameObj.reLocate();
            }
        });
//        this.addWindowListener(new WindowAdapter() {
//            @Override
//            public void windowClosing(WindowEvent e) {
//                new ExitSystemController().exit();
//            }
//        });
//        this.addWindowListener(new ISystemTray());

        this.setVisible(true);


    }
    private class HeadImageLabel extends JLabel{
        private HeadImage headImage;
        public HeadImageLabel(){
            headImage = new HeadImage(60,"u"+mine.getUser_id(),new UserAdapter());

            headImage.setLocation(10,20);

            this.setBounds(0,0,userPanelWidth,108);
            this.setLayout(null);
            this.add(headImage);
        }
        public void paintComponent(Graphics g){
            super.paintComponent(g);
            g.setColor(Color.white);
            g.drawLine(8,100,72,100);
            g.drawLine(8,101,72,101);
        }

        private class UserAdapter extends MouseAdapter{
            @Override
            public void mouseClicked(MouseEvent e) {
                userMsgFrame.showFrame();
            }
        }
    }

    //功能按钮，例如设置，添加，消息
    private class FunctionButton extends JLabel{
        private JLabel jLabel;
        private String[] image = {"add1.png","add2.png","notice1.png","notice2.png","setting1.png","setting2.png"};
        public FunctionButton(int num){
            jLabel = new JLabel(new ImageIcon(this.getClass().getResource("/bodyframe/"+image[num*2])));

            jLabel.setBounds(userPanelWidth/2-17,10,35,35);
            this.setBounds(0,110+num*60,userPanelWidth,55);
            this.add(jLabel);
            this.addMouseListener(new PopWindowListener(num,jLabel));
        }

    }
    //主要是用来聊天、朋友圈、群组i
    private class PanelButton extends JLabel{
        private int num;
        String imagePath = "/bodyframe/";
        String[] images = {"mess1.png","mess2.png","match1.png","match2.png","navigation1.png","navigation2.png"};
        private JLabel imageLabel;
        public PanelButton(int num){
            imageLabel = new JLabel();
            imageLabel.setBounds(userPanelWidth/2-17,10,35,35);

            this.num = num;
            this.setBounds(0,128+num*60,userPanelWidth,55);
            this.flashButton();
            this.add(imageLabel);
            this.addMouseListener(new PanelButtonMouse(num));
        }
        public void flashButton(){
            if (whilePanelButton==this.num)
                imageLabel.setIcon(new ImageIcon(this.getClass().getResource(imagePath+images[num*2+1])));
            else
                imageLabel.setIcon(new ImageIcon(this.getClass().getResource(imagePath+images[num*2])));
            this.repaint();
        }

    }
    //弹窗事件
    private class PopWindowListener extends MouseAdapter{
        private int num;
        private JLabel jLabel;
        public PopWindowListener(int num,JLabel jLabel){
            this.num = num;
            this.jLabel = jLabel;
        }

        public void mousePressed(MouseEvent e){
            switch (num){
                case 0:
                    jLabel.setIcon(new ImageIcon(this.getClass().getResource("/bodyframe/add2.png")));
                    break;
                case 1:
                    jLabel.setIcon(new ImageIcon(this.getClass().getResource("/bodyframe/notice2.png")));
                    break;
                case 2:
                    jLabel.setIcon(new ImageIcon(this.getClass().getResource("/bodyframe/setting2.png")));
                    break;
            }
        }
        public void mouseReleased(MouseEvent e){
            switch (num){
                case 0:
                    jLabel.setIcon(new ImageIcon(this.getClass().getResource("/bodyframe/add1.png")));
                    searchFriendFrame.showFrame();
                    break;
                case 1:
                    jLabel.setIcon(new ImageIcon(this.getClass().getResource("/bodyframe/notice1.png")));
                    noticeFrame.showFrame();
                    break;
                case 2:
                    jLabel.setIcon(new ImageIcon(this.getClass().getResource("/bodyframe/setting1.png")));
                    settingWindow.setLocation(BodyFrame.this.getX()+80,BodyFrame.this.getY()+BodyFrame.this.getHeight()-200);
                    settingWindow.setVisible(true);
                    break;
            }
        }
    }

    //面板切换时间
    private class PanelButtonMouse extends MouseAdapter{
        private int num;
        public PanelButtonMouse(int num){
            this.num = num;
        }
        public void mouseClicked(MouseEvent e) {
            whilePanelButton = this.num;
            for (int i = 0; i < 3; i++) {
                panelButtonList.get(i).flashButton();
            }

        }
    }
    //组件随界面改变
    private class UIResized extends ComponentAdapter {
        public void componentResized(ComponentEvent e){
//            int width = e.getComponent().getWidth();
//            int height = e.getComponent().getHeight();
//
//            BodyFrame.this.userPanel_JPanel.setSize(userPanelWidth,height);
//            BodyFrame.this.cardLayoutPanel_JPanel.setSize(width-userPanelWidth,height);
//            System.out.println("卡片布局面板："+(width-userPanelWidth)+"，和"+height);
////            BodyFrameResized.BodyFrameSize = new Dimension(BodyFrame.this.getWidth(),BodyFrame.this.getHeight());
//            BodyFrameResized.reSized();
            BodyFrameObj.reLocate();
            BodyFrameObj.setBodyFrame(BodyFrame.this);
        }
    }
}
