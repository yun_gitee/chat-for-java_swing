package com.talking.view.componentpojo;

import com.talking.view.bodyframe.SearchFriendFrame;
import com.talking.view.bodyframe.NoticeFrame;
import com.talking.view.bodyframe.SettingWindow;
import com.talking.view.bodyframe.UserMsgFrame;

import javax.swing.*;

public class BodyFrameObj {
    //窗体
    private static JFrame bodyFrame;

    //所有可以移动的Window,设置,表情包,好友信息
    private static SettingWindow settingWindow;
    private static JWindow emjioWindow;
    private static JWindow friendMsgWindow;
    private static UserMsgFrame userMessFrame;

    //添加好友界面,通知界面
    private static SearchFriendFrame searchFriendFrame;
    private static NoticeFrame noticeFrame;

    public static SearchFriendFrame getSearchFriendFrame() {
        return searchFriendFrame;
    }

    public static void setSearchFriendFrame(SearchFriendFrame searchFriendFrame) {
        BodyFrameObj.searchFriendFrame = searchFriendFrame;
    }

    public static NoticeFrame getNoticeFrame() {
        return noticeFrame;
    }

    public static void setNoticeFrame(NoticeFrame noticeFrame) {
        BodyFrameObj.noticeFrame = noticeFrame;
    }

    public static JWindow getFriendMsgWindow() {
        return friendMsgWindow;
    }

    public static void setFriendMsgWindow(JWindow friendMsgWindow) {
        BodyFrameObj.friendMsgWindow = friendMsgWindow;
    }

    public static SettingWindow getSettingWindow() {
        return settingWindow;
    }

    public static void setSettingWindow(SettingWindow settingWindow) {
        BodyFrameObj.settingWindow = settingWindow;
    }

    public static JWindow getEmjioWindow() {
        return emjioWindow;
    }

    public static void setEmjioWindow(JWindow emjioWindow) {
        BodyFrameObj.emjioWindow = emjioWindow;
    }

    public static void setBodyFrame(JFrame bodyFrame) {
        BodyFrameObj.bodyFrame = bodyFrame;
    }

    public static JFrame getBodyFrame() {
        return bodyFrame;
    }

    public static UserMsgFrame getUserMessFrame() {
        return userMessFrame;
    }

    public static void setUserMessFrame(UserMsgFrame userMessFrame) {
        BodyFrameObj.userMessFrame = userMessFrame;
    }

    public static void reLocate(){
//        userPanel_JPanel.setSize(userPanel_JPanel.getWidth(),frameContainer.getHeight());
//        cardLayoutPanel_JPanel.setSize(frameContainer.getWidth()-userPanel_JPanel.getWidth(),frameContainer.getHeight());
        settingWindow.setLocation(bodyFrame.getX()+80,bodyFrame.getY()+bodyFrame.getHeight()-200);
        emjioWindow.setBounds(bodyFrame.getX()+350,bodyFrame.getY()+bodyFrame.getHeight()-400,500,200);
        friendMsgWindow.setLocation(bodyFrame.getX()+bodyFrame.getWidth()-friendMsgWindow.getWidth(),bodyFrame.getY()+60);
    }
}
