package com.talking.view.componentpojo;

import com.talking.view.component.MyScrollBarUI;

import javax.swing.*;

public class JScrollPaneConfig {
    public static void setUI(JScrollPane jScrollPane){
        jScrollPane.getVerticalScrollBar().setUnitIncrement(15);
        jScrollPane.getVerticalScrollBar().setUI(new MyScrollBarUI());
        jScrollPane.setBorder(null);
        jScrollPane.setHorizontalScrollBarPolicy(jScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane.setVerticalScrollBarPolicy(jScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    }
}
