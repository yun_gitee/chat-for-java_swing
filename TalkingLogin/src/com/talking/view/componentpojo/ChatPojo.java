package com.talking.view.componentpojo;

import com.talking.view.chat.LeftPanel;
import com.talking.view.chat.RightPanel;
import com.talking.view.component.BlockPanel;
import com.talking.view.component.JIMSendTextPane;

import javax.swing.*;
import java.awt.*;

public class ChatPojo {
    private static LeftPanel leftPanel;

    private static RightPanel rightPanel;
    private static CardLayout rightCard;
    private static RightPanel.TopPanel topPanel;
    private static JIMSendTextPane sendPane;
    private static BlockPanel blockPanel;

    private static JScrollPane msgJScroll;         //滚动面板
    private static JPanel msgShow;          //显示面包那

    public static JPanel getMsgShow() {
        return msgShow;
    }

    public static void setMsgShow(JPanel msgShow) {
        ChatPojo.msgShow = msgShow;
    }

    public static LeftPanel getLeftPanel() {
        return leftPanel;
    }

    public static void setLeftPanel(LeftPanel leftPanel) {
        ChatPojo.leftPanel = leftPanel;
    }

    public static RightPanel getRightPanel() {
        return rightPanel;
    }

    public static void setRightPanel(RightPanel rightPanel) {
        ChatPojo.rightPanel = rightPanel;
    }

    public static CardLayout getRightCard() {
        return rightCard;
    }

    public static void setRightCard(CardLayout rightCard) {
        ChatPojo.rightCard = rightCard;
    }

    public static RightPanel.TopPanel getTopPanel() {
        return topPanel;
    }

    public static void setTopPanel(RightPanel.TopPanel topPanel) {
        ChatPojo.topPanel = topPanel;
    }

    public static JIMSendTextPane getSendPane() {
        return sendPane;
    }

    public static void setSendPane(JIMSendTextPane sendPane) {
        ChatPojo.sendPane = sendPane;
    }

    public static BlockPanel getBlockPanel() {
        return blockPanel;
    }

    public static void setBlockPanel(BlockPanel blockPanel) {
        ChatPojo.blockPanel = blockPanel;
    }

    public static JScrollPane getMsgJScroll() {
        return msgJScroll;
    }

    public static void setMsgJScroll(JScrollPane msgJScroll) {
        ChatPojo.msgJScroll = msgJScroll;
    }
}
