package com.talking.view.componentpojo;

import java.awt.*;

public class ColorObj {
    public final static Color MAIN_BACKGROUND = Color.white;
    public final static Color mainThemeColor = new Color(108, 99, 255);
    public final static Color mainThemeColor1 = new Color(122, 119, 186);
    public final static Color NAVIGATION_COLOR = new Color(97, 97, 97);
    public final static Color lightThemeColor = new Color(189, 165, 255);
    public final static Color buttonReleasedColor = new Color(205, 205, 205);
    public final static Color TEXT_TIPS = new Color(170, 170, 170);
    public final static Color splitLineColor = new Color(0xEFEFEF);
    public final static Color buttonText = new Color(0, 97, 255);
    public final static Color leftPanelColor = new Color(0xF8F8F8);

    public final static Color SPOT_GREEN = new Color(0xfefedf);

    //浅白色
    public final static Color LIGHT_WHITE1 = new Color(0xF8F8F8);
    public final static Color LIGHT_WHITE2 = new Color(0xEEEEEE);
    public final static Color LIGHT_WHITE3 = new Color(0xE8E8E8);
    public final static Color LIGHT_WHITE4 = new Color(0xE0E0E0);
    public final static Color LIGHT_WHITE5 = new Color(0xD8D8D8);
    public final static Color LIGHT_WHITE6 = new Color(0xD0D0D0);
    public final static Color LIGHT_WHITE7 = new Color(0x7D7A79);


    public final static Color GENERIC1 = new Color(214, 93, 177);
    public final static Color GENERIC2 = new Color(255, 111, 145);
    public final static Color GENERIC3 = new Color(255, 150, 113);
    public final static Color GENERIC4 = new Color(255, 199, 95);
    public final static Color GENERIC5 = new Color(249, 248, 113);

    //通用渐变
    public final static Color Generic_Gradient1 = new Color(200, 89, 174);
    public final static Color Generic_Gradient2 = new Color(255, 105, 145);
    public final static Color Generic_Gradient3 = new Color(255, 145, 112);
    public final static Color Generic_Gradient4 = new Color(255, 196, 94);
    public final static Color Generic_Gradient5 = new Color(249, 248, 113);

    //相似渐变
    public final static Color Matching_Gradient1 = new Color(0, 112, 203);
    public final static Color Matching_Gradient2 = new Color(0, 128, 201);
    public final static Color Matching_Gradient3 = new Color(0, 140, 183);
    public final static Color Matching_Gradient4 = new Color(0, 147, 156);
    public final static Color Matching_Gradient5 = new Color(0, 152, 127);

    //主色调版
    public final static Color Spot_Palette1 = new Color(0, 200, 160);
    public final static Color Spot_Palette2 = new Color(198, 252, 237);
    public final static Color Spot_Palette3 = new Color(78, 128, 116);
    public final static Color Spot_Palette4 = new Color(84, 174, 255);


}
