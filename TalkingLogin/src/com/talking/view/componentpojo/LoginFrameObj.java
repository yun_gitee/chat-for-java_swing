package com.talking.view.componentpojo;

import javax.swing.*;
import java.awt.*;

public class LoginFrameObj {
    public static JFrame login_JFrame;
    private static JLabel container;
    private static CardLayout cardLayout;

    public static JFrame getLogin_JFrame() {
        return login_JFrame;
    }

    public static void setLogin_JFrame(JFrame login_JFrame) {
        LoginFrameObj.login_JFrame = login_JFrame;
    }

    public static JLabel getContainer() {
        return container;
    }

    public static void setContainer(JLabel container) {
        LoginFrameObj.container = container;
    }

    public static CardLayout getCardLayout() {
        return cardLayout;
    }

    public static void setCardLayout(CardLayout cardLayout) {
        LoginFrameObj.cardLayout = cardLayout;
    }
}
