package com.talking.view.chat;

import com.talking.controller.chat.CreateGroupController;
import com.talking.view.componentpojo.ColorObj;
import com.talking.view.component.IAccountField;
import com.talking.view.component.IButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CreateGroupFrame {
    public JFrame jFrame;
    private JPanel frameContainer;
    private IAccountField groupName;
    private IButton createButton;           //创建按钮

    private int width = 500;
    private int height = 300;

    public CreateGroupFrame(){
        Dimension jFrameSize = Toolkit.getDefaultToolkit().getScreenSize();
        jFrame = new JFrame("创建群聊");
        frameContainer = new JPanel();
        groupName = new IAccountField("群名称",125,80,250,40);
        createButton = new IButton("创建群聊",ColorObj.mainThemeColor,true);



        jFrame.setBounds((int)(jFrameSize.getWidth()-width)/2,(int)(jFrameSize.getHeight()-height)/2-100,width,height);
        jFrame.setResizable(false);
        jFrame.addWindowFocusListener(new WindowAdapter() {


            @Override
            public void windowLostFocus(WindowEvent e) {
                jFrame.dispose();
            }
        });

        frameContainer.setBackground(ColorObj.MAIN_BACKGROUND);
        frameContainer.setLayout(null);
        groupName.setEnabled(false);
        createButton.setBounds(125,140,250,40);

        frameContainer.add(groupName);
        frameContainer.add(createButton);
        jFrame.add(frameContainer);

        createButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new CreateGroupController(groupName.getTheText());
            }
        });
    }
    public void showFrame(){
        jFrame.dispose();
        jFrame.setVisible(true);
    }
}
