package com.talking.view.chat;

import com.talking.controller.bodyframe.SearchFriendController;
import com.talking.controller.chat.SetCurrentObjUIController;
import com.talking.controller.friend.ExitGroupController;
import com.talking.view.component.*;
import com.talking.view.componentpojo.BodyFrameObj;
import com.talking.view.componentpojo.ChatPojo;
import com.talking.view.componentpojo.ColorObj;
import com.talking.view.componentpojo.JScrollPaneConfig;
import com.talking.pojo.CurrentChatObj;
import com.talking.pojo.Group;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.tool.MsgFileInput;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

//左边面板
public class LeftPanel extends JPanel {
    private SearchPanel searchPanel;      //顶部索面板

    private JPanel bottomContainer;         //滚动面板与私聊群聊面板的容器面板

    private JPanel chatKindPanel;           //私聊与群聊面板

    private BlockPanel signChatBlock;
    private BlockPanel groupChatBlock;

    private JPanel listContainer;             //好友列表与群聊列表的容器面板
    private CardLayout cardLayout;          //卡片布局

    private JScrollPane friendJScroll;          //滚动面板
    private JPanel friendListPanel;         //好友面板的内容组件

    private JScrollPane groupJScroll;           //群聊滚动面板
    private JPanel groupContainer;              //群聊的

    private CreateGroupFrame createGroupFrame;

    public List<User> friendList;           //好友列表
    private List<Group> groupList;          //群列表

    public List<FriendPanel> friendBlocks;          //好友块儿
    public List<GroupPanel> groupBlocks;            //群聊块儿


    private RightPanel rightPanel;
    private CardLayout rightCardLayout;

    private final int topPanelHeight = 50;
    private final int chatKindHeight = 30;
    private final int leftPanelWidth = 260;       //左侧联系人面板宽度

    private final Color MAIN_COLOR = ColorObj.MAIN_BACKGROUND;
    private BlockPanel beClickedBlock;              //记录哪个BlockPanel被点击了

    public LeftPanel(){
        friendList = SystemVariable.getFriendList();    //获取好友列表
        groupList = SystemVariable.getGroupList();      //获取群列表
        rightPanel = ChatPojo.getRightPanel();
        rightCardLayout = ChatPojo.getRightCard();

        friendBlocks = new ArrayList<>();
        groupBlocks = new ArrayList<>();

        IPopupMenu rootPop = new IPopupMenu(BodyFrameObj.getBodyFrame());
        rootPop.setItem("创建新群");


        searchPanel = new SearchPanel("搜索",leftPanelWidth,topPanelHeight,new SearchUser(), ColorObj.buttonReleasedColor);
        searchPanel.setPreferredSize(new Dimension(leftPanelWidth,topPanelHeight));

        bottomContainer = new JPanel();

        chatKindPanel = new JPanel();
        signChatBlock = new BlockPanel("好友");
        signChatBlock.setAllColor(ColorObj.Spot_Palette1,ColorObj.Spot_Palette2,MAIN_COLOR);
        signChatBlock.beClicked(true);
        groupChatBlock = new BlockPanel("群组");
        groupChatBlock.setAllColor(ColorObj.Spot_Palette1,ColorObj.Spot_Palette2,MAIN_COLOR);

        listContainer = new JPanel();
        cardLayout = new CardLayout();
        friendListPanel = new JPanel();
        friendJScroll = new JScrollPane(friendListPanel);
        groupContainer = new JPanel();
        groupJScroll = new JScrollPane(groupContainer);

        createGroupFrame = new CreateGroupFrame();

        this.setBackground(MAIN_COLOR);
        friendJScroll.setBackground(MAIN_COLOR);
        groupContainer.setLayout(new VerticalFlowLayout());
        JScrollPaneConfig.setUI(groupJScroll);
        this.setPreferredSize(new Dimension(leftPanelWidth,1));
        this.setLayout(new BorderLayout());

        bottomContainer.setBackground(MAIN_COLOR);
        bottomContainer.setLayout(new BorderLayout());
        chatKindPanel.setPreferredSize(new Dimension(leftPanelWidth,chatKindHeight));
        chatKindPanel.setLayout(null);
        chatKindPanel.setBackground(MAIN_COLOR);
        signChatBlock.setBounds(20,0,100,chatKindHeight);
        groupChatBlock.setBounds(140,0,100,chatKindHeight);

        listContainer.setLayout(cardLayout);
        listContainer.add(friendJScroll,"0");
        listContainer.add(groupJScroll,"1");

        JScrollPaneConfig.setUI(friendJScroll);
        friendListPanel.setBackground(MAIN_COLOR);
        groupContainer.setBackground(MAIN_COLOR);

        chatKindPanel.add(signChatBlock);
        chatKindPanel.add(groupChatBlock);
        bottomContainer.add(chatKindPanel, BorderLayout.NORTH);
        bottomContainer.add(listContainer,BorderLayout.CENTER);

        this.updateFriend();
        this.updateGroupList();

        this.add(searchPanel,BorderLayout.NORTH);
        this.add(bottomContainer,BorderLayout.CENTER);

        signChatBlock.addMouseListener(new CardChangeListener());
        groupChatBlock.addMouseListener(new CardChangeListener());


        ChatPojo.setLeftPanel(this);
        groupContainer.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton()==MouseEvent.BUTTON3){
                    rootPop.showMenu();
                }
            }
        });
        rootPop.setMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                createGroupFrame.showFrame();
            }
        },null);
    }

    //更新好友页面内容
    public void updateFriend(){
        friendList = SystemVariable.getFriendList();
        friendListPanel.setLayout(new VerticalFlowLayout());
        friendBlocks.removeAll(friendBlocks);
            for (int i = 0; i < friendList.size(); i++) {
                friendBlocks.add(new FriendPanel(i));
            }

        this.addFriendBlocks(friendBlocks);
    }

    public void addFriendBlocks(List<FriendPanel> friendPanels){
        friendBlocks = friendPanels;
        friendListPanel.removeAll();
        for (FriendPanel friendPanel : friendPanels) {
            friendListPanel.add(friendPanel);
        }

        if (friendList.size()==0){
            BlockPanel blockPanel = new BlockPanel("暂无任何好友！");
            blockPanel.setAllColor(ColorObj.MAIN_BACKGROUND,ColorObj.MAIN_BACKGROUND,ColorObj.MAIN_BACKGROUND);
            blockPanel.setPreferredSize(new Dimension(leftPanelWidth-20,60));
            friendListPanel.add(blockPanel);
        }
        friendListPanel.updateUI();
    }

    private class CardChangeListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e){
            rightCardLayout.show(rightPanel,"0");
            if (beClickedBlock!=null)
                beClickedBlock.beClicked(false);
            if (e.getComponent()==signChatBlock){
                signChatBlock.beClicked(true);
                groupChatBlock.beClicked(false);
                cardLayout.show(listContainer,"0");
            }else {
                signChatBlock.beClicked(false);
                groupChatBlock.beClicked(true);
                cardLayout.show(listContainer,"1");
            }
        }

    }

    //更新群列表
    public void updateGroupList(){
        groupList = SystemVariable.getGroupList();
        groupContainer.removeAll();
        groupBlocks.removeAll(groupBlocks);

        for (int i = 0; i < groupList.size(); i++) {
            groupBlocks.add(new GroupPanel(i));
        }
        this.addGroupBlocks(groupBlocks);
    }

    public void addGroupBlocks(List<GroupPanel> groupPanels){
        groupBlocks = groupPanels;
        groupContainer.removeAll();
        for (GroupPanel groupPanel : groupPanels) {
            groupContainer.add(groupPanel);
        }
        if (groupList.size()==0||groupList==null){
            BlockPanel blockPanel = new BlockPanel("暂未加入任何群聊");
            blockPanel.setAllColor(ColorObj.MAIN_BACKGROUND,ColorObj.MAIN_BACKGROUND,ColorObj.MAIN_BACKGROUND);
            blockPanel.setPreferredSize(new Dimension(leftPanelWidth-20,60));
            groupContainer.add(blockPanel);
        }
        groupContainer.updateUI();
    }

    // 每个群面板
    public class GroupPanel extends BlockPanel {
        public Group group;
        private HeadImage headLabel;
        private JLabel groupName;
        public JLabel groupTips;           //不知道取什么名字

        private IPopupMenu iPopupMenu;

        private RedDot redDot;          //红点

        public GroupPanel(int i){
            group = groupList.get(i);

            headLabel = new HeadImage(50,"g"+group.getGroup_id(),null);
            groupName = new JLabel(group.getGroup_name());
            groupTips = new JLabel(group.getGroup_describe());
            iPopupMenu = new IPopupMenu(BodyFrameObj.getBodyFrame());
            iPopupMenu.setItem("创建新群","删除群");

            redDot = new RedDot();


            headLabel.setLocation(10,5);
            groupName.setBounds(70,10,220,28);
            groupTips.setBounds(70,35,170,25);
            groupName.setFont(new Font("黑体",Font.PLAIN,18));
            groupTips.setFont(new Font("宋体",Font.PLAIN,12));
            groupTips.setForeground(ColorObj.LIGHT_WHITE6);

            redDot.setLocation(leftPanelWidth-40,10);

            this.setLayout(null);
            this.setAllColor(ColorObj.LIGHT_WHITE3,ColorObj.LIGHT_WHITE2,ColorObj.LIGHT_WHITE1);
            this.setPreferredSize(new Dimension(leftPanelWidth-20,60));

            this.add(headLabel);
            this.add(groupName);
            this.add(groupTips);

            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getButton()==MouseEvent.BUTTON1){
                        rightCardLayout.show(rightPanel,"1");
                        SystemVariable.setCurrentChatObj(new CurrentChatObj(group,false));
                        new SetCurrentObjUIController().start();

                        if (beClickedBlock!=null){
                            beClickedBlock.beClicked(false);
                        }
                        beClickedBlock = GroupPanel.this;
                        beClickedBlock.beClicked(true);
                        ChatPojo.setBlockPanel(GroupPanel.this);
                        remove(redDot);

    //                        new MsgFileInput(((Group)SystemVariable.getCurrentChatObj().getCurrentChatObj()).getGroup_id(),msgShow).read();
                    }else if (e.getButton()==MouseEvent.BUTTON3){
//                        jPopupMenu.show(e.getComponent(),e.getX(),e.getY());
                        iPopupMenu.showMenu();
                    }
                }
            });

            iPopupMenu.setMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    createGroupFrame.showFrame();
                }
            }, new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    new ExitGroupController(group);
                }
            });

        }

        public void setDot(){
            this.add(redDot);
            repaint();
        }
    }
    //每个好友的面板
    public class FriendPanel extends BlockPanel{
        public User user;
        private HeadImage headImageLabel;           //头像标签
        private JLabel userNameLabel;            //名字标签
        public JLabel messLabel;                //消息标签

        private RedDot redDot;          //红点


        public FriendPanel(int i){
            user = friendList.get(i);   //当前

            headImageLabel = new HeadImage(50,"u"+user.getUser_id(),null);
            userNameLabel = new JLabel(user.getUser_name());
            messLabel = new JLabel(user.getUser_describe());
            redDot = new RedDot();

            headImageLabel.setLocation(10,5);
            userNameLabel.setBounds(70,10,220,28);
            messLabel.setBounds(70,35,170,25);
            userNameLabel.setFont(new Font("黑体",Font.PLAIN,18));
            messLabel.setFont(new Font("宋体",Font.PLAIN,12));
            messLabel.setForeground(ColorObj.LIGHT_WHITE6);
            redDot.setLocation(leftPanelWidth-40,10);

            this.setLayout(null);
            this.setAllColor(ColorObj.LIGHT_WHITE3,ColorObj.LIGHT_WHITE2,ColorObj.LIGHT_WHITE1);
            this.setPreferredSize(new Dimension(leftPanelWidth-20,60));



            this.add(headImageLabel);
            this.add(userNameLabel);
            this.add(messLabel);

            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {

                    rightCardLayout.show(rightPanel,"1");
                    SystemVariable.setCurrentChatObj(new CurrentChatObj(user,true));
                    new SetCurrentObjUIController().start();
                    new MsgFileInput(((User)SystemVariable.getCurrentChatObj().getCurrentChatObj()).getUser_id()).read();

                    if (beClickedBlock!=null){
                        beClickedBlock.beClicked(false);
                    }
                    remove(redDot);
                    beClickedBlock = FriendPanel.this;
                    beClickedBlock.beClicked(true);
                    ChatPojo.setBlockPanel(FriendPanel.this);

                }
                @Override
                public void mouseEntered(MouseEvent e){
                    setBackground(ColorObj.SPOT_GREEN);
                }
                @Override
                public void mouseExited(MouseEvent e){
                    setBackground(ColorObj.leftPanelColor);
                }
            });
        }
        public void setDot(){
            this.add(redDot);
            repaint();
        }
    }

    //主页搜索
    private class SearchUser extends MouseAdapter{
        public void mouseClicked(MouseEvent e){
            String content = searchPanel.jTextField.getText().trim();
            String userName = "";
            String userID = "";

            List<FriendPanel> userBlocks = friendBlocks;


            for (FriendPanel userBlock : userBlocks) {
                userName = userBlock.user.getUser_name();
                userID = String.valueOf(userBlock.user.getUser_id());

                if (userName.equals(content)||userID.equals(content)){
                    rightCardLayout.show(rightPanel,"1");
                    SystemVariable.setCurrentChatObj(new CurrentChatObj(userBlock.user,true));
                    new SetCurrentObjUIController().start();
                    new MsgFileInput(((User)SystemVariable.getCurrentChatObj().getCurrentChatObj()).getUser_id()).read();

                    if (beClickedBlock!=null){
                        beClickedBlock.beClicked(false);
                    }

                    beClickedBlock = userBlock;
                    beClickedBlock.beClicked(true);
                }
            }
        }
    }
}

