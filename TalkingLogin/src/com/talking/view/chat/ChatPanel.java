package com.talking.view.chat;

import com.talking.view.componentpojo.BodyFrameObj;
import com.talking.pojo.Group;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.view.component.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ChatPanel extends JPanel {
    private LeftPanel leftPanel;           //左侧联系人面板
    private RightPanel rightPanel;          //右侧聊天面板
//
//    private CardLayout rightCardLayout;             //右边面板的卡片布局
//    private EmjioWidow emjioWidow;              //表情包内容窗口
//    private FriendMsgWindow friendMsgWindow;
//    private int caretPosition;              //输入框光标位置
//
//
//    //右边面板的全局组件
//    private UserNamePanel topPanel;            //顶部名字面板
//    private JIMSendTextPane msgShow;                    //消息显示框
//    private JIMSendTextPane jimSendTextPane;            //消息发送框
//
//    private List<EmjioTemp> emjioTempList = new ArrayList<>();


    public ChatPanel() {

        rightPanel = new RightPanel();
        leftPanel = new LeftPanel();

        this.setLayout(new BorderLayout());

        this.add(leftPanel, BorderLayout.WEST);
        this.add(rightPanel, BorderLayout.CENTER);

//        BodyFrameObj.setLeftPanel(leftPanel);
//        BodyFrameObj.setRightPanel(rightPanel);
    }
}
//    public void packageSendMSG(){
//        StringBuffer stringBuffer = new StringBuffer(jimSendTextPane.getText());
//        EmjioTemp[] emjioTemps = new EmjioTemp[emjioTempList.size()];
//
//        int temp1 = 0;
//         for (EmjioTemp emjioTemp : emjioTempList) {
//             emjioTemps[temp1] = emjioTemp;
//             temp1++;
//        }
//
//        for (int i=0;i<emjioTemps.length-1;i++){
//            for (int j=0;j<emjioTemps.length-1-i;j++){
//                EmjioTemp temp = null;
//                if (emjioTemps[j].position>emjioTemps[j+1].position) {
//                    temp = emjioTemps[j];
//                    emjioTemps[j] = emjioTemps[j+1];
//                    emjioTemps[j+1] = temp;
//                }
//            }
//        }
//
//        for (int i = 0; i < emjioTemps.length; i++) {
//            System.out.println(emjioTemps[i].toString());
//        }
//
//        for (int i = emjioTemps.length-1; i >=0 ; i--) {
//            stringBuffer.replace(emjioTemps[i].position,emjioTemps[i].position+1,emjioTemps[i].content);
//        }
//        System.out.println(stringBuffer.toString());
//        new SendChatMsgController(stringBuffer.toString(),jimSendTextPane);
//        emjioTempList.removeAll(emjioTempList);
//
//    }

//    //右边的面板
//    private class RightPanel extends JPanel{
//        private JLabel logoLabel;               //logo面板
//        private JPanel messageSendPanel;        //消息面板，包含聊天窗，消息窗
//        private JScrollPane centerJScrollPane;          //中间层的聊天滚动面板
//        private JPanel bottomPanel;                      //发送消息面板
//        private ToolPanel toolPanel;                  //表情包面板
//        private JScrollPane jScrollPane;                //发送消息面板文本框滚动条
//        private SendButtonPanel sendMsgButton;              //发送按钮面板
//        private JLabel emjioLabel;                      //表情包按钮
//
//
//        public RightPanel(){
//            rightCardLayout = new CardLayout();
//            logoLabel = new JLabel(new ImageIcon("image/logo/logo200x200.png"));
//            messageSendPanel = new JPanel();
//            topPanel = new UserNamePanel();
//            friendMsgWindow = new FriendMsgWindow(BodyFrameObj.getBodyFrame());
//            msgShow = new JIMSendTextPane();
//            centerJScrollPane = new JScrollPane(msgShow);
//
//            bottomPanel = new JPanel();
//            toolPanel = new ToolPanel();
//            emjioLabel = new JLabel(new ImageIcon("image/chat/emjio1.png"));
//            emjioWidow = new EmjioWidow(BodyFrameObj.getBodyFrame());
//
//
//            jimSendTextPane = new JIMSendTextPane();
//            jScrollPane = new JScrollPane(jimSendTextPane);
//            sendMsgButton = new SendButtonPanel();
//
//            msgShow.setEditable(false);
//            centerJScrollPane.setBackground(Color.blue);
//            centerJScrollPane.getVerticalScrollBar().setUnitIncrement(15);
//            centerJScrollPane.getVerticalScrollBar().setUI(new MyScrollBarUI());
//            centerJScrollPane.setBorder(null);
//            centerJScrollPane.setHorizontalScrollBarPolicy(centerJScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
//            centerJScrollPane.setVerticalScrollBarPolicy(centerJScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
//
//            this.setLayout(rightCardLayout);
//            messageSendPanel.setLayout(new BorderLayout());
//
//            bottomPanel.setPreferredSize(new Dimension(1,200));
//            bottomPanel.setLayout(new BorderLayout());
//            bottomPanel.setBackground(Color.white);
//            toolPanel.setPreferredSize(new Dimension(1,40));
//            jimSendTextPane.setPreferredSize(new Dimension(1,100));
//            jScrollPane.getVerticalScrollBar().setUnitIncrement(15);
//
//            jScrollPane.getVerticalScrollBar().setUI(new MyScrollBarUI());
//            jScrollPane.setBorder(null);
//            jScrollPane.setHorizontalScrollBarPolicy(jScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
//            jScrollPane.setVerticalScrollBarPolicy(jScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
//
//            sendMsgButton.setPreferredSize(new Dimension(1,60));
//
//            toolPanel.setLayout(null);
//            toolPanel.add(emjioLabel);
//
//            messageSendPanel.add(topPanel, BorderLayout.NORTH);
//            messageSendPanel.add(centerJScrollPane,BorderLayout.CENTER);
//            messageSendPanel.add(bottomPanel,BorderLayout.SOUTH);
//            bottomPanel.add(toolPanel,BorderLayout.NORTH);
//            JLabel t = new JLabel();
//            t.setPreferredSize(new Dimension(20,1));
//            bottomPanel.add(t, BorderLayout.WEST);
//            bottomPanel.add(jScrollPane,BorderLayout.CENTER);
//            bottomPanel.add(sendMsgButton,BorderLayout.SOUTH);
//            this.add(logoLabel,"0");
//            this.add(messageSendPanel,"1");
//
//            this.setBackground(ColorObj.MAIN_BACKGROUND);
//            BodyFrameObj.setEmjioWindow(emjioWidow);
//            BodyFrameObj.setFriendMsgWindow(friendMsgWindow);
//            BodyFrameObj.setMsgShowPane(msgShow);
//            BodyFrameObj.setRightCarLayout(rightCardLayout);
//            emjioLabel.setBounds(20,5,25,25);
//            emjioLabel.addMouseListener(new MouseAdapter() {
//                @Override
//                public void mousePressed(MouseEvent e) {
//                    emjioLabel.setIcon(new ImageIcon("image/chat/emjio2.png"));
//                    caretPosition = jimSendTextPane.getCaretPosition();
//                    repaint();
//                }
//
//                @Override
//                public void mouseReleased(MouseEvent e) {
//                    emjioLabel.setIcon(new ImageIcon("image/chat/emjio1.png"));
//                    repaint();
//                    JFrame jFrame = BodyFrameObj.getBodyFrame();
//                    emjioWidow.setBounds(jFrame.getX()+350,jFrame.getY()+jFrame.getHeight()-400,500,200);
//                    emjioWidow.setFocusableWindowState(true);
//                    emjioWidow.requestFocus();
//                    emjioWidow.setVisible(true);
//                }
//            });
//
//            jimSendTextPane.addKeyListener(new MyKeyListener());
//
//        }
//        //这个类用来处理表情包与文字
//        private class MyKeyListener extends KeyAdapter{
//            private int banTwoButton=0;
//
//            @Override
//            public void keyPressed(KeyEvent e) {
//                System.out.println("Press监听"+e.getKeyCode());
//                int position = jimSendTextPane.getCaretPosition();          //光标位置
//
//                banTwoButton = e.getKeyCode();
//                //仅仅处理删除键与回车键
//                if (e.getKeyCode()==8){
//                    System.out.println("press检测到删除键");
//                    int start = jimSendTextPane.getSelectionStart();
//                    int end = jimSendTextPane.getSelectionEnd();
//                    int selectTextCount = end-start;
//
//                    if (start==end&&position!=0) {            //是否是单个删除
//                        System.out.println("单个删除");
//
//                                for (int i = 0,countTemp = 0; countTemp < emjioTempList.size()&&emjioTempList.size()!=0; i++,countTemp++) {
//
//                                    EmjioTemp emjioTemp = emjioTempList.get(i);
//
//
//                                    if (position <= emjioTemp.position) {
//                                        System.out.println("字符删除");
//                                        System.out.println("光标位置是：" + position + ",表情包位置：" + emjioTemp.position);
//                                        System.out.println("删除前长度：" + emjioTempList.size());
//                                        emjioTempList.remove(i);
//                                        System.out.println("删除后的长度：" + emjioTempList.size());
//                                        i--;
//                                        emjioTempList.add(new EmjioTemp(emjioTemp.position-1,emjioTemp.content));
//                                        System.out.println("增加后的长度：" + emjioTempList.size());
//
//                                        for (EmjioTemp emjioTemp1 : emjioTempList) {
//                                            System.out.println(emjioTemp1.toString());
//                                        }
//                                    } else if (position == emjioTemp.position + 1) {
//                                        System.out.println("执行了删除单个表情包：");
//                                        //移除自己，并且后面每一个元素的坐标都-1
//                                        emjioTempList.remove(i);
//
//                                        i--;
//                                        countTemp--;
//                                    }
//                                }
//
//                    }else {     //选择删除
//                        System.out.println("选择性删除判断");
//                        if (position==0&&end==jimSendTextPane.getText().length()) {            //全选删除
//                            System.out.println("全删");
//                            emjioTempList.removeAll(emjioTempList);
//                        } else{
//                            System.out.println("非全选");
//                            for (int i = 0,temp=0; temp < emjioTempList.size()&&emjioTempList.size()!=0; i++,temp++) {
//                                EmjioTemp emjioTemp = emjioTempList.get(i);
//                                if (start <= emjioTemp.position) {
//                                    if (end <= emjioTemp.position) {
//                                        System.out.println("结束光标在左边");
//                                        System.out.println("光标位置是：" + position + ",表情包位置：" + emjioTemp.position);
//                                        emjioTemp.position -= selectTextCount;
//                                        emjioTempList.remove(i);
//                                        i--;
//                                        emjioTempList.add(emjioTemp);
//                                        for (EmjioTemp emjioTemp1 : emjioTempList) {
//                                            System.out.println(emjioTemp1.toString());
//                                        }
//                                    } else {
//                                        System.out.println("结束光标在右边");
//                                        System.out.println("光标位置是：" + position + ",表情包位置：" + emjioTemp.position);
//                                        emjioTempList.remove(i);
//                                        i--;
//                                        temp--;
//                                        for (EmjioTemp emjioTemp1 : emjioTempList) {
//                                            System.out.println(emjioTemp1.toString());
//                                        }
//                                    }
//                                }
//                            }
//
//                        }
//                    }
//                }else if (e.getKeyCode()==10) {
//                    e.consume();
//                    ChatPanel.this.packageSendMSG();
//                }
//            }
//
//
//
//            @Override
//            public void keyTyped(KeyEvent e){
//                int keyCode = e.getKeyChar();           //键入事件无法获取keycode
//                int position = jimSendTextPane.getCaretPosition();
//                int start = jimSendTextPane.getSelectionStart();
//                int end = jimSendTextPane.getSelectionEnd();
//                int selectTextCount = end-start;
//
//
//                System.out.println("\n这是Type监听,监听KeyChar是："+keyCode+"按钮");
//
//                if (keyCode!=8&&keyCode!=10&&banTwoButton==keyCode){
//                    if (start==end){            //单选
//                        System.out.println("插入一个字符");
//
//                        for (int i = 0,temp=0; temp < emjioTempList.size()&&emjioTempList.size()!=0; i++,temp++) {
//                            EmjioTemp emjioTemp = emjioTempList.get(i);
//
//                            if (position<=emjioTemp.position){
//                                System.out.println("单个字符增加");
//                                System.out.println("光标位置是：" + position + ",表情包位置：" + emjioTemp.position);
//                                System.out.println("删除前长度：" + emjioTempList.size());
//                                emjioTempList.remove(i);
//                                System.out.println("删除后的长度：" + emjioTempList.size());
//                                i--;
//                                emjioTempList.add(new EmjioTemp(emjioTemp.position+1,emjioTemp.content));
//                                System.out.println("增加后的长度：" + emjioTempList.size());
//
//                                for (EmjioTemp emjioTemp1 : emjioTempList) {
//                                    System.out.println(emjioTemp1.toString());
//                                }
//                            }
//                        }
//
//                    }else {
//                        System.out.println("一段字符变成一个字符");
//
//                        if (position==0&&end==jimSendTextPane.getText().length()) {            //全选删除
//                            System.out.println("全部选中");
//                            emjioTempList.removeAll(emjioTempList);
//                        } else{
//                            System.out.println("非全选");
//
//                            for (int i = 0,temp=0; temp < emjioTempList.size()&&emjioTempList.size()!=0; i++,temp++) {
//                                EmjioTemp emjioTemp = emjioTempList.get(i);
//                                if (start <= emjioTemp.position) {
//                                    if (end <= emjioTemp.position) {
//                                        System.out.println("结束光标在左边");
//                                        System.out.println("光标位置是：" + position + ",表情包位置：" + emjioTemp.position);
//                                        emjioTemp.position = emjioTemp.position - selectTextCount+1;
//                                        emjioTempList.remove(i);
//                                        i--;
//                                        emjioTempList.add(emjioTemp);
//                                        for (EmjioTemp emjioTemp1 : emjioTempList) {
//                                            System.out.println(emjioTemp1.toString());
//                                        }
//                                    } else {
//                                        System.out.println("结束光标在右边");
//                                        System.out.println("光标位置是：" + position + ",表情包位置：" + emjioTemp.position);
//                                        emjioTempList.remove(i);
//                                        i--;
//                                        temp--;
//                                        for (EmjioTemp emjioTemp1 : emjioTempList) {
//                                            System.out.println(emjioTemp1.toString());
//                                        }
//                                    }
//                                }
//                            }
//
//                        }
//
//                    }
//                }
//
//
////                if (e.getKeyCode()==8)
////                    e.consume();
////                else if (e.getKeyCode()==0) {
////
////                }else {
////                    int position = jimSendTextPane.getCaretPosition();
////
////                    for (int i = 0; i < emjioTempList.size(); i++) {
////                        EmjioTemp emjioTemp = emjioTempList.get(i);
////                        if (position<=emjioTemp.position)
////                            emjioTemp.position++;
////                    }
////                }
//            }
//        }
//    }

//    private class EmjioTemp{
//        private int position;
//        private String content;
//        public EmjioTemp(int position,String content){
//            this.position = position;
//            this.content = content;
//        }
//
//        @Override
//        public String toString() {
//            return "EmjioTemp{" +
//                    "position=" + position +
//                    ", content='" + content + '\'' +
//                    '}';
//        }
//    }
//
//    //表情包的弹窗
//    private class EmjioWidow extends JWindow{
//        private JLabel jLabelShadow;           //阴影面板
//        private JScrollPane jScrollPane;       //滚动面板
//        private JPanel jPanelContent;          //内容面板
//        public EmjioWidow(JFrame jFrame){
//            super(jFrame);
//            jLabelShadow = new JLabel();
//            jPanelContent = new JPanel();
//            jScrollPane = new JScrollPane(jPanelContent);
//
//            this.setSize(new Dimension(500,200));
//            this.setBackground(new Color(0,0,0,0));
//            this.setFocusable(true);
//            jPanelContent.setBackground(Color.black);
//            jLabelShadow.setBorder(new ShadowBorder(10));
//            jLabelShadow.setLayout(null);
//            jPanelContent.setBackground(ColorObj.MAIN_BACKGROUND);
//            jScrollPane.getVerticalScrollBar().setUnitIncrement(15);
//            jScrollPane.getVerticalScrollBar().setUI(new MyScrollBarUI());
//            jScrollPane.setBorder(null);
//            jScrollPane.setHorizontalScrollBarPolicy(jScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
//            jScrollPane.setVerticalScrollBarPolicy(jScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
//            jScrollPane.setBounds(13,13,474,174);
//
//            GridLayout gridLayout = new GridLayout(5,7);
//            gridLayout.setVgap(10);
//            jPanelContent.setLayout(gridLayout);
//            for (int i = 0; i < 31; i++) {
//                jPanelContent.add(new DetailEmjio(i));
//            }
//
//            jLabelShadow.add(jScrollPane, BorderLayout.CENTER);
//            this.add(jLabelShadow);
//            this.addWindowFocusListener(new WindowAdapter() {
//                @Override
//                public void windowLostFocus(WindowEvent e) {
//                    dispose();
//                }
//            });
//        }
//    }
//    //每一个表情包
//    private class DetailEmjio extends JLabel{
//        private int num;
//        private List<String> emjioName;
//        private String code;
//        public DetailEmjio(int num){
//            this.num = num;
//            emjioName = new ArrayList();
//
//            File imagePath = new File("image/chat/emjio");
//            File[] allPng = imagePath.listFiles();
//            for (File file : allPng) {
//                emjioName.add(file.getName());
//            }
//
////            this.setIcon(new ImageIcon("image/chat/emjio/"+emjioName.get(num)));
//            setIcon(new ImageIcon("image/chat/emjiomin/"+emjioName.get(num)));
//            this.setToolTipText(emjioName.get(num).replace(".png",""));
//            this.setPreferredSize(new Dimension(40,40));
//
//            this.addMouseListener(new MouseAdapter() {
//                @Override
//                public void mouseEntered(MouseEvent e){
////                    setIcon(new ImageIcon("image/chat/emjiomin/"+emjioNameMin.get(num)));
//                    setIcon(new ImageIcon("image/chat/emjio/"+emjioName.get(num)));
//                }
//                @Override
//                public void mouseExited(MouseEvent e){
////                    setIcon(new ImageIcon("image/chat/emjio/"+emjioName.get(num)));
//                    setIcon(new ImageIcon("image/chat/emjiomin/"+emjioName.get(num)));
//                }
//                @Override
//                public void mouseClicked(MouseEvent e) {
//                    jimSendTextPane.setCaretPosition(caretPosition);
//                    Font f = new Font("黑体",Font.PLAIN,24);
//                    FontMetrics fm = sun.font.FontDesignMetrics.getMetrics(f);
//                    ImageIcon imageIcon = new ImageIcon("image/chat/emjio/"+emjioName.get(num));
//                    imageIcon.setImage(imageIcon.getImage().getScaledInstance(fm.getHeight(),fm.getHeight(),Image.SCALE_DEFAULT));
//                    jimSendTextPane.insertIcon(imageIcon);
//                    emjioTempList.add(new EmjioTemp(caretPosition,emjioName.get(num).replace(".png","")));
////                    }
//                }
//            });
//        }
//    }
//
//    //顶部的名字面板
//    public class UserNamePanel extends JPanel{
//        public JLabel userNameLabel;
//        private PersonPanel null2;
//        public UserNamePanel(){
//            userNameLabel = new JLabel("暂无用户");
//            null2 = new PersonPanel();
//
//            this.setLayout(new BorderLayout());
//            userNameLabel.setFont(new Font("微软雅黑",Font.BOLD,20));
//            userNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
//            this.add(userNameLabel,BorderLayout.CENTER);
//            this.add(null2,BorderLayout.EAST);
//            this.setPreferredSize(new Dimension(1,50));
//            this.setBackground(ColorObj.MAIN_BACKGROUND);
//        }
//        public void paintComponent(Graphics g){
//            super.paintComponent(g);
//            g.setColor(ColorObj.splitLineColor);
//            g.drawLine(0,this.getHeight()-1,this.getWidth(),this.getHeight()-1);
//            g.drawLine(1,0,1,this.getHeight());
//        }
//    }
//
//    //好友的信息面板
//    private class PersonPanel extends JLabel{
//        private Color buttonColor;
//        public PersonPanel(){
//            buttonColor = ColorObj.buttonReleasedColor;
//            this.setPreferredSize(new Dimension(50,50));
//            this.addMouseListener(new MouseAdapter() {
//                @Override
//                public void mousePressed(MouseEvent e) {
//                    buttonColor = ColorObj.mainThemeColor;
//                    PersonPanel.this.repaint();
//                }
//
//                @Override
//                public void mouseReleased(MouseEvent e) {
//                    JFrame jFrame = BodyFrameObj.getBodyFrame();
//                    friendMsgWindow.setLocation(jFrame.getX()+jFrame.getWidth()-340,jFrame.getY()+40);
//                    new FriendMsgController(friendMsgWindow).setContent();
//                    friendMsgWindow.setVisible(true);
//                    buttonColor = ColorObj.buttonReleasedColor;
//                    PersonPanel.this.repaint();
//                }
//            });
//        }
//        public void paintComponent(Graphics g){
//            super.paintComponent(g);
//            g.setColor(buttonColor);
//            for (int i = 0; i < 3; i++) {
//                g.fillOval(5+i*10,22,5,5);
//            }
//        }
//    }
//    private class ToolPanel extends JPanel{
//        public void paintComponent(Graphics g){
//            g.setColor(ColorObj.splitLineColor);
//            g.drawLine(0,1,this.getWidth(),1);
//        }
//    }
//    private class SendButtonPanel extends JPanel{
//        private SendButton sendButton;
//        public SendButtonPanel(){
//            sendButton = new SendButton();
//            sendButton.setPreferredSize(new Dimension(120,40));
//            this.setLayout(new BorderLayout());
//            this.setBackground(ColorObj.MAIN_BACKGROUND);
//
//            this.add(new JLabel(),BorderLayout.CENTER);
//            this.add(sendButton,BorderLayout.EAST);
//
//        }
//    }
//
//    //发送按钮
//    private class SendButton extends JLabel{
//        private Color buttonTextColor;
//        public SendButton(){
//            this.setBackground(ColorObj.MAIN_BACKGROUND);
//            buttonTextColor = ColorObj.buttonReleasedColor;
//            this.addMouseListener(new MouseAdapter() {
//                @Override
//                public void mousePressed(MouseEvent e) {
//                    buttonTextColor = Color.BLACK;
//                    repaint();
//                }
//
//                @Override
//                public void mouseReleased(MouseEvent e) {
//                    buttonTextColor = ColorObj.buttonReleasedColor;
//                    repaint();
//                    ChatPanel.this.packageSendMSG();
//                }
//            });
//        }
//        public void paintComponent(Graphics g){
//            super.paintComponent(g);
//            Graphics2D draw = (Graphics2D)g;                                                            //抗锯齿
//            draw.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//
//            draw.setColor(ColorObj.buttonReleasedColor);
//
//            draw.drawArc(2,0,39,39,90,180);
//            draw.drawArc(3,1,37,37,90,180);
//            draw.drawLine(21,0,59,0);
//            draw.drawLine(21,1,59,1);
//            draw.drawLine(21,39,59,39);
//            draw.drawLine(21,38,59,38);
//            draw.drawArc(39,1,37,37,270,180);
//            draw.drawArc(38,0,39,39,270,180);
//
//            draw.setColor(buttonTextColor);
//            draw.setFont(new Font("等线",Font.BOLD,20));
//            draw.drawString("发送",20,26);
//        }
//    }







//    //左边面板
//    public class LeftPanel extends JPanel{
//        private SearchField searchField;      //顶部索面板
//
//        private JPanel bottomContainer;         //滚动面板与私聊群聊面板的容器面板
//        private JPanel chatKindPanel;           //私聊与群聊面板
//        private JPanel signChatPanel;               //私聊按钮
//        private JLabel sighChatLabel;               //私聊图片标签
//        private JPanel groupChatPanel;             //群聊按钮
//        private JLabel groupChatLabel;             //群聊图片标签
//
//        private JPanel friendContainer;             //好友列表与群聊列表的容器面板
//        private CardLayout cardLayout;          //卡片布局
//        private JScrollPane jscrollPane;          //滚动面板
//        private JPanel friendListPanel;         //好友面板的内容组件
//        private Box groupBox = Box.createVerticalBox();     //群聊好友面包那
//        private GroupListPanel groupListPanel;      //群聊板
//        private CreateGroupFrame createGroupFrame;
//
//        private final int topPanelHeight = 50;
//        private final int chatKindHeight = 30;
//
//        public LeftPanel(){
//            searchField = new SearchField("搜索",leftPanelWidth,topPanelHeight,null,ColorObj.buttonReleasedColor);
//            searchField.setPreferredSize(new Dimension(leftPanelWidth,topPanelHeight));
//            bottomContainer = new JPanel();
//            chatKindPanel = new JPanel();
//            signChatPanel = new JPanel();
//            sighChatLabel = new JLabel(new ImageIcon("image/chat/leftPanel/signChat2.png"));
//            groupChatPanel = new JPanel();
//            groupChatLabel = new JLabel(new ImageIcon("image/chat/leftPanel/groupChat2.png"));
//            friendContainer = new JPanel();
//            cardLayout = new CardLayout();
//            friendListPanel = new JPanel();
//            jscrollPane = new JScrollPane(friendListPanel);
//            groupListPanel = new GroupListPanel();
//            createGroupFrame = new CreateGroupFrame();
//
//            this.setBackground(ColorObj.leftPanelColor);
//            jscrollPane.setBackground(ColorObj.leftPanelColor);
//            this.setPreferredSize(new Dimension(leftPanelWidth,1));
//            this.setLayout(new BorderLayout());
//
//            bottomContainer.setBackground(ColorObj.leftPanelColor);
//            bottomContainer.setLayout(new BorderLayout());
//            chatKindPanel.setPreferredSize(new Dimension(leftPanelWidth,chatKindHeight));
//            chatKindPanel.setLayout(new GridLayout(1,2));
//            chatKindPanel.setBackground(ColorObj.leftPanelColor);
//            signChatPanel.setBackground(ColorObj.TEXT_TIPS);
//            groupChatPanel.setBackground(ColorObj.leftPanelColor);
//
//            friendContainer.setLayout(cardLayout);
//            friendContainer.add(jscrollPane,"0");
//            friendContainer.add(groupListPanel,"1");
//
//            JScrollPaneConfig.setUI(jscrollPane);
//            friendListPanel.setBackground(ColorObj.leftPanelColor);
//
//            signChatPanel.add(sighChatLabel);
//            groupChatPanel.add(groupChatLabel);
//            chatKindPanel.add(signChatPanel);
//            chatKindPanel.add(groupChatPanel);
//            bottomContainer.add(chatKindPanel, BorderLayout.NORTH);
//            bottomContainer.add(friendContainer,BorderLayout.CENTER);
//            this.updateFriend();
//            this.add(searchField,BorderLayout.NORTH);
//            this.add(bottomContainer,BorderLayout.CENTER);
//            BodyFrameObj.setJscrollPane(jscrollPane);
//
//            signChatPanel.addMouseListener(new CardChangeListener());
//            groupChatPanel.addMouseListener(new CardChangeListener());
//
//
//        }
//
//        //更新页面内容
//        public void updateFriend(){
//            friendList = SystemVariable.getFriendList();
//            friendListPanel.removeAll();
//            friendListPanel.setLayout(new GridLayout(friendList.size()>40?friendList.size():40,1));
//
//            for (int i = 0; i < friendList.size(); i++) {
//                friendListPanel.add(new FriendPanel(i));
//            }
//
//            friendListPanel.updateUI();
//        }
//
//        private class CardChangeListener extends MouseAdapter{
//            @Override
//            public void mouseClicked(MouseEvent e){
//                rightCardLayout.show(rightPanel,"0");
//                if (e.getComponent()==signChatPanel){
//                    signChatPanel.setBackground(ColorObj.MAIN_BACKGROUND);
//                    groupChatPanel.setBackground(ColorObj.leftPanelColor);
//                    cardLayout.show(friendContainer,"0");
//                }else {
//                    signChatPanel.setBackground(ColorObj.leftPanelColor);
//                    groupChatPanel.setBackground(ColorObj.MAIN_BACKGROUND);
//                    cardLayout.show(friendContainer,"1");
//                }
//            }
//
//        }
//
//        //群聊用户面板
//        private class GroupListPanel extends JPanel{
//            private JScrollPane groupJScroll;           //群聊滚动面板
//            private JPanel groupContainer;              //组件容器
//
//            public GroupListPanel(){
//                groupContainer = new JPanel();
//                groupJScroll = new JScrollPane(groupContainer);
//
//
//                this.setBackground(ColorObj.MAIN_BACKGROUND);
//                this.setLayout(new BorderLayout());             //不设置，则滚动面板不显示
//                groupContainer.setBackground(ColorObj.splitLineColor);
//                JScrollPaneConfig.setUI(groupJScroll);
//
//                groupContainer.add(groupBox);
//                groupJScroll.setOpaque(true);
//                groupJScroll.getViewport().setOpaque(true);
//                updateGroupList();
//                this.add(groupJScroll);
//            }
//        }
//        public void updateGroupList(){
//            groupList = SystemVariable.getGroupList();
//            groupBox.removeAll();
//
//            JPanel createPanel;                     //建群的面板用来装label
//            JLabel createGroupLabel;            //创建群聊的按钮
//            createPanel = new JPanel();
//            ImageIcon image = new ImageIcon("image/chat/leftPanel/createGroup1.png");
//            image.setImage(image.getImage().getScaledInstance(leftPanelWidth-50,48,Image.SCALE_DEFAULT));
//            createGroupLabel = new JLabel(image);
//
//            createPanel.setPreferredSize(new Dimension(leftPanelWidth,60));
//            createPanel.setBackground(ColorObj.leftPanelColor);
//            createPanel.add(createGroupLabel);
//            groupBox.add(createPanel);
//            for (int i = 0; i < groupList.size(); i++) {
//                groupBox.add(new GroupPanel(i));
//            }
//
//            groupBox.updateUI();
//            createGroupLabel.addMouseListener(new MouseAdapter() {
//                @Override
//                public void mousePressed(MouseEvent e) {
//                    ImageIcon image1 = new ImageIcon("image/chat/leftPanel/createGroup2.png");
//                    image1.setImage(image1.getImage().getScaledInstance(leftPanelWidth-50,48,Image.SCALE_DEFAULT));
//                    createGroupLabel.setIcon(image1);
//                }
//
//                @Override
//                public void mouseReleased(MouseEvent e) {
//                    ImageIcon image1 = new ImageIcon("image/chat/leftPanel/createGroup1.png");
//                    image1.setImage(image1.getImage().getScaledInstance(leftPanelWidth-50,48,Image.SCALE_DEFAULT));
//                    createGroupLabel.setIcon(image1);
//                    createGroupFrame.jFrame.setVisible(true);
//                }
//            });
//
//        }
//
//        // 每个群面板
//        private class GroupPanel extends BlockPanel{
//            private Group group;
//            private HeadImage headLabel;
//            private JLabel groupName;
//            private JLabel groupTips;           //不知道取什么名字
//
//            public GroupPanel(int i){
//                super();
//                group = groupList.get(i);
//
//                headLabel = new HeadImage(50,"image/chat/test.jpg",null);
//                groupName = new JLabel(group.getGroup_name());
//                groupTips = new JLabel(group.getGroup_describe());
//
//
//                headLabel.setLocation(10,5);
//                groupName.setBounds(70,10,220,28);
//                groupTips.setBounds(70,35,170,25);
//                groupName.setFont(new Font("黑体",Font.PLAIN,18));
//                groupTips.setFont(new Font("宋体",Font.PLAIN,14));
//                groupTips.setForeground(ColorObj.buttonReleasedColor);
//
//                this.setLayout(null);
//                this.setAllColor(ColorObj.GENERIC3,ColorObj.lightThemeColor,ColorObj.leftPanelColor);
//                this.setPreferredSize(new Dimension(1,60));
//
//                this.add(headLabel);
//                this.add(groupName);
//                this.add(groupTips);
//
//                this.addMouseListener(new MouseAdapter() {
//                    @Override
//                    public void mouseClicked(MouseEvent e) {
//                        rightCardLayout.show(rightPanel,"1");
//                        SystemVariable.setCurrentChatObj(new CurrentChatObj(group,false));
//                        new SetCurrentObjUIController(topPanel,msgShow,jimSendTextPane).start();
////                        new MsgFileInput(((Group)SystemVariable.getCurrentChatObj().getCurrentChatObj()).getGroup_id(),msgShow).read();
//                    }
//                });
//
//            }
//        }
//    }
//
//    //每个好友的面板
//    private class FriendPanel extends JPanel{
//        private User user;
//        private HeadImage headImageLabel;           //头像标签
//        private JLabel userNameLabel;            //名字标签
//        private JLabel messLabel;                //消息标签
//
//
//        public FriendPanel(int i){
//            user = friendList.get(i);   //当前
//
//            headImageLabel = new HeadImage(50,"image/chat/index.jpg",null);
//            userNameLabel = new JLabel(user.getUser_name());
//            messLabel = new JLabel(user.getUser_describe());
//
//            headImageLabel.setLocation(10,15);
//            userNameLabel.setBounds(70,15,220,28);
//            messLabel.setBounds(70,45,170,25);
//            userNameLabel.setFont(new Font("微软雅黑",Font.BOLD,18));
//            messLabel.setFont(new Font("微软雅黑",Font.PLAIN,14));
//            messLabel.setForeground(ColorObj.TEXT_TIPS);
//
//            this.setLayout(null);
//            this.setBackground(ColorObj.leftPanelColor);
//            this.setPreferredSize(new Dimension(300,80));
//
//            this.add(headImageLabel);
//            this.add(userNameLabel);
//            this.add(messLabel);
//            this.addMouseListener(new MouseAdapter() {
//                @Override
//                public void mouseClicked(MouseEvent e) {
//
//                    rightCardLayout.show(rightPanel,"1");
//                    SystemVariable.setCurrentChatObj(new CurrentChatObj(user,true));
//                    new SetCurrentObjUIController(topPanel,msgShow,jimSendTextPane).start();
//                    new MsgFileInput(((User)SystemVariable.getCurrentChatObj().getCurrentChatObj()).getUser_id(),msgShow).read();
//
//                }
//                @Override
//                public void mouseEntered(MouseEvent e){
//                    setBackground(ColorObj.SPOT_GREEN);
//                }
//                @Override
//                public void mouseExited(MouseEvent e){
//                    setBackground(ColorObj.leftPanelColor);
//                }
//            });
//        }
//        public void paintComponent(Graphics g){
//            super.paintComponent(g);
//            g.setColor(ColorObj.buttonReleasedColor);
//            g.drawLine(5,this.getHeight()-2,this.getWidth()-5,this.getHeight()-2);
//        }
//    }
//}
