package com.talking.view.chat;

import com.talking.controller.friend.FriendRequestController;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.view.component.HeadImage;
import com.talking.view.component.IButton;
import com.talking.view.component.TextLabel;
import com.talking.view.componentpojo.ColorObj;
import com.talking.view.componentpojo.JScrollPaneConfig;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class AccountMessFrame extends JFrame {
    private JPanel container;
    private SpringLayout springLayout;
    private HeadImage headImage;
    private TextLabel nameLabel;
    private TextLabel IDLabel;
    private TextLabel sexLabel;
    private TextLabel ageLabel;
    private TextLabel describeLabel;

    private JTextField nameField;
    private JTextField IDField;
    private JLabel sexField;
    private JTextField ageField;
    private JTextArea describeArea;
    private JScrollPane jScrollPane;

    private IButton addButton;

    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private final int height = 500;
    private final int width = 300;

    private final int headSize = 80;
    private final int leftBor = 20;
    private final int bua = 10;

    private User currentUser;

    public AccountMessFrame(){
        container = new JPanel();
        springLayout = new SpringLayout();
        headImage =new HeadImage(headSize,null,null);
        nameLabel = new TextLabel("昵称：");
        IDLabel = new TextLabel("账号");
        sexLabel = new TextLabel("性别");
        ageLabel = new TextLabel("年龄");
        describeLabel = new TextLabel("个人说明：");
        addButton = new IButton("添加好友",ColorObj.mainThemeColor,true);

        nameField = new JTextField();
        IDField = new JTextField();
        sexField = new JLabel();
        ageField = new JTextField();
        describeArea = new JTextArea();
        jScrollPane = new JScrollPane(describeArea);

        this.setSpring(springLayout,headImage,container,20,container,(width-headSize)/2);
        this.setSpring(springLayout,nameLabel,headImage,(int)headImage.getPreferredSize().getHeight()+bua,container,leftBor);
        this.setSpring(springLayout,IDLabel,nameLabel,(int)nameLabel.getPreferredSize().getHeight()+bua,container,leftBor);
        this.setSpring(springLayout,sexLabel,IDLabel,(int)IDLabel.getPreferredSize().getHeight()+bua,container,leftBor);
        this.setSpring(springLayout,ageLabel,sexLabel,(int)sexLabel.getPreferredSize().getHeight()+bua,container,leftBor);
        this.setSpring(springLayout,describeLabel,ageLabel,(int)ageLabel.getPreferredSize().getHeight()+bua,container,leftBor);


        this.setSpring(springLayout,nameField,headImage,(int)headImage.getPreferredSize().getHeight()+bua+5,nameLabel,(int)nameLabel.getPreferredSize().getWidth()+bua);
        this.setSpring(springLayout,IDField,nameLabel,(int)nameLabel.getPreferredSize().getHeight()+5+bua,IDLabel,(int)IDLabel.getPreferredSize().getWidth()+bua);
        this.setSpring(springLayout,sexField,IDLabel,(int)IDLabel.getPreferredSize().getHeight()+5+bua,sexLabel,(int)sexLabel.getPreferredSize().getWidth()+bua);
        this.setSpring(springLayout,ageField,sexLabel,(int)sexLabel.getPreferredSize().getHeight()+5+bua,ageLabel,(int)ageLabel.getPreferredSize().getWidth()+bua);
        this.setSpring(springLayout,describeArea,describeLabel,(int)describeLabel.getPreferredSize().getHeight()+bua,container,leftBor);

        springLayout.putConstraint(SpringLayout.SOUTH,addButton,-leftBor,SpringLayout.SOUTH,container);
        springLayout.putConstraint(SpringLayout.EAST,addButton,-leftBor,SpringLayout.EAST,container);


        this.setStyle(nameField,IDField,ageField,describeArea);
        describeArea.setPreferredSize(new Dimension(width-2*leftBor,70));
        describeArea.setBackground(ColorObj.MAIN_BACKGROUND);
        describeArea.setLineWrap(true);
        JScrollPaneConfig.setUI(jScrollPane);
        container.setBackground(ColorObj.MAIN_BACKGROUND);
        headImage.setBac(ColorObj.LIGHT_WHITE1);



        container.setLayout(springLayout);
        this.setSize(new Dimension(width,height));
        this.setResizable(false);
        this.setLocation((int)(screenSize.getWidth()-width)/2,100);

        this.addComponents(nameLabel,IDLabel,sexLabel,ageLabel,describeLabel,addButton);
        this.addComponents(headImage,nameField,IDField,sexField,ageField,describeArea);
        this.add(container);

        this.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                dispose();
            }
        });
        addButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new FriendRequestController(currentUser.getUser_id());
            }
        });
    }
    public void setSpring(SpringLayout springLayout,JComponent currentComponent,JComponent north,int NorthPad,JComponent west,int WestPad){
        springLayout.putConstraint(SpringLayout.NORTH,currentComponent,NorthPad,SpringLayout.NORTH,north);
        springLayout.putConstraint(SpringLayout.WEST,currentComponent,WestPad,SpringLayout.WEST,west);
    }
    private void addComponents(JComponent... components){
        for (JComponent component : components) {
            container.add(component);
        }
    }
    public void showFrame(User user){
        this.currentUser = user;
        this.dispose();
        this.setIconImage(new ImageIcon("temp/"+ SystemVariable.getCurrentUser().getUser_id()+"/u"+user.getUser_id()+".png").getImage());
        headImage.setImage("/u"+user.getUser_id());
        nameField.setText(user.getUser_name());
        IDField.setText(String.valueOf(user.getUser_id()));

        if (user.getUser_sex().equals("男"))
            sexField.setIcon(new ImageIcon("image/bodyframe/user/m.png"));
        else
            sexField.setIcon(new ImageIcon("image/bodyframe/user/w.png"));

        List<User> friendList = SystemVariable.getFriendList();
        for (User user1 : friendList) {
            if (user1.getUser_id()==user.getUser_id()||user.getUser_id()==SystemVariable.getCurrentUser().getUser_id()){
                addButton.setButtonText("已是好友");
                addButton.setButtonEnable(false);
            }
        }

        ageField.setText(user.getUser_age()+"岁");
        describeArea.setText(user.getUser_describe());

        this.setVisible(true);
    }
    public void setStyle(JTextComponent... components){
        for (JTextComponent component : components) {
            component.setFont(new Font("微软雅黑", Font.PLAIN,16));
            component.setForeground(ColorObj.LIGHT_WHITE4);
            component.setBorder(null);
            component.setEditable(false);
            component.setBackground(ColorObj.MAIN_BACKGROUND);
        }
    }
}
