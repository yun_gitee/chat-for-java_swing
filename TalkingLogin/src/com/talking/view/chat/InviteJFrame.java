package com.talking.view.chat;

import com.sun.scenario.effect.impl.prism.PrImage;
import com.talking.controller.chat.InviteGroupController;
import com.talking.controller.chat.SetCurrentObjUIController;
import com.talking.pojo.CurrentChatObj;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.tool.MsgFileInput;
import com.talking.view.component.*;
import com.talking.view.componentpojo.ColorObj;
import com.talking.view.componentpojo.JScrollPaneConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class InviteJFrame extends JFrame {
    private JPanel container;
    private JPanel userPane;
    private JScrollPane jScrollPane;
    private JPanel operationPane;
    private SpringLayout springLayout;

    private TextLabel allSelect;
    private TextLabel cancel;
    private IButton invite;

    private List<User> notIsGroups;
    private Hashtable<Integer,User> selectUsers;

    private final int height = 500;
    private final int width = 300;
    private Dimension screenSize;

    public InviteJFrame(){
        container = new JPanel();
        userPane = new JPanel();
        jScrollPane = new JScrollPane(userPane);
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();


        operationPane = new JPanel();
        springLayout = new SpringLayout();
        allSelect = new TextLabel("全选");
        cancel = new TextLabel("取消全选");
        invite = new IButton("邀请好友",ColorObj.mainThemeColor,true);

        notIsGroups = new ArrayList<>();
        selectUsers = new Hashtable<>();

        JScrollPaneConfig.setUI(jScrollPane);
        jScrollPane.setPreferredSize(new Dimension(width,1));
        operationPane.setPreferredSize(new Dimension(1,60));
        operationPane.setLayout(springLayout);
        userPane.setLayout(new VerticalFlowLayout());
        userPane.setBackground(ColorObj.MAIN_BACKGROUND);

        this.setSpring(springLayout,allSelect,operationPane,10,operationPane,20);
        this.setSpring(springLayout,cancel,operationPane,10,allSelect,(int)allSelect.getPreferredSize().getWidth()+10);
        springLayout.putConstraint(SpringLayout.NORTH,invite,20,SpringLayout.NORTH,operationPane);
        springLayout.putConstraint(SpringLayout.EAST,invite,-20,SpringLayout.EAST,operationPane);


        this.setTitle("邀请好友");
        this.setSize(new Dimension(width,height));
        this.setResizable(false);
        this.setLocation((int)(screenSize.getWidth()-width)/2,100);
        this.setLayout(new BorderLayout());

        operationPane.add(allSelect);
        operationPane.add(cancel);
        operationPane.add(invite);

        this.add(jScrollPane,BorderLayout.CENTER);
        this.add(operationPane,BorderLayout.SOUTH);

        this.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                dispose();
            }
        });
        invite.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new InviteGroupController(selectUsers);
            }
        });

        allSelect.addMouseListener(new SelectListener());
        cancel.addMouseListener(new SelectListener());
    }

    private class SelectListener extends MouseAdapter{
        public void mouseClicked(MouseEvent e){
            Component[] blockPanel = userPane.getComponents();

            if (e.getSource()==allSelect){

                for (Component component : blockPanel) {
                    ((UserPanel)component).beClicked(true);
                }

                for (User notIsGroup : notIsGroups) {
                    selectUsers.put(notIsGroup.getUser_id(),notIsGroup);
                }

            }else if (e.getSource()==cancel){
                for (Component component : blockPanel) {
                    ((UserPanel)component).beClicked(false);
                }
                selectUsers.clear();
            }
        }
    }

    public void setSpring(SpringLayout springLayout,JComponent currentComponent,JComponent north,int NorthPad,JComponent west,int WestPad){
        springLayout.putConstraint(SpringLayout.NORTH,currentComponent,NorthPad,SpringLayout.NORTH,north);
        springLayout.putConstraint(SpringLayout.WEST,currentComponent,WestPad,SpringLayout.WEST,west);
    }

    public void showPane(){
        userPane.removeAll();
        notIsGroups.removeAll(notIsGroups);

        Hashtable<Integer,User> groupNums = SystemVariable.getCurrentGroupNumbers();
        List<User> allUsers = SystemVariable.getFriendList();

        for (User user : allUsers) {
            if (groupNums.get(user.getUser_id())==null){
                notIsGroups.add(user);
            }
        }

        for (User num : notIsGroups) {
            userPane.add(new UserPanel(num));
        }
        InviteJFrame.this.setVisible(true);
    }

    private class UserPanel extends BlockPanel{
        private HeadImage headImageLabel;           //头像标签
        private JLabel userNameLabel;            //名字标签


        public UserPanel(User user){

            headImageLabel = new HeadImage(30,"/u"+user.getUser_id(),null);
            userNameLabel = new JLabel(user.getUser_name()+"("+user.getUser_id()+")");

            headImageLabel.setLocation(10,5);
            userNameLabel.setBounds(70,10,220,28);
            userNameLabel.setFont(new Font("黑体",Font.PLAIN,18));

            this.setLayout(null);
            this.setAllColor(ColorObj.Spot_Palette1,ColorObj.LIGHT_WHITE1,ColorObj.LIGHT_WHITE1);
            this.setPreferredSize(new Dimension(width-40,40));

            this.add(headImageLabel);
            this.add(userNameLabel);

            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {

                    if (e.getClickCount()==2){
                        beClicked(false);
                        selectUsers.remove(user.getUser_id());
                    }else {
                        beClicked(true);
                        selectUsers.put(user.getUser_id(),user);
                    }

                }
                @Override
                public void mouseEntered(MouseEvent e){
                    setBackground(ColorObj.SPOT_GREEN);
                }
                @Override
                public void mouseExited(MouseEvent e){
                    setBackground(ColorObj.leftPanelColor);
                }
            });
        }
    }
}
