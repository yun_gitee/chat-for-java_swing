package com.talking.view.chat;

import com.talking.controller.friend.DeleteFriendController;
import com.talking.controller.friend.ExitGroupController;
import com.talking.pojo.Group;
import com.talking.view.component.*;
import com.talking.view.componentpojo.BodyFrameObj;
import com.talking.view.componentpojo.ColorObj;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.view.componentpojo.JScrollPaneConfig;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class FriendMsgWindow extends JWindow {
    private JLabel borderLabel;         //阴影边框
    private JPanel contentJPane;        //内容面板
    private CardLayout cardLayout;

    private UserPanel userPanel;            //用户面板
    private GroupPanel groupPanel;          //群面板

    //好友信息面板的属性
    private HeadImage userHeadImage;
    private JTextField userNameField;
    private JLabel userSexLabel;
    private JTextField userAgeField;
    private JTextField userCreateField;
    private JTextArea userDescribeArea;

    //群信息面板的属性
    private HeadImage groupHeadImage;
    private JTextField groupName;
    private JTextField groupID;
    private JPanel groupNumPanel;


    private final int height = 290;
    private final int width = 480;
    private final int shadow_border = 23;
    private final int headSize = 80;
    private final int space = 10;
    private final int left_border = 20;

    private boolean isUser = true;
    private JFrame jFrame;
    private User user;
    private Group group;
    private InviteJFrame inviteJFrame;

    public FriendMsgWindow(JFrame jFrame){
        super(jFrame);
        borderLabel = new JLabel();
        contentJPane = new JPanel();
        cardLayout = new CardLayout();
        userPanel = new UserPanel();
        groupPanel = new GroupPanel();
        inviteJFrame = new InviteJFrame();

        borderLabel.setBorder(new ShadowBorder(20));
        borderLabel.setLayout(null);
        contentJPane.setBounds(shadow_border,shadow_border,width-2*shadow_border,height-2*shadow_border);
        contentJPane.setLayout(cardLayout);

        contentJPane.add(userPanel,"0");
        contentJPane.add(groupPanel,"1");
        borderLabel.add(contentJPane);

        this.setBackground(new Color(0,0,0,0));
        this.setSize(width,height);
        this.add(borderLabel);
        this.setFocusable(true);

        this.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                dispose();
            }
        });
    }

    //好友信息面板
    private class UserPanel extends JPanel{
        private SpringLayout userSpring;
        private JScrollPane describeScroll;
        private IButton deleteButton;


        public UserPanel(){
            userSpring = new SpringLayout();

            userHeadImage = new HeadImage(headSize,null,null);
            userNameField = new JTextField();
            userSexLabel = new JLabel();
            userAgeField = new JTextField();
            userCreateField = new JTextField();
            userDescribeArea = new JTextArea();
            describeScroll = new JScrollPane(userDescribeArea);
            deleteButton = new IButton("删除好友",ColorObj.LIGHT_WHITE4,true);

            this.setBackground(ColorObj.MAIN_BACKGROUND);
            this.setLayout(userSpring);

            describeScroll.setPreferredSize(new Dimension(1,70));
            deleteButton.setPreferredSize(new Dimension(100,30));
            userSexLabel.setPreferredSize(new Dimension(20,20));
            userNameField.setFont(new Font("黑体", Font.PLAIN,20));
            userNameField.setForeground(new Color(114, 114, 114));
            userNameField.setBorder(null);
            userNameField.setEditable(false);
            userNameField.setBackground(ColorObj.MAIN_BACKGROUND);

            JScrollPaneConfig.setUI(describeScroll);
            userDescribeArea.setLineWrap(true);

            userSpring.putConstraint(SpringLayout.NORTH,userHeadImage,left_border,SpringLayout.NORTH,this);
            userSpring.putConstraint(SpringLayout.EAST,userHeadImage,-20,SpringLayout.EAST,this);


            setSpring(userSpring,userNameField,this,left_border,this,left_border);
            setSpring(userSpring,userSexLabel,userNameField,(int)userNameField.getPreferredSize().getHeight()+space,this,left_border);
            setSpring(userSpring,userAgeField,userNameField,(int)userNameField.getPreferredSize().getHeight()+space,userSexLabel,(int)userSexLabel.getPreferredSize().getWidth()+space);
            setSpring(userSpring,userCreateField,userSexLabel,(int)userSexLabel.getPreferredSize().getHeight()+space,this,left_border);
            setSpring(userSpring,describeScroll,userCreateField,(int)userCreateField.getPreferredSize().getHeight()+space,this,left_border);
            userSpring.putConstraint(SpringLayout.EAST,describeScroll,-left_border,SpringLayout.EAST,this);

            userSpring.putConstraint(SpringLayout.SOUTH,deleteButton,-left_border/2,SpringLayout.SOUTH,this);
            userSpring.putConstraint(SpringLayout.EAST,deleteButton,-10,SpringLayout.EAST,this);

            setStyle(userAgeField,userCreateField,userDescribeArea);

            this.add(userHeadImage);
            this.add(userNameField);
            this.add(userSexLabel);
            this.add(userAgeField);
            this.add(userCreateField);
            this.add(describeScroll);
            this.add(deleteButton);

            deleteButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    new DeleteFriendController();
                }
            });
        }

        public void paintComponent(Graphics g){
            super.paintComponent(g);
            g.setColor(ColorObj.LIGHT_WHITE1);
            g.drawLine(20,110,width-46-20,110);
        }
    }

    private void addUserComponent(){
        user = (User) SystemVariable.getCurrentChatObj().getCurrentChatObj();
        userNameField.setText(user.getUser_name()+"("+user.getUser_id()+")");

        userHeadImage.setImage("u"+user.getUser_id());

        if (user.getUser_sex().equals("男"))
            userSexLabel.setIcon(new ImageIcon("image/bodyframe/user/m.png"));
        else
            userSexLabel.setIcon(new ImageIcon("image/bodyframe/user/w.png"));

        userAgeField.setText(user.getUser_age()+"岁");
        userCreateField.setText(String.valueOf(user.getCreate_time()));
        userDescribeArea.setText(user.getUser_describe());

    }

    //群信息面板
    private class GroupPanel extends JPanel{
        private SpringLayout groupSpring;
        private JScrollPane numsScroll;
        private TextButton addNums;
        private TextButton delete;

        private GroupPanel(){
            groupSpring = new SpringLayout();
            groupNumPanel = new JPanel();
            numsScroll = new JScrollPane(groupNumPanel);
            addNums = new TextButton("邀请好友");
            delete = new TextButton("退出群聊");

            addNums.setFont(new Font("宋体",Font.PLAIN,16));
            delete.setFont(new Font("宋体",Font.PLAIN,16));
            delete.setTextColor(ColorObj.LIGHT_WHITE5);

            groupHeadImage = new HeadImage(headSize,null,null);
            groupName = new JTextField();
            groupID = new JTextField();

            this.setBackground(ColorObj.MAIN_BACKGROUND);
            groupNumPanel.setBackground(ColorObj.MAIN_BACKGROUND);
            numsScroll.setBackground(ColorObj.MAIN_BACKGROUND);
            this.setLayout(groupSpring);

            JScrollPaneConfig.setUI(numsScroll);
            groupSpring.putConstraint(SpringLayout.NORTH,groupHeadImage,left_border,SpringLayout.NORTH,this);
            groupSpring.putConstraint(SpringLayout.EAST,groupHeadImage,-20,SpringLayout.EAST,this);
            numsScroll.setPreferredSize(new Dimension(width-46-40,100));
            setSpring(groupSpring,groupName,this,left_border,this,left_border);
            setSpring(groupSpring,groupID,groupName,(int)groupName.getPreferredSize().getHeight()+space,this,left_border);
            setSpring(groupSpring,addNums,groupID,(int)groupID.getPreferredSize().getHeight()+space,this,left_border);
            setSpring(groupSpring,delete,groupID,(int)groupID.getPreferredSize().getHeight()+space,addNums,(int)addNums.getPreferredSize().getWidth()+space);
            setSpring(groupSpring,numsScroll,addNums,(int)addNums.getPreferredSize().getHeight()+2*space,this,left_border);

            setStyle(groupName,groupID);

            this.add(groupHeadImage);
            this.add(groupName);
            this.add(groupID);
            this.add(addNums);
            this.add(delete);
            this.add(numsScroll);

            addNums.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e){
                    inviteJFrame.showPane();
                }
            });
            delete.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    new ExitGroupController();
                }
            });

        }

        public void paintComponent(Graphics g){
            super.paintComponent(g);
            g.setColor(ColorObj.LIGHT_WHITE1);
            g.drawLine(20,110,width-46-20,110);
        }
    }

    private void addGroupComponent(){

        groupNumPanel.removeAll();
        group = (Group) SystemVariable.getCurrentChatObj().getCurrentChatObj();
        Hashtable<Integer,User> nums = SystemVariable.getCurrentGroupNumbers();

        groupHeadImage.setImage("/g"+group.getGroup_id());
        groupName.setText(group.getGroup_name()+"("+nums.size()+"人)");
        groupID.setText(String.valueOf(group.getGroup_id()));

//        if ()



        List<User> numList = new ArrayList<>();
        Set numKeys = nums.keySet();
        for (Object numKey : numKeys) {
            numList.add(nums.get(numKey));
        }

        int count = numList.size()/8;
        int mod = numList.size()%8;
        if (mod!=0)
            count++;

        if (count==1)
            count++;
        groupNumPanel.setLayout(new GridLayout(count,8));

        for (int i = 0; i < numList.size(); i++) {
            UserBlock userBlock = new UserBlock(45,numList.get(i),15,12);
            groupNumPanel.add(userBlock);
        }

        for (int i = 0; i < count*8-numList.size(); i++) {
            JLabel jLabel = new JLabel();
            jLabel.setSize(45,45);
            groupNumPanel.add(jLabel);
        }

    }

    public void showWindow(){
        this.dispose();
        jFrame = BodyFrameObj.getBodyFrame();
        this.setLocation(jFrame.getX()+jFrame.getWidth()-width,jFrame.getY()+60);

        isUser = SystemVariable.getCurrentChatObj().isUser();
        if (isUser){
            cardLayout.show(contentJPane,"0");
            this.addUserComponent();
        }else {
            cardLayout.show(contentJPane,"1");
            this.addGroupComponent();
        }

        this.setVisible(true);
    }

    public void setSpring(SpringLayout springLayout,JComponent currentComponent,JComponent north,int NorthPad,JComponent west,int WestPad){
        springLayout.putConstraint(SpringLayout.NORTH,currentComponent,NorthPad,SpringLayout.NORTH,north);
        springLayout.putConstraint(SpringLayout.WEST,currentComponent,WestPad,SpringLayout.WEST,west);
    }

    public void setStyle(JComponent... components){
        for (JComponent component : components) {
            component.setFont(new Font("微软雅黑", Font.PLAIN,16));
            component.setForeground(new Color(147, 147, 147));
            component.setBorder(null);
            ((JTextComponent)component).setEditable(false);
            component.setBackground(ColorObj.MAIN_BACKGROUND);
        }
    }
}
