package com.talking.view.component;

import javax.swing.*;
import java.awt.*;

public class RedDot extends JLabel {
    private int size = 10;
    public RedDot(){
        this.setPreferredSize(new Dimension(size,size));
        this.setSize(size,size);
    }

    public void paintComponent(Graphics g){
        g.setColor(Color.red);

        Graphics2D g1 = (Graphics2D) g;
        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);         //抗锯齿

        g1.fillOval(0,0,size,size);
    }
}
