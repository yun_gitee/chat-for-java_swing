package com.talking.view.component;

import com.talking.controller.bodyframe.ExitSystemController;
import com.talking.pojo.SystemVariable;
import com.talking.view.componentpojo.BodyFrameObj;
import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ISystemTray extends WindowAdapter {
    private JDialog jDialog;
    private ContentPanel content;
    private BlockPanel userMessBlock;
    private BlockPanel openSoft;
    private BlockPanel exitSoft;

    private  SystemTray systemTray;
    private TrayIcon trayIcon;
    private String userName;
    private int userId;

    private final int width = 100;
    private final int height = 140;
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();         //获取屏幕尺寸
    private boolean isSupported = SystemTray.isSupported();             //判断当前系统是否支持系统托盘


    public ISystemTray(){
        if (isSupported){
        userName = SystemVariable.getCurrentUser().getUser_name();
        userId = SystemVariable.getCurrentUser().getUser_id();
        systemTray=SystemTray.getSystemTray();       //获取系统托盘
        trayIcon=new TrayIcon(Toolkit.getDefaultToolkit().getImage("image/logo/logo120x120.png"),null,null);
        trayIcon.setImageAutoSize(true);

        jDialog = new JDialog();
        content = new ContentPanel();

        jDialog.setSize(new Dimension(width,height));
        jDialog.setAlwaysOnTop(true);

        jDialog.setUndecorated(true);
        jDialog.setBackground(ColorObj.MAIN_BACKGROUND);

        jDialog.add(content);

        trayIcon.addMouseListener(new MouseListenerList());
        jDialog.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                content.removeAll();

                addComponent(content);
                jDialog.dispose();
            }
        });
        }else
            JOptionPane.showMessageDialog(null, "当前系统系统暂不支持系统托盘！", "错误提示", JOptionPane.WARNING_MESSAGE);
    }
    private void addComponent(ContentPanel component){
        openSoft.setAllColor(ColorObj.MAIN_BACKGROUND,ColorObj.Spot_Palette4,ColorObj.MAIN_BACKGROUND);
        exitSoft.setAllColor(ColorObj.MAIN_BACKGROUND,ColorObj.Spot_Palette4,ColorObj.MAIN_BACKGROUND);

        component.add(userMessBlock);
        component.add(openSoft);
        component.add(exitSoft);
    }

    //内容面板
    private class ContentPanel extends JPanel{
        private JLabel nameLabel;
        private JLabel idLabel;

        public ContentPanel(){
            userMessBlock = new BlockPanel();
            openSoft = new BlockPanel("打开软件");
            exitSoft = new BlockPanel("退出");
            nameLabel = new JLabel(userName);
            idLabel = new JLabel("("+userId+")");

            userMessBlock.setPreferredSize(new Dimension(width,60));
            nameLabel.setFont(new Font("黑体",Font.PLAIN,16));
            idLabel.setFont(new Font("宋体",Font.PLAIN,14));
            idLabel.setForeground(ColorObj.LIGHT_WHITE5);
            openSoft.setPreferredSize(new Dimension(width,30));
            exitSoft.setPreferredSize(new Dimension(width,30));

            this.setLayout(new VerticalFlowLayout());
            this.setBorder(BorderFactory.createLineBorder(Color.GRAY));
            this.setBackground(ColorObj.GENERIC1);
            userMessBlock.setLayout(new GridLayout(2,1));
            userMessBlock.add(nameLabel);
            userMessBlock.add(idLabel);

            addComponent(this);
            try {
                systemTray.add(trayIcon);
            }catch (Exception e1){
                JOptionPane.showMessageDialog(null, "系统托盘失败！！", "错误提示", JOptionPane.WARNING_MESSAGE);
            }

            openSoft.addMouseListener(new MouseListenerList());
            exitSoft.addMouseListener(new MouseListenerList());
        }

        public void paintComponent(Graphics g){
            g.setColor(ColorObj.LIGHT_WHITE2);
            g.drawLine(5,66,width-10,66);
        }
    }

    //所有鼠标的点击事件集合
    private class MouseListenerList extends MouseAdapter{
        @Override
        public void mouseClicked(MouseEvent e) {
            Object source = e.getSource();
            if (source==openSoft)
                BodyFrameObj.getBodyFrame().setVisible(true);
            else if (source==exitSoft)
                new ExitSystemController().exit();
            else if (source==trayIcon)
                if (e.getButton()==MouseEvent.BUTTON3){
                    int x = e.getX();
                    int y = e.getY();

                    if (x+width>screenSize.getWidth())
                        x = x-width;
                    if (y+height>screenSize.getHeight())
                        y = y-2*height;

                    jDialog.setLocation(x,y);
                    jDialog.setVisible(true);
                }
                else if (e.getClickCount()==2) {
                    BodyFrameObj.getBodyFrame().setVisible(true);
                }

        }
    }
}
