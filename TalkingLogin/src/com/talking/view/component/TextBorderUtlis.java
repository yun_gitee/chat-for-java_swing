package com.talking.view.component;

//需要新增红色警示功能

import javax.swing.border.LineBorder;
import java.awt.*;

public class TextBorderUtlis extends LineBorder {

    private static final long serialVersionUID = 1L;
    private int arc;

    public TextBorderUtlis(Color color,int arc){
        super(color, 2, true);
        this.arc = arc;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height){

        Color oldColor = g.getColor();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        int i;
        g2.setColor(lineColor);

        for (i = 0; i < 1; i++){
            if (!roundedCorners){
                g2.drawRect(x + i, y + i, width - i - i - 1, height - i - i - 1);
            }else{
                g2.drawRoundRect(x + i, y + i, width - i - i - 1, height - i - i - 1, arc, arc);
            }
        }
        g2.setColor(oldColor);
    }

}
