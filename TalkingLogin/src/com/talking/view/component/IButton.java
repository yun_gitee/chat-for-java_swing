package com.talking.view.component;

//需要新增不可选择功能
import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;

public class IButton extends JButton {
    private boolean enable;
    private Color color;

    public IButton(String text,Color color,boolean enable){
        super(text);
        this.enable = enable;
        this.color = color;

        this.setForeground(ColorObj.MAIN_BACKGROUND);
        this.setBorder(null);
        this.setFont(new Font("微软雅黑",Font.PLAIN,18));
        this.setContentAreaFilled(false);
        this.setFocusPainted(false);        //去除文字周围的虚线框
        this.setEnabled(enable);

    }
    public void setButtonText(String text){
        this.setText(text);
    }
    public void setButtonEnable(boolean enable){
        this.setEnabled(enable);
        this.setForeground(ColorObj.MAIN_BACKGROUND);
    }

    @Override
    public void paintComponent(Graphics g){

        Graphics2D g1 =(Graphics2D) g;  //抗锯齿
        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

        if (this.getModel().isArmed()) {
                g1.setColor(ColorObj.buttonReleasedColor);    //按下后按钮变成灰色

        } else {
            if (enable)
                g1.setColor(color);
            else
                g1.setColor(ColorObj.buttonReleasedColor);
        }
        g1.fillRoundRect(0, 0, this.getSize().width-1, this.getSize().height-1,20,20);//填充圆角矩形边界
        // 这个调用会画一个标签和焦点矩形。
        super.paintComponent(g);
    }
}
