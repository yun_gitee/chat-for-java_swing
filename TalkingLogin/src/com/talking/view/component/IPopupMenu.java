package com.talking.view.component;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class IPopupMenu extends JWindow {
    private JPanel content;

    private final int WIDTH = 100;
    private final int BLOCK_HEIGHT = 30;
    private List<BlockPanel> items;

    public IPopupMenu(JFrame jFrame){
        super(jFrame);
        content = new JPanel();
        items = new ArrayList<>();

        content.setBorder(BorderFactory.createLineBorder(ColorObj.LIGHT_WHITE5));
        content.setBackground(ColorObj.MAIN_BACKGROUND);

        this.setLayout(new BorderLayout());
        this.setBackground(new Color(0,0,0,0));
        this.setFocusable(true);
        this.setAlwaysOnTop(true);
        content.setLayout(new VerticalFlowLayout());


        this.add(content,BorderLayout.CENTER);
        this.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowLostFocus(WindowEvent e) {
                dispose();
            }
        });

    }
    public void setItem(String... item){
//        this.setLayout(new GridLayout(item.length,1));
        int height = 0;
        for (String s : item) {
            BlockPanel blockPanel = new BlockPanel(s);
            blockPanel.setArc(10);
            blockPanel.setAllColor(ColorObj.GENERIC3,ColorObj.lightThemeColor,ColorObj.MAIN_BACKGROUND);
            blockPanel.setPreferredSize(new Dimension(WIDTH-10,BLOCK_HEIGHT));
            content.add(blockPanel);
            items.add(blockPanel);
            height = height+40;
        }
        this.setSize(new Dimension(WIDTH,height));
    }
    public void setMouseListener(MouseAdapter... adapters){
        List<MouseAdapter> allAdapter = new ArrayList<>();

        for (MouseAdapter adapter : adapters) {
            allAdapter.add(adapter);
        }

        int temp = 0;
        for (BlockPanel item : items) {
            item.addMouseListener(allAdapter.get(temp));
            temp++;
        }
    }
    public void setWindowListener(){
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Point point = MouseInfo.getPointerInfo().getLocation();
                int x = (int) point.getX();
                int y = (int) point.getY();
                int x1= e.getX();
                int y1 = e.getY();

                if (!(x1>x&&x1<x+getWidth()&&y1>y&&y1<y+getHeight()))
                    dispose();

                System.out.println("所有坐标是："+x+","+y+","+x1+","+y1);
            }
        });
    }

    public void showMenu(){
        this.dispose();
        this.requestFocus();
        Point point = MouseInfo.getPointerInfo().getLocation();
        int x = (int) point.getX();
        int y = (int) point.getY();
        this.setLocation(x,y);
        this.setVisible(true);
        System.out.println("右键弹窗了");
    }
}
