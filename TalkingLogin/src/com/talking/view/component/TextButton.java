package com.talking.view.component;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TextButton extends JLabel {
    private Color color;
    public TextButton(String label){
        super(label);
        this.setForeground(ColorObj.buttonText);
        color = ColorObj.buttonText;
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                setForeground(ColorObj.LIGHT_WHITE3);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                setForeground(color);
            }
        });
    }
    public void setTextColor(Color color){
        this.setForeground(color);
        this.color = color;
    }
}
