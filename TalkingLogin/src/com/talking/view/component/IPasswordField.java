package com.talking.view.component;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class IPasswordField extends JLabel{
    private String tips;
    private JPasswordField jPasswordField;
    private EyeLabel enableVisit;
    private int width;
    private int height;

    public IPasswordField(String tips,int x,int y,int width,int height){
        this.tips = tips;
        this.width = width;
        this.height = height;

        jPasswordField = new JPasswordField();
        enableVisit = new EyeLabel(width-40,0,height,height);

        jPasswordField.setBorder(null);
        this.setLayout(null);
        this.setBounds(x,y,width,height);
        this.setBorder(new TextBorderUtlis(ColorObj.buttonReleasedColor,30));
        jPasswordField.setEchoChar((char)0);
        jPasswordField.setFocusable(false);


        jPasswordField.setBounds(10,4,width-40,height-8);
        jPasswordField.setFont(new Font("宋体",Font.PLAIN,14));
        jPasswordField.setForeground(ColorObj.buttonReleasedColor);
        jPasswordField.setText(this.tips);

        this.add(jPasswordField);
        this.add(enableVisit);

        jPasswordField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jPasswordField.setFocusable(true);
                jPasswordField.requestFocus();
            }
        });
        jPasswordField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                IPasswordField.this.setBorder(new TextBorderUtlis(ColorObj.mainThemeColor,30));
                if (String.valueOf(jPasswordField.getPassword()).trim().equals(tips)) {
                    jPasswordField.setEchoChar('●');
                    jPasswordField.setText("");
                    jPasswordField.setForeground(Color.black);
                }

            }

            @Override
            public void focusLost(FocusEvent e) {
                if (String.valueOf(jPasswordField.getPassword()).trim().equals("") || String.valueOf(jPasswordField.getPassword()).trim().equals(tips)) {
                    IPasswordField.this.setBorder(new TextBorderUtlis(ColorObj.buttonReleasedColor,30));
                    jPasswordField.setForeground(ColorObj.buttonReleasedColor);
                    jPasswordField.setEchoChar((char) 0);
                    jPasswordField.setText(tips);
                }else {
                    IPasswordField.this.setBorder(new TextBorderUtlis(ColorObj.mainThemeColor,30));
                    jPasswordField.setForeground(Color.black);
                }
            }
        });
    }
    private class EyeLabel extends JLabel{
        private Color color = ColorObj.buttonReleasedColor;
        private boolean bePressed = false;

        public EyeLabel(int x,int y,int width,int height){
            this.setBounds(x,y,width,height);

            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    if (!(String.valueOf(jPasswordField.getPassword()).trim().equals(tips))) {
                        bePressed = true;
                        jPasswordField.setEchoChar((char) 0);
                        color = ColorObj.mainThemeColor;
                        EyeLabel.this.repaint();
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (bePressed) {
                        bePressed = false;
                        jPasswordField.setEchoChar('●');
                        color = ColorObj.buttonReleasedColor;
                        EyeLabel.this.repaint();
                    }
                }
            });


        }
        public void paintComponent(Graphics g){
            Graphics2D g1 = (Graphics2D) g;
            g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
            g.setColor(color);

//            g.drawOval(13,13,14,14);
            g.drawArc(13,13,14,10,0,180);
            g.fillOval(17,17,6,6);
        }
    }
    public String getTheText(){
        return String.valueOf(jPasswordField.getPassword());
    }
}
