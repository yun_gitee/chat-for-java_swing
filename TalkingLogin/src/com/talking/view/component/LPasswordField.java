package com.talking.view.component;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

public class LPasswordField extends JLabel {
    private JLabel jLabel;
    private JPasswordField jPasswordField;
    private EyeLabel eyeLabel;
    private SpringLayout springLayout;

    private String placeHolder;
    private int width;
    private int height;
    private final int LEFT_SPACE = 20;
    private final int TOP_SPACE = 2;
    private Color BACKGROUND_COLOR = ColorObj.LIGHT_WHITE2;
    private final char charWord = '•';

    public LPasswordField(String placeHolder, int x, int y, int width, int height) {
        this.placeHolder = placeHolder;
        this.width = width;
        this.height = height;

        int width1 = width - 2 * LEFT_SPACE;
        int height1 = height - 2 * TOP_SPACE;
        springLayout = new SpringLayout();
        jLabel = new JLabel(placeHolder);
        jPasswordField = new JPasswordField();


        eyeLabel = new EyeLabel(height,height);
        this.setSpringLayout(eyeLabel,width1,0);

        this.placeHolder = placeHolder;
        this.setBounds(x, y, width, height);
        this.setLayout(springLayout);
//        this.setBorder(new TextBorderUtlis(ColorObj.buttonReleasedColor, 30));

        //提示文字的样式
        jLabel.setPreferredSize(new Dimension(width1 - 6-height, height1));
        jLabel.setFont(new Font("微软雅黑", Font.PLAIN, 14));
        jLabel.setForeground(ColorObj.LIGHT_WHITE7);
        this.setSpringLayout(jLabel, LEFT_SPACE + 3, TOP_SPACE);

        jPasswordField.setBorder(null);
        jPasswordField.setFocusable(false);
        jPasswordField.setFont(new Font("微软雅黑", Font.BOLD, 14));
        jPasswordField.setPreferredSize(new Dimension(width1-height, height1));
        jPasswordField.setBackground(BACKGROUND_COLOR);
        jPasswordField.setEchoChar(charWord);
        this.setSpringLayout(jPasswordField, LEFT_SPACE, TOP_SPACE);

        this.add(jLabel);
        this.add(jPasswordField);
        this.add(eyeLabel);
        this.setTextFieldListener();
        jPasswordField.getDocument().addDocumentListener(new ContentListener());
    }

    private void setTextFieldListener() {
        jPasswordField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                BACKGROUND_COLOR = ColorObj.LIGHT_WHITE4;
                jPasswordField.setBackground(BACKGROUND_COLOR);
                repaint();
            }

            @Override
            public void focusLost(FocusEvent e) {
                String content = String.valueOf(jPasswordField.getPassword());
                if (content.length() == 0) {
                    BACKGROUND_COLOR = ColorObj.LIGHT_WHITE2;
                    jPasswordField.setBackground(BACKGROUND_COLOR);
                    repaint();
                }
            }
        });

        jPasswordField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                jLabel.setText("");
                System.out.println("触发了");

            }

            @Override
            public void keyTyped(KeyEvent e) {
                int length = jPasswordField.getText().length();
                if (length == 0)
                    jLabel.setText(placeHolder);
            }
        });

        jPasswordField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jPasswordField.setFocusable(true);
                jPasswordField.requestFocus();
            }
        });
    }

    /**
     * 函数Spring配置
     */
    private void setSpringLayout(JComponent component, int westPad, int northPad) {
        springLayout.putConstraint(SpringLayout.WEST, component, westPad, SpringLayout.WEST, this);
        springLayout.putConstraint(SpringLayout.NORTH, component, northPad, SpringLayout.NORTH, this);
    }

    public String getContent() {
        return String.valueOf(jPasswordField.getPassword());
    }

    public void paintComponent(Graphics g) {
        g.setColor(BACKGROUND_COLOR);
        Graphics2D g1 = (Graphics2D) g;
        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g1.fillRoundRect(0, 0, width, height, 40, 40);
    }

    private class ContentListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            System.out.println("插入监听");
            jLabel.setText("");
        }

        @Override
        public void removeUpdate(DocumentEvent e) {

        }

        @Override
        public void changedUpdate(DocumentEvent e) {

        }
    }

    private class EyeLabel extends JLabel{
        private Color color = ColorObj.mainThemeColor;
        private boolean bePressed = false;

        public EyeLabel(int width,int height){
            this.setPreferredSize(new Dimension(width,height));

            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    if (!(String.valueOf(jPasswordField.getPassword()).trim().equals(placeHolder))) {
                        bePressed = true;
                        jPasswordField.setEchoChar((char) 0);
                        color = ColorObj.LIGHT_WHITE6;
                        EyeLabel.this.repaint();
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (bePressed) {
                        bePressed = false;
                        jPasswordField.setEchoChar(charWord);
                        color = ColorObj.mainThemeColor;
                        EyeLabel.this.repaint();
                    }
                }
            });


        }
        public void paintComponent(Graphics g){
            Graphics2D g1 = (Graphics2D) g;
            g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
            g.setColor(color);

//            g.drawOval(13,13,14,14);
            g.drawArc(13,13,14,10,0,180);
            g.fillOval(17,17,6,6);
        }
    }
}
