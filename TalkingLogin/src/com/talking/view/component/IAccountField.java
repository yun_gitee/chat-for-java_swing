package com.talking.view.component;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
/**
*自定义文本输入框
*/
public class IAccountField extends JLabel {
    private String name;
    private JTextField jTextField;

    public IAccountField(String name,int x,int y,int width,int height){
        this.name = name;
        jTextField = new JTextField();

        jTextField.setBorder(null);
        this.setLayout(null);
        this.setBounds(x,y,width,height);

        jTextField.setFocusable(false);
        jTextField.setBounds(10,4,width-20,height-8);
        jTextField.setFont(new Font("宋体",Font.BOLD,14));
        jTextField.setForeground(ColorObj.buttonReleasedColor);
        jTextField.setText(this.name);
        this.setBorder(new TextBorderUtlis(ColorObj.buttonReleasedColor,30));

        this.add(jTextField);

        jTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jTextField.setFocusable(true);
                jTextField.requestFocus();
            }
        });

        jTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                IAccountField.this.setBorder(new TextBorderUtlis(ColorObj.mainThemeColor,30));
                if (jTextField.getText().trim().equals(name))
                    jTextField.setText("");
                    jTextField.setForeground(Color.black);

             }

            @Override
            public void focusLost(FocusEvent e) {
                if (jTextField.getText().trim().equals("") || jTextField.getText().trim().equals(name)) {
                    IAccountField.this.setBorder(new TextBorderUtlis(ColorObj.buttonReleasedColor,30));
                    jTextField.setForeground(ColorObj.buttonReleasedColor);
                    jTextField.setText(name);
                }else {
                    IAccountField.this.setBorder(new TextBorderUtlis(ColorObj.mainThemeColor,30));
                    jTextField.setForeground(Color.black);
                }
            }
        });
    }
    public String getTheText(){
        return jTextField.getText();
    }
}
