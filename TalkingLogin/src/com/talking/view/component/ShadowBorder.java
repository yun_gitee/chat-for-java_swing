package com.talking.view.component;

import javax.swing.border.Border;
import java.awt.*;

public class ShadowBorder implements Border {
    private static final int arcPix = 10;        //圆角

    private static final int TOP_OPACITY = 40;   //最大透明度值

    private int pixels;                    //阴影有多宽

    public ShadowBorder(int pixels){
        this.pixels = pixels;
    }



    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        double opacity = 0;
        String osName = System.getProperty("os.name").toLowerCase();
        Graphics2D g1 = (Graphics2D) g;
        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        if (!osName.contains("ubuntu")) {
            for (int i = 0; i < pixels; i++) {
//            opacity = TOP_OPACITY * ((double) 1 / (double) (i==0?1:i));
//            opacity = TOP_OPACITY * ((double) 1 / Math.log((i==0?1:i)+1));
                opacity = TOP_OPACITY * ((double) 1 / ((double) (i == 0 ? 1 : i) / 2));
//            opacity = TOP_OPACITY *
                Color color = new Color(168, 168, 168);
                g1.setColor(new Color(168, 168, 168, (int) opacity));
                g1.drawRoundRect(pixels - i + 1, pixels - i + 1, width - 2 * (pixels - i + 1), height - 2 * (pixels - i + 1), arcPix, arcPix);
            }
        }
    }

    public Insets getBorderInsets(Component c) {
        return new Insets(0,0,0,0);
    }
    public boolean isBorderOpaque() {
        return false;
    }
}
