package com.talking.view.component;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SearchPanel extends JPanel{
    private String name;
    public JTextField jTextField;
    private JLabel searchButton;
    private Color color;

    public SearchPanel(String name, int width, int height, MouseAdapter mouseAdapter, Color color){
        this.name = name;
        this.color = color;
        jTextField = new JTextField();
        searchButton = new JLabel(new ImageIcon("image/component/searchButton.png"));

        this.setBackground(new Color(0,0,0,0));
        jTextField.setBorder(null);
        this.setLayout(null);
        this.setPreferredSize(new Dimension(width,height));
        this.setSize(new Dimension(width,height));
        searchButton.setBounds(width-55,9,32,32);

        jTextField.setBounds(25,6,width-85,height-12);
        jTextField.setFont(new Font("宋体",Font.PLAIN,14));
        jTextField.setForeground(ColorObj.MAIN_BACKGROUND);
        jTextField.setText(this.name);
        jTextField.setBackground(color);
        this.add(jTextField);
        this.add(searchButton);

        searchButton.addMouseListener(mouseAdapter);
        searchButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                searchButton.setIcon(new ImageIcon("image/component/searchButton1.png"));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                searchButton.setIcon(new ImageIcon("image/component/searchButton.png"));
            }
        });

        jTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                if (jTextField.getText().trim().equals(name))
                    jTextField.setText("");
                    jTextField.setForeground(Color.black);
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (jTextField.getText().trim().equals("") || jTextField.getText().trim().equals(name)) {
                    jTextField.setText(name);
                    jTextField.setForeground(ColorObj.MAIN_BACKGROUND);
                }

            }
        });
    }
    public void setContent(String text){
        jTextField.setText(text);
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(color);
        Graphics2D g1 = (Graphics2D) g;
        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        g1.fillRoundRect(5,5,this.getWidth()-10,this.getHeight()-10,40,40);
    }
}
