package com.talking.view.component;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;

public class TextLabel extends JPanel {
    private JLabel jLabel;
    public TextLabel(String text){
        jLabel = new JLabel(text);
        jLabel.setBorder(null);
        jLabel.setForeground(ColorObj.MAIN_BACKGROUND);
        jLabel.setFont(new Font("宋体", Font.PLAIN,16));

        this.add(jLabel);
    }
    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);         //抗锯齿

        g2.setColor(ColorObj.Spot_Palette1);
        g2.fillRoundRect(0,0,getWidth(),getHeight(),20,20);
    }
}
