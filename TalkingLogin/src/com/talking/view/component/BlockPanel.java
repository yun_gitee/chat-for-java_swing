package com.talking.view.component;


import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class BlockPanel extends JPanel {
    private JComponent parent;
    private JLabel label;

    private Color tempColor;
    private Color clickColor;
    private Color enterColor;
    private Color exitColor;

    public boolean clicked;
    private int arcSize = 0;

    public BlockPanel(String labelText){
        this();
        label = new JLabel("<html>"+labelText+"</html>");
        label.setFont(new Font("微软雅黑", Font.PLAIN,16));
        this.addComponents(label);
    }

    public BlockPanel(){

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (!clicked) {
                    tempColor = enterColor;
                    parent =(JComponent) getParent();

                    while(!parent.getUIClassID().equals("PanelUI")){
                        parent = (JComponent) parent.getParent();
                    }

                    parent.updateUI();
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (!clicked) {
                    tempColor = exitColor;
                    parent =(JComponent) getParent();

                    while(!parent.getUIClassID().equals("PanelUI")){
                        parent = (JComponent) parent.getParent();
                    }

                    parent.updateUI();
                }
            }
        });
    }

    public void setLabelColor(Color color){
        label.setForeground(color);
    }

    public void paintComponent(Graphics g){
        int tempArc = arcSize==0?20:arcSize;

        //使用双缓冲技术，将图形一次性写入到组件中
        BufferedImage imageBuffer = new BufferedImage(getWidth(),getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g1 = imageBuffer.createGraphics();  //把它的画笔拿过来,给gImage保存着

        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);         //抗锯齿

        g1.setColor(tempColor);
        g1.fillRoundRect(0,0,getWidth(),getHeight(),tempArc,tempArc);
        g1.dispose();

        g.drawImage(imageBuffer,0,0,null);

    }

    public void beClicked(boolean clicked){
        this.clicked = clicked;

        if (clicked)
            tempColor = clickColor;
        else
            tempColor = exitColor;
        parent =(JComponent) getParent();
        if (parent!=null)
            parent.updateUI();
    }

    public void setAllColor(Color clickColor,Color enterColor,Color exitColor){
        this.clickColor = clickColor;
        this.tempColor = exitColor;
        this.enterColor = enterColor;
        this.exitColor = exitColor;
    }

    public void setArc(int arc){
        this.arcSize = arc;
    }

    public void addComponents(JComponent... components){
        for (JComponent component : components) {
            this.add(component);
        }
    }
}
