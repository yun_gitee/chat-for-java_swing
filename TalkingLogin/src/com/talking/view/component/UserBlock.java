package com.talking.view.component;

import com.talking.pojo.User;
import com.talking.view.chat.AccountMessFrame;
import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class UserBlock extends BlockPanel{
    private AccountMessFrame accountMessFrame;

    public UserBlock(int size, User user, int textHeight,int fontSize){
        accountMessFrame = new AccountMessFrame();
        JLabel head = new JLabel();
        HeadImage headImage = new HeadImage(size-textHeight-10,"/u"+user.getUser_id(),null);
        JLabel jLabel = new JLabel(user.getUser_name(),JLabel.CENTER);
        jLabel.setForeground(ColorObj.LIGHT_WHITE6);
        jLabel.setFont(new Font("宋体",Font.PLAIN,fontSize));
        jLabel.setPreferredSize(new Dimension(size,textHeight));
        jLabel.setSize(new Dimension(size,textHeight));

        head.setBounds(0,0,size,size-textHeight-10);
        headImage.setBac(ColorObj.LIGHT_WHITE1);
        jLabel.setBounds(0,size-textHeight,size,textHeight);

        this.setSize(new Dimension(size,size));
        this.setLayout(null);
        head.setPreferredSize(new Dimension(size-textHeight,size-textHeight));
        head.setLayout(new FlowLayout());

        head.add(headImage);
        this.add(head);
        this.add(jLabel);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                accountMessFrame.showFrame(user);
            }
        });
    }
}
