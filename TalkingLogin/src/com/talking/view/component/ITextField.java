package com.talking.view.component;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

/**
 * 自定义的单行文本框
 * 主要实现文字提示
 * 与外观改变
 */
public class ITextField extends JLabel {
    private JLabel jLabel;
    private JTextField jTextField;
    private SpringLayout springLayout;

    private String placeHolder;
    private int width;
    private int height;
    private final int LEFT_SPACE = 20;
    private final int TOP_SPACE = 2;
    private Color BACKGROUND_COLOR = ColorObj.LIGHT_WHITE2;

    public ITextField(String placeHolder, int x, int y, int width, int height) {
        this.placeHolder = placeHolder;
        this.width = width;
        this.height = height;

        int width1 = width - 2 * LEFT_SPACE;
        int height1 = height - 2 * TOP_SPACE;
        springLayout = new SpringLayout();
        jLabel = new JLabel(placeHolder);
        jTextField = new JTextField();

        this.placeHolder = placeHolder;
        this.setBounds(x, y, width, height);
        this.setLayout(springLayout);
//        this.setBorder(new TextBorderUtlis(ColorObj.buttonReleasedColor, 30));

        //提示文字的样式
        jLabel.setPreferredSize(new Dimension(width1-6, height1));
        jLabel.setFont(new Font("微软雅黑",Font.PLAIN,14));
        jLabel.setForeground(ColorObj.LIGHT_WHITE7);
        this.setSpringLayout(jLabel, LEFT_SPACE+3, TOP_SPACE);

        jTextField.setBorder(null);
        jTextField.setFocusable(false);
        jTextField.setFont(new Font("Times New Roman", Font.BOLD, 14));
        jTextField.setPreferredSize(new Dimension(width1, height1));
        jTextField.setBackground(BACKGROUND_COLOR);
        this.setSpringLayout(jTextField, LEFT_SPACE, TOP_SPACE);

        this.add(jLabel);
        this.add(jTextField);
        this.setTextFieldListener();
        jTextField.getDocument().addDocumentListener(new ContentListener());
    }

    private void setTextFieldListener(){
        jTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                BACKGROUND_COLOR = ColorObj.LIGHT_WHITE4;
                jTextField.setBackground(BACKGROUND_COLOR);
                repaint();
            }

            @Override
            public void focusLost(FocusEvent e) {
                String content = jTextField.getText().trim();
                if (content.length() == 0) {
                    BACKGROUND_COLOR = ColorObj.LIGHT_WHITE2;
                    jTextField.setBackground(BACKGROUND_COLOR);
                    repaint();
                }
            }
        });

        jTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                jLabel.setText("");
                System.out.println("触发了");

            }

            @Override
            public void keyTyped(KeyEvent e){
                int length = jTextField.getText().length();
                if (length==0)
                    jLabel.setText(placeHolder);
            }
        });

        jTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jTextField.setFocusable(true);
                jTextField.requestFocus();
            }
        });
    }

    /**
     * 函数Spring配置
     */
    private void setSpringLayout(JComponent component, int westPad, int northPad) {
        springLayout.putConstraint(SpringLayout.WEST, component, westPad, SpringLayout.WEST, this);
        springLayout.putConstraint(SpringLayout.NORTH, component, northPad, SpringLayout.NORTH, this);
    }

    public String getContent() {
        return jTextField.getText();
    }
    public void paintComponent(Graphics g){
        g.setColor(BACKGROUND_COLOR);
        Graphics2D g1 = (Graphics2D) g;
        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        g1.fillRoundRect(0,0,width,height,40,40);
    }

    private class ContentListener implements DocumentListener{

        @Override
        public void insertUpdate(DocumentEvent e) {
            System.out.println("插入监听");
            jLabel.setText("");
        }

        @Override
        public void removeUpdate(DocumentEvent e) {

        }

        @Override
        public void changedUpdate(DocumentEvent e) {

        }
    }
}

