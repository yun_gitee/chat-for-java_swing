package com.talking.view.login;

import com.talking.controller.login.LoginController;
import com.talking.view.component.*;
import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LoginPanel extends JPanel {
    private JLabel theName_JLabel;                          //登录标签
    private ITextField theAccount_IAccountFiled;         //帐号文本行
    private LPasswordField thePassword_IPasswordField;      //密码框
    private JCheckBox rememberButton_JCheckBox;         //记住密码
    private JCheckBox autoLoginButton_JCheckBox;        //自动登录
    private JLabel registerButton_JLabel;                //注册按钮
    private JLabel splitLabel_JLabel;                   //分隔符“|”
    private JLabel retrievePSW_JLabel;                  //找回密码
    private IButton theLogin_JButton;               //登录按钮

    private int leftBorder = 20;
    private int topBorder = 0;
    private int height = 40;
    private int width = 240;
    private int pixels = 30;

    public LoginPanel(){

        this.setBackground(ColorObj.MAIN_BACKGROUND);
        this.setLayout(null);

        theName_JLabel = new JLabel("登录");
        theAccount_IAccountFiled = new ITextField("帐号/邮箱",leftBorder,topBorder+80,width,40);
        thePassword_IPasswordField = new LPasswordField("密码",leftBorder,topBorder+140,width,40);
        theLogin_JButton = new IButton("登录", ColorObj.mainThemeColor,true);
        rememberButton_JCheckBox = new JCheckBox("记住密码");
        autoLoginButton_JCheckBox = new JCheckBox("自动登录");
        registerButton_JLabel = new JLabel("注册");
        splitLabel_JLabel = new JLabel("|");
        retrievePSW_JLabel = new JLabel("找回密码");

        theName_JLabel.setBounds(leftBorder,topBorder,140,40);
        theName_JLabel.setFont(new Font("微软雅黑",Font.PLAIN,20));
        theLogin_JButton.setBounds(170,350,100,height);
        registerButton_JLabel.setBounds(20,350,40,height);
        registerButton_JLabel.setForeground(ColorObj.buttonText);
        registerButton_JLabel.setFont(new Font("微软雅黑",Font.PLAIN,14));

        splitLabel_JLabel.setBounds(60,350,80,height);
        splitLabel_JLabel.setForeground(new java.awt.Color(64, 64, 64));
        retrievePSW_JLabel.setBounds(70,350,60,height);
        retrievePSW_JLabel.setForeground(ColorObj.buttonText);
        retrievePSW_JLabel.setFont(new Font("微软雅黑",Font.PLAIN,14));
        rememberButton_JCheckBox.setBounds(leftBorder+10,topBorder+190,width,height);
        rememberButton_JCheckBox.setBackground(new java.awt.Color(0,0,0,0));
        autoLoginButton_JCheckBox.setBounds(leftBorder+10,topBorder+230,width,height);
        autoLoginButton_JCheckBox.setBackground(null);

        this.add(theName_JLabel);
        this.add(theAccount_IAccountFiled);
        this.add(thePassword_IPasswordField);
        this.add(theLogin_JButton);
        this.add(registerButton_JLabel);
        this.add(splitLabel_JLabel);
        this.add(retrievePSW_JLabel);

//        1表示注册，2表示找回密码
        registerButton_JLabel.addMouseListener(new TheMouseAdapter(this,1));
        retrievePSW_JLabel.addMouseListener(new TheMouseAdapter(this,2));
        theLogin_JButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new LoginController(theAccount_IAccountFiled.getContent(),thePassword_IPasswordField.getContent()).login();
            }
        });

    }
}
