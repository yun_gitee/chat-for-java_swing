package com.talking.view.login;

import com.talking.controller.retrieve.GetCodeController;
import com.talking.controller.retrieve.RetrieveController;
import com.talking.view.component.IAccountField;
import com.talking.view.component.IButton;
import com.talking.view.component.IPasswordField;
import com.talking.view.component.ShadowBorder;
import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RetrievePanel extends JPanel {
    private JLabel registerTitle_JLabel;         //注册标题
    private IAccountField mailFiled;            //邮箱框
    private IAccountField verifyCode_Filed;     //验证码框
    private IButton getCode_JButton;   //获取验证码
    private IPasswordField passwordField;       //密码
    private IPasswordField verifyPSW;           //确认密码
    private IButton verifyButton_JButton;   //确认按钮
    private JLabel retrieveBackToLogin_JLabel;          //找回密码页面返回登录

    private int leftBorder = 20;
    private int topBorder = 0;
    private int height = 40;
    private int width = 240;
    private int pixels = 30;
    private static final int TOP_OPACITY = 20;

    public RetrievePanel(){
        registerTitle_JLabel = new JLabel("找回密码");
        mailFiled = new IAccountField("邮箱地址",leftBorder,topBorder+80,width,40);
        verifyCode_Filed = new IAccountField("验证码",leftBorder,topBorder+140,width/2-10,40);
        getCode_JButton = new IButton("验证码", ColorObj.mainThemeColor,true);
        passwordField = new IPasswordField("新密码",leftBorder,topBorder+200,width,40);
        verifyPSW = new IPasswordField("确认密码",leftBorder,topBorder+260,width,40);
        retrieveBackToLogin_JLabel = new JLabel("返回登录");
        verifyButton_JButton = new IButton("确认",ColorObj.mainThemeColor,true);

        this.setLayout(null);
        this.setBounds(80,50,360,500);
//        this.setBorder(new ShadowBorder(20));
        this.setBackground(ColorObj.MAIN_BACKGROUND);

        registerTitle_JLabel.setBounds(leftBorder,topBorder,140,40);
        registerTitle_JLabel.setFont(new Font("微软雅黑",Font.PLAIN,20));
        getCode_JButton.setBounds(leftBorder+width/2+10,topBorder+140,width/2-10,40);
        retrieveBackToLogin_JLabel.setBounds(20,350,80,height);
        retrieveBackToLogin_JLabel.setForeground(ColorObj.buttonText);
        retrieveBackToLogin_JLabel.setFont(new Font("微软雅黑",Font.PLAIN,14));
        verifyButton_JButton.setBounds(170,350,100,height);


        this.add(registerTitle_JLabel);
        this.add(mailFiled);
        this.add(verifyCode_Filed);
        this.add(getCode_JButton);
        this.add(passwordField);
        this.add(verifyPSW);
        this.add(retrieveBackToLogin_JLabel);
        this.add(verifyButton_JButton);

        retrieveBackToLogin_JLabel.addMouseListener(new TheMouseAdapter(this,0));
        getCode_JButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new GetCodeController(mailFiled.getTheText());
            }
        });
        verifyButton_JButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new RetrieveController(mailFiled.getTheText(),verifyCode_Filed.getTheText(),passwordField.getTheText(),verifyPSW.getTheText());
            }
        });


    }

}
