package com.talking.view.login;

import com.talking.view.componentpojo.ColorObj;
import com.talking.view.componentpojo.LoginFrameObj;

import javax.swing.*;
import java.awt.*;

public class LoginFrame {
    private JFrame loginPage_JFrame;
    private JPanel leftPanel_JPanel;
    private JPanel rightPanel_JPanel;
    private JLabel ipLabel_JLabel;                  //版本信息
    private final int width = 980;
    private final int height = 680;

    public LoginFrame(){
        //初始化对象
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        loginPage_JFrame = new JFrame("讯笺");
        leftPanel_JPanel = new JPanel();
        rightPanel_JPanel = new JPanel();
        ipLabel_JLabel = new JLabel("Version:1.22.03.28,01");
        loginPage_JFrame.setIconImage(new ImageIcon("image/logo/logo120x120.png").getImage());

        //设置大小、布局、颜色等
        loginPage_JFrame.setBounds((int)(screenSize.getWidth()-width)/2,100,width,height);
        loginPage_JFrame.setLayout(new GridLayout());
        loginPage_JFrame.setResizable(false);
        loginPage_JFrame.setDefaultCloseOperation(loginPage_JFrame.EXIT_ON_CLOSE);

        ipLabel_JLabel.setFont(new Font("微软雅黑",Font.PLAIN,16));
        ipLabel_JLabel.setForeground(ColorObj.buttonReleasedColor);
        ipLabel_JLabel.setBounds(20,600,360,20);


        leftPanel_JPanel.setBackground(ColorObj.MAIN_BACKGROUND);

        rightPanel_JPanel.setBackground(null);
        leftPanel_JPanel.setLayout(null);
        rightPanel_JPanel.setLayout(null);
        rightPanel_JPanel.setBackground(ColorObj.MAIN_BACKGROUND);



        leftPanel_JPanel.add(new CarouselPanel());
        leftPanel_JPanel.add(ipLabel_JLabel);
        rightPanel_JPanel.add(new FunctionPanel());
        loginPage_JFrame.add(leftPanel_JPanel);
        loginPage_JFrame.add(rightPanel_JPanel);
        LoginFrameObj.login_JFrame = loginPage_JFrame;
        loginPage_JFrame.setVisible(true);

    }
}
