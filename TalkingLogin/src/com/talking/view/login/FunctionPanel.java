package com.talking.view.login;
/*
* 这是登录面板
* */
import com.talking.controller.login.LoginController;
import com.talking.controller.register.GetMailCodeController;
import com.talking.controller.register.RegisterController;
import com.talking.controller.retrieve.GetCodeController;
import com.talking.controller.retrieve.RetrieveController;
import com.talking.view.component.*;
import com.talking.view.componentpojo.ColorObj;
import com.talking.view.componentpojo.LoginFrameObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class FunctionPanel extends JLabel {
    public static JPanel cardLayout_Container;
    public static CardLayout cardLayout;               //卡片布局管理器
    private LoginPanel loginPanel;                     //登录面板
    private RegisterPanel registerPanel_JPanel;                     //注册面板
    private RetrievePanel retrievePanel_JPanel;              //找回密码面板

    private int leftBorder = 40;
    private int topBorder = 50;
    private int height = 40;
    private int width = 240;
    private int pixels = 30;
    private static final int TOP_OPACITY = 20;

    public FunctionPanel(){

//        0px 0px 20px 5px #ececec
        this.setIcon(new ImageIcon("image/login/bg.jpg"));

        // 初始化对象
        cardLayout_Container = new JPanel();
        cardLayout = new CardLayout();
        loginPanel = new LoginPanel();
        registerPanel_JPanel = new RegisterPanel();
        retrievePanel_JPanel = new RetrievePanel();

        //设置大小、布局、位置
        this.setBounds(80,50,360,500);
        this.setLayout(null);
        cardLayout_Container.setLayout(cardLayout);
        cardLayout_Container.setBounds(leftBorder,topBorder,360-2*leftBorder,500-2*topBorder);

        cardLayout_Container.add(loginPanel,"0");
        cardLayout_Container.add(registerPanel_JPanel,"1");
        cardLayout_Container.add(retrievePanel_JPanel,"2");
        this.add(cardLayout_Container);


        LoginFrameObj.setContainer(this);
        LoginFrameObj.setCardLayout(cardLayout);
    }
}
