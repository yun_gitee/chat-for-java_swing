package com.talking.view.login;

import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class CarouselPanel extends JPanel {
    private JLabel textLabel_JLabel;    //文字标签
    private JLabel picLabel_JLabel1;    //图片标签
    private JLabel picLabel_JLabel2;
    private JLabel picLabel_JLabel3;
//    private scrollButton scrollButton1;
//    private scrollButton scrollButton2;
//    private scrollButton scrollButton3;
    private ScrollLabel scrollLabel;

    private int whichPicNum = 0;                    //轮播图
    private List<JLabel> picList;
//    private List<scrollButton> scrollButtonsList;   //滚动按钮集合
    private Thread picMoveThread;


    public CarouselPanel(){
        //实例化对象
        textLabel_JLabel = new JLabel(new ImageIcon(this.getClass().getResource("/login/05.png")));
        picLabel_JLabel1 = new JLabel(new ImageIcon(this.getClass().getResource("/login/2.png")));
        picLabel_JLabel2 = new JLabel(new ImageIcon(this.getClass().getResource("/login/1.png")));
        picLabel_JLabel3 = new JLabel(new ImageIcon(this.getClass().getResource("/login/3.png")));
        scrollLabel = new ScrollLabel(0,360,360,30);
        picList = new ArrayList();

        picList.add(picLabel_JLabel1);
        picList.add(picLabel_JLabel2);
        picList.add(picLabel_JLabel3);


        //位置，大小
        textLabel_JLabel.setBounds(16,50,328,78);
        textLabel_JLabel.setFont(new Font("微软雅黑",Font.BOLD,20));
        picLabel_JLabel1.setBounds(16,150,328,200);
        picLabel_JLabel2.setBounds(360,150,328,200);
        picLabel_JLabel3.setBounds(-330,150,328,200);


        this.setLayout(null);
        this.setBounds(80,50,360,500);
        this.setBackground(ColorObj.MAIN_BACKGROUND);

        this.add(textLabel_JLabel);
        this.add(picLabel_JLabel1);
        this.add(picLabel_JLabel2);
        this.add(picLabel_JLabel3);
        this.add(scrollLabel);

        picMoveThread = new Thread(new Timer());
        picMoveThread.start();

    }
    private void movePic() throws InterruptedException {
        for (int i=0;i<180;i++){
            picList.get(whichPicNum%3).setLocation(16-2*i,150);
            picList.get((whichPicNum+1)%3).setLocation(360-2*i,150);
            Thread.sleep(8);
        }

        picList.get((whichPicNum+2)%3).setLocation(361,150);
    }
    private class Timer implements Runnable{

        @Override
        public void run() {
            try {
                while(true) {
                    Thread.sleep(4000);
                    movePic();
                    whichPicNum += 1;
                    scrollLabel.repaint();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    private class ScrollLabel extends JLabel{
        private int temp;
        private ScrollLabel(int x,int y,int width,int height){
            this.setBounds(x,y,width,height);

        }

        public void paintComponent(Graphics g){
            for (int i = 0;i<3;i++){
                if (i == whichPicNum%3) {
                    g.setColor(ColorObj.mainThemeColor);
                    temp = 24;
                } else {
                    g.setColor(ColorObj.buttonReleasedColor);
                    temp = 0;
                }
                //光标在谁，就绘制谁
                g.fillRect(149+16*i+24*(whichPicNum%3==(i-1)||whichPicNum%3==(i-2)?(i==0?0:1):0),12,6+temp,6);
            }
        }
    }
}
