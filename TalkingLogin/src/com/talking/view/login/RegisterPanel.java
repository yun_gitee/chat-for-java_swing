package com.talking.view.login;

import com.talking.controller.register.GetMailCodeController;
import com.talking.controller.register.RegisterController;
import com.talking.view.component.IAccountField;
import com.talking.view.component.IButton;
import com.talking.view.component.IPasswordField;
import com.talking.view.component.ShadowBorder;
import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RegisterPanel extends JPanel {
    private JLabel registerTitle_JLabel;         //注册标题
    private IAccountField userName_Filed;        //用户名
    private IAccountField mailFiled;            //邮箱框
    private IAccountField verifyCode_Filed;     //验证码框
    private IButton getVerifyCode_JButton;   //获取验证码
    private IPasswordField passwordField;       //密码
    private IButton registerButton;   //确认按钮
    private JLabel registerBackToLogin_JLabel;  //注册页面返回登录

    private int leftBorder = 20;
    private int topBorder = 0;
    private int height = 40;
    private int width = 240;
    private int pixels = 30;
    private static final int TOP_OPACITY = 20;

    public RegisterPanel(){
        registerTitle_JLabel = new JLabel("注册");
        userName_Filed = new IAccountField("昵称",leftBorder,topBorder+80,width,40);
        mailFiled = new IAccountField("新邮箱",leftBorder,topBorder+140,width,40);
        verifyCode_Filed = new IAccountField("验证码",leftBorder,topBorder+200,width/2-10,40);
        getVerifyCode_JButton = new IButton("验证码", ColorObj.mainThemeColor,true);
        passwordField = new IPasswordField("密码",leftBorder,topBorder+260,width,40);
        registerBackToLogin_JLabel = new JLabel("返回登录");
        registerButton = new IButton("注册",ColorObj.mainThemeColor,true);

        this.setLayout(null);
        this.setBounds(80,50,360,500);
//        this.setBorder(new ShadowBorder(20));
        this.setBackground(ColorObj.MAIN_BACKGROUND);

        registerTitle_JLabel.setBounds(leftBorder,topBorder,140,40);
        registerTitle_JLabel.setFont(new Font("微软雅黑",Font.PLAIN,20));
        getVerifyCode_JButton.setBounds(leftBorder+width/2+10,topBorder+200,width/2-10,40);
        registerBackToLogin_JLabel.setBounds(20,350,80,height);
        registerBackToLogin_JLabel.setForeground(ColorObj.buttonText);
        registerBackToLogin_JLabel.setFont(new Font("微软雅黑",Font.PLAIN,14));
        registerButton.setBounds(170,350,100,height);


        this.add(registerTitle_JLabel);
        this.add(userName_Filed);
        this.add(mailFiled);
        this.add(verifyCode_Filed);
        this.add(getVerifyCode_JButton);
        this.add(passwordField);
        this.add(registerBackToLogin_JLabel);
        this.add(registerButton);

        registerBackToLogin_JLabel.addMouseListener(new TheMouseAdapter(this,0));
        getVerifyCode_JButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new GetMailCodeController(mailFiled.getTheText());
            }
        });
        registerButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new RegisterController(userName_Filed.getTheText(),mailFiled.getTheText(),verifyCode_Filed.getTheText(),passwordField.getTheText());
            }
        });

    }
}
