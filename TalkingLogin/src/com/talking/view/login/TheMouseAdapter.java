package com.talking.view.login;

import com.talking.view.componentpojo.ColorObj;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TheMouseAdapter extends MouseAdapter {
    private Object creator;
    private int opt = 0;

    public TheMouseAdapter(Object creator,int opt){
        this.creator = creator;
        this.opt = opt;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        super.mouseClicked(e);
        e.getComponent().setForeground(ColorObj.buttonReleasedColor);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
        e.getComponent().setForeground(ColorObj.buttonText);

        try {
            Thread.sleep(50);
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }

        if (this.creator instanceof LoginPanel){
            if (opt==1)
                FunctionPanel.cardLayout.show(FunctionPanel.cardLayout_Container,"1");
            else
                FunctionPanel.cardLayout.show(FunctionPanel.cardLayout_Container,"2");
        }else if (this.creator instanceof RegisterPanel)
            FunctionPanel.cardLayout.show(FunctionPanel.cardLayout_Container,"0");
        else if (this.creator instanceof RetrievePanel)
            FunctionPanel.cardLayout.show(FunctionPanel.cardLayout_Container,"0");

    }
}