package com.talking.tool;

import com.talking.pojo.ChatMsg;
import com.talking.pojo.Group;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.view.chat.LeftPanel;
import com.talking.view.componentpojo.BodyFrameObj;
import com.talking.view.componentpojo.ChatPojo;

import java.awt.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * 用于即时给聊天界面绘制内容，
 * 并且生成童子
 */
public class NewMsgDispose {
    private boolean isUser;
    private int senderID;
    private String content;
    private String time;
    private int chatUserId;
    private String chatName;

    public NewMsgDispose(ChatMsg chatMsg){

        new AudioNotice().start();

        if (SystemVariable.getCurrentChatObj()!=null) {
            isUser = SystemVariable.getCurrentChatObj().isUser();       //当前是群聊还是私聊

            this.content = chatMsg.getContent();
            this.time = chatMsg.getSendTime();

            if (isUser) {   //判断我当前是私聊
                this.senderID = chatMsg.getSender().getUser_id();      //消息发送者ID
                //我的聊天对象ID
                this.chatUserId = ((User) SystemVariable.getCurrentChatObj().getCurrentChatObj()).getUser_id();
                //我聊天对象名字
                this.chatName = ((User) SystemVariable.getCurrentChatObj().getCurrentChatObj()).getUser_name();
            } else {//判断我当前是私聊

                senderID = ((User)chatMsg.getReceiver()).getUser_id();
                this.chatUserId = ((Group) SystemVariable.getCurrentChatObj().getCurrentChatObj()).getGroup_id();
                this.chatName = ((User)chatMsg.getSender()).getUser_name();
            }

            if (isUser == chatMsg.isSignChat() && senderID == chatUserId) {
                System.out.println("开始消息展示：");
                new MsgToPane(chatMsg);
                System.out.println("消息展示完毕");
            } else {
                System.out.println("判断输出信息：" + isUser + "\n" + chatMsg.isSignChat() + "\n" + senderID + "\n" + chatUserId);
                this.createMsgNotice(chatMsg);
            }
        }else{
            this.createMsgNotice(chatMsg);
        }
    }

    //当当前聊天对象与消息发送者不一致时
    private void createMsgNotice(ChatMsg chatMsg){
        if (chatMsg.isSignChat()) {
            //私聊
            System.out.println("这是私聊");
            List<LeftPanel.FriendPanel> list = ChatPojo.getLeftPanel().friendBlocks;
            LeftPanel.FriendPanel currentBlock = null;
            int index = 0;

            for (LeftPanel.FriendPanel friendPanel : list) {
                if (friendPanel.user.getUser_id() == chatMsg.getSender().getUser_id()) {
                    System.out.println("找到了");
                    currentBlock = friendPanel;
                    friendPanel.setDot();
                    friendPanel.messLabel.setText("对方：" + chatMsg.getContent());
                    break;
                }
                index++;
            }
            for (int i = index; i > 0; i--) {
                list.set(i, list.get(i - 1));
            }
            list.set(0, currentBlock);

            ChatPojo.getLeftPanel().addFriendBlocks(list);
        }else {
            //群聊
            List<LeftPanel.GroupPanel> list = ChatPojo.getLeftPanel().groupBlocks;
            LeftPanel.GroupPanel groupPanel = null;
            int temp = 0;

            for (LeftPanel.GroupPanel panel : list) {
                if (((Group)chatMsg.getReceiver()).getGroup_id()==panel.group.getGroup_id()){
                    groupPanel = panel;
                    groupPanel.setDot();
                    groupPanel.groupTips.setText(chatMsg.getSender().getUser_name()+":"+chatMsg.getContent());
                    break;
                }
                temp++;
            }

            for (int i = temp; i > 0; i--) {
                list.set(i, list.get(i - 1));
            }
            list.set(0, groupPanel);

            ChatPojo.getLeftPanel().addGroupBlocks(list);
        }

    }


}
