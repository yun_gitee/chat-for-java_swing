package com.talking.tool;

import com.talking.pojo.ChatMsg;
import com.talking.view.componentpojo.ChatPojo;
import com.talking.view.componentpojo.ColorObj;
import com.talking.pojo.CurrentChatObj;
import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;
import com.talking.view.component.JIMSendTextPane;

import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import java.awt.*;
import java.io.*;
import java.sql.Timestamp;

public class MsgFileInput {
    private String logFilePath = "log/";
    private int id;
    private File files;
    private FileReader reader;
    private JIMSendTextPane showMsg;
    private CurrentChatObj chatObj;
    private boolean isUser;
    private int userId;

    //, JIMSendTextPane showMsg
    public MsgFileInput(int id){
        this.id = id;
        userId = SystemVariable.getCurrentUser().getUser_id() ;
        chatObj = SystemVariable.getCurrentChatObj();
        isUser = chatObj.isUser();

        if (isUser){
            //私聊
            logFilePath = logFilePath+"friend/";
        }else {
            //群聊
            logFilePath = logFilePath+"group/";
        }
        files = new File(logFilePath+userId+"/"+id+".log");

    }

    public void read(){
        try {
            if (files.exists()) {

                reader = new FileReader(files);
                StringBuffer msgHistory = new StringBuffer();
                String[] allMsg = null;
                BufferedReader bufferedReader = new BufferedReader(reader);
                String ch;

                while (((ch=bufferedReader.readLine())!=null)){
                    msgHistory.append(ch);
                }

                reader.close();
                bufferedReader.close();

                allMsg = msgHistory.toString().split("<%#&>");

                for (String s : allMsg) {
                    //用户名与时间设置
//                    System.out.println("单挑内容是:"+s);
                    String[] timeAndContent = s.split("<#@&>"); //id,时间,内容

                    for (String s1 : timeAndContent) {
                        System.out.println(s1);
                    }

                    String name = "";
                    String myId = String.valueOf(SystemVariable.getCurrentUser().getUser_id());
                    if (timeAndContent[0].equals(myId)){
                        name = SystemVariable.getCurrentUser().getUser_name();
                    }else {

                        if (chatObj.isUser()){
                            name = ((User)chatObj.getCurrentChatObj()).getUser_name();
                        }else{
//                            System.out.println("id是多少："+timeAndContent[0]);
                            try {
                                name = SystemVariable.getCurrentGroupNumbers().get(Integer.valueOf(timeAndContent[0])).getUser_name();
                            }catch (NullPointerException e){
                                e.printStackTrace();
                                name = String.valueOf(timeAndContent[0]);
                            }
                            if (name==""||name==null){
                                name = timeAndContent[0];
                            }
                        }
                    }
                    ChatMsg chatMsg = new ChatMsg();
                    User user = new User();
                    user.setUser_name(name);
                    user.setUser_id(Integer.parseInt(timeAndContent[0]));
                    chatMsg.setSender(user);
                    chatMsg.setSendTime(timeAndContent[1]);

                    chatMsg.setContent(timeAndContent[2]);



                    new MsgToPane(chatMsg);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
