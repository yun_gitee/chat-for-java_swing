package com.talking.tool;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.FileInputStream;
import java.io.IOException;

public class AudioNotice extends Thread{
    public void run(){
        try {
            AudioStream as = new AudioStream(new FileInputStream("audio/mess.wav"));
            AudioPlayer.player.start(as);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
