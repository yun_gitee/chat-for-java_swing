package com.talking.tool;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SocketConfig {
    private String serverSocketIP;
    private int serverPort;

    public SocketConfig() {
        Properties prop = new Properties();
        InputStream in = null;
        try {
            System.out.println("路径为：");
//            in = new FileInputStream(this.getClass().getResourceAsStream("/resources/server.properties"));
            in = this.getClass().getResourceAsStream("/resources/server.properties");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            prop.load(in);
            serverSocketIP = prop.getProperty("serverSocketIP").trim();
            serverPort = Integer.valueOf(prop.getProperty("serverPort").trim());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getServerSocketIP() {
        return serverSocketIP;
    }

    public int getServerPort() {
        return serverPort;
    }

    @Override
    public String toString() {
        return "SocketConfig{" +
                "serverSocketIP='" + serverSocketIP + '\'' +
                ", serverPort=" + serverPort +
                '}';
    }
}
