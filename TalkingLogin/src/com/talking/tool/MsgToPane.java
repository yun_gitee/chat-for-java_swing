package com.talking.tool;

import com.talking.pojo.ChatMsg;
import com.talking.view.component.ChatBlock;
import com.talking.view.componentpojo.BodyFrameObj;
import com.talking.view.component.JIMSendTextPane;
import com.talking.view.componentpojo.ChatPojo;
import com.talking.view.componentpojo.ColorObj;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MsgToPane {
    private JScrollPane jScrollPane;        //消息显示面板的滚动面包那
    private JPanel msgShow;     //消息显示面板


    public MsgToPane(ChatMsg chatMsg) {        //String name, String time, String content, boolean isMine
        this.msgShow = ChatPojo.getMsgShow();

        msgShow.add(new ChatBlock(chatMsg));
        msgShow.repaint();

        this.jScrollPane = ChatPojo.getMsgJScroll();
        JScrollBar bar = jScrollPane.getVerticalScrollBar();
        int value = bar.getMaximum();
        System.out.println("这发送个值是："+value);
        bar.setValue(value+100);
    }

}
