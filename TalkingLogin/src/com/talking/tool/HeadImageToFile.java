package com.talking.tool;

import com.talking.pojo.SystemVariable;
import com.talking.pojo.User;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;

public class HeadImageToFile {
    private final static String filePath = "temp/";
    private final static User mine = SystemVariable.getCurrentUser();
    public static void toFile(byte[] bytes,int id,boolean isUser){
        try {
            File file = new File(filePath + mine.getUser_id() + "/");
            if (!file.exists())
                file.mkdirs();
            File file1 = null;

            if (isUser)
                file1 = new File(file+"/u"+id+".png");
            else
                file1 = new File(file+"/g"+id+".png");

//            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
//            BufferedImage bi1 = ImageIO.read(bais);
//            ImageIO.write(bi1, "jpg", file1);// 不管输出什么格式图片，此处不需改动
            FileOutputStream out = new FileOutputStream(file1);
            System.out.println("执行地址是："+file1.getPath());
            out.write(bytes);
            out.flush();
            out.close();

        }catch (Exception e){e.printStackTrace();}
    }
}
