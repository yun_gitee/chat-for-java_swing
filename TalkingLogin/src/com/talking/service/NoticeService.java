package com.talking.service;

import com.talking.pojo.Message;
import com.talking.pojo.MessageType;
import com.talking.pojo.Notice;

public class NoticeService {
    Message writeMSG = new Message();
    Message inMSG;

    //发出通知
    public void noticeSend(Notice notice){
        System.out.println("发出请求：确认加入群聊");
        writeMSG.setMessageType(MessageType.NOTICE);
        writeMSG.setContent(notice);
        SocketService.writeMessage(writeMSG);

    }
}
