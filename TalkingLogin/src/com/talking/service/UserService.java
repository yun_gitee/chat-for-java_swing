package com.talking.service;

import com.talking.tool.HeadImageToFile;
import com.talking.view.componentpojo.ColorObj;
import com.talking.pojo.*;
import com.talking.tool.MsgFileOutput;
import com.talking.tool.MsgToPane;

import javax.swing.*;
import java.io.IOException;
import java.util.List;

public class UserService {
    Message writeMSG = new Message();
    Message inMSG;

    //注册功能
    public Message register(User user){
        System.out.println("发送请求：注册账户");
        try {
            writeMSG.setMessageType(MessageType.REGISTER);
            writeMSG.setContent(user);

            SocketService.writeMessage(writeMSG);
            System.out.println("注册发送信息成功");
            inMSG = (Message) SocketService.getClientInput().readObject();

        }catch (Exception e){
            e.printStackTrace();
        }
        return inMSG;
    }

    //登录功能
    public boolean login(String id,String pwd){
        boolean loginFlag = false;

        User user = new User();

        try {
        user.setUser_mail(id);
        user.setUser_pwd(pwd);

        writeMSG.setMessageType(MessageType.LOGIN);
        writeMSG.setContent(user);
        SocketService.writeMessage(writeMSG);
        System.out.println("信息发送成功");

        inMSG = (Message) SocketService.getClientInput().readObject();
        System.out.println("接收信息成功，内容是：");

        if (inMSG.isResponseType()){
            loginFlag = true;
            User mine = (User) inMSG.getContent();
            SystemVariable.setCurrentUser(mine);
            HeadImageToFile.toFile(mine.getHead_source(),mine.getUser_id(),true);
        }else {
            String tips = "";
//            if (inMSG.getResponseContent() == ResponseType.PWD_ERROR)
//                tips = "账号或者密码错误";
//            else if (inMSG.getResponseContent() == ResponseType.BE_LOGIN)
//                tips = "用户已登录";
            tips = inMSG.getResponseContent();
                JOptionPane.showMessageDialog(null, tips, "错误提示", JOptionPane.WARNING_MESSAGE);
        }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return loginFlag;
    }


    //获取好友列表
    public List<User> getFriendList(){
        System.out.println("发出请求：请求好友列表");
        try {
            writeMSG = new Message();
            writeMSG.setMessageType(MessageType.GET_FRIENDS);
            writeMSG.setContent(SystemVariable.getCurrentUser());
            SocketService.writeMessage(writeMSG);

            //接收返回值
            inMSG =(Message) SocketService.getClientInput().readObject();
        }catch (Exception e){
            e.printStackTrace();
        }

        if (inMSG!=null)
            if (!inMSG.isResponseType())
                JOptionPane.showMessageDialog(null, "好友列表获取失败！", "错误提示", JOptionPane.WARNING_MESSAGE);

            return (List<User>)inMSG.getContent();
    }


    //注销
    public void logout(){
        System.out.println("发出请求：请求注销");
        writeMSG.setMessageType(MessageType.LOGOUT);
        writeMSG.setContent(SystemVariable.getCurrentUser());

        System.out.println("发送注销命令");
        SocketService.writeMessage(writeMSG);
    }

    //退出功能呢
    public void exit(){
        System.out.println("发出请求：请求退出");
        writeMSG.setMessageType(MessageType.OFFLINE);
        writeMSG.setContent(SystemVariable.getCurrentUser());

        System.out.println("发送退出命令");
        SocketService.writeMessage(writeMSG);
    }

    //发送私聊信息功能
    public void chat(ChatMsg chatMsg){
        System.out.println("发出请求：发送消息");

        writeMSG.setMessageType(MessageType.CHAT);
        System.out.println("发送聊天信息");
        writeMSG.setContent(chatMsg);
        SocketService.writeMessage(writeMSG);

        new MsgFileOutput(chatMsg).input();       //消息写入本地文件
        new MsgToPane(chatMsg);
    }

    //搜索好友功能
    public void searchFriend(String content){
        System.out.println("发出请求：搜索好友");
        User user = new User();
        user.setUser_mail(content);
        writeMSG.setMessageType(MessageType.SEARCH_FRI);
        writeMSG.setContent(user);
        System.out.println("发送查找指令");
        SocketService.writeMessage(writeMSG);

    }

    //申请添加好友
    public void friendRequest(Message message){
        System.out.println("发出请求：申请添加好友");

        SocketService.writeMessage(message);
    }

    //删除好友
    public void deleteFriend(Notice notice){
        System.out.println("发出请求：删除好友功能");

        writeMSG.setMessageType(MessageType.DELETE_FRI);
        writeMSG.setContent(notice);
        SocketService.writeMessage(writeMSG);
    }

    //修改账户资料
    public void updateUserMSG(User user,String mailCode){
        System.out.println("发出请求：修改账户资料");
        writeMSG.setMessageType(MessageType.UPDATE_USER_MSG);
        writeMSG.setContent(user);
        writeMSG.setResponseContent(mailCode);
        SocketService.writeMessage(writeMSG);
    }
}
