package com.talking.service;

import com.talking.pojo.Message;
import com.talking.pojo.MessageType;
import com.talking.pojo.User;

import java.util.WeakHashMap;

public class MailService {
    Message writeMSG = new Message();
    Message inMSG;

    //邮箱是否存在
    public boolean mailIsExited(String user_mail){
        System.out.println("发出请求：邮箱是否存在");
        boolean flag = false;
        try {
            writeMSG = new Message();
            writeMSG.setMessageType(MessageType.MAIL_IS_EXITED);
            writeMSG.setContent(user_mail);
            SocketService.writeMessage(writeMSG);
            inMSG = (Message) SocketService.getClientInput().readObject();

            if (inMSG.isResponseType())
                flag = inMSG.isResponseType();

        }catch (Exception e){
            e.printStackTrace();
        }

        return flag;
    }

    //创建验证码
    public Message createMailCode(String user_mail,String kind){
        System.out.println("发出请求：创建验证码");
        try {
            writeMSG = new Message();
            writeMSG.setMessageType(MessageType.GET_MAIL_CODE);
            writeMSG.setContent(user_mail);
            writeMSG.setResponseContent(kind);
            SocketService.writeMessage(writeMSG);
            if (kind!=null)
                inMSG = (Message) SocketService.getClientInput().readObject();

        }catch (Exception e){
            e.printStackTrace();
        }
        return inMSG;
    }

    //验证邮箱验证码
    public boolean verifyMailCode(User user){

        System.out.println("发出请求：验证邮箱验证码");
        boolean flag = false;
        try{
            writeMSG = new Message();
            writeMSG.setMessageType(MessageType.VERIFY_CODE);
            writeMSG.setContent(user);
            SocketService.writeMessage(writeMSG);
            inMSG = (Message) SocketService.getClientInput().readObject();

            if (inMSG.isResponseType())
                flag = inMSG.isResponseType();

        }catch (Exception e){
            e.printStackTrace();
        }
        return flag;
    }
}
