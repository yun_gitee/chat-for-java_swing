package com.talking.service;

import com.talking.pojo.*;
import com.talking.thread.ReceiveThread;

import java.util.Hashtable;
import java.util.List;

public class GroupService {
    Message writeMSG = new Message();
    Message inMSG;

    //获取群列表
    public List<Group> getGroupList(){
        System.out.println("发出请求：请求群成员");
        List<Group> groupList = null;
        try{
            writeMSG = new Message();
            writeMSG.setMessageType(MessageType.GET_GROUPS);
            writeMSG.setContent(SystemVariable.getCurrentUser());
            SocketService.writeMessage(writeMSG);

            //接收返回值
            inMSG =(Message) SocketService.getClientInput().readObject();
            groupList = (List<Group>) inMSG.getContent();
        }catch (Exception e){
            e.printStackTrace();
        }
        return groupList;
    }


    //获取群成员信息
    public void getGroupNumbers(Group group){
        System.out.println("发出请求：请求群成员");
        try{
            writeMSG = new Message();
            writeMSG.setMessageType(MessageType.GET_GROUP_NUMBERS);
            writeMSG.setContent(group);
            SocketService.writeMessage(writeMSG);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //创建群
    public void createGroup(Group group){
        System.out.println("发出请求：创建群");
        try{
            writeMSG = new Message();
            writeMSG.setMessageType(MessageType.CREATE_GROUP);
            writeMSG.setContent(group);
            SocketService.writeMessage(writeMSG);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //邀请加入群聊
    public boolean inviteNums(Message message){
        System.out.println("发出请求：邀请加入群聊");
        boolean flag = false;
        try{
            SocketService.writeMessage(message);
//            flag = inMSG.isResponseType();
        }catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    //退出群聊
    public void exitGroup(Message message){
        System.out.println("发出请求：退出群聊");

        SocketService.writeMessage(message);
    }

}
