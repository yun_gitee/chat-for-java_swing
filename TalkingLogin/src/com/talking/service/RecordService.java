package com.talking.service;

import com.talking.pojo.Message;
import com.talking.pojo.MessageType;
import com.talking.pojo.Record;
import com.talking.pojo.SystemVariable;

import java.io.IOException;
import java.util.List;

public class RecordService {

    Message writeMSG = new Message();
    Message inMSG;

    public List<Record> getOffLineRecord(){
        System.out.println("发出请求：获取离线消息");
        try {
        writeMSG = new Message();
        writeMSG.setMessageType(MessageType.GE_OFFLINE_MSG);
        writeMSG.setContent(SystemVariable.getCurrentUser().getUser_id());
        SocketService.writeMessage(writeMSG);
        inMSG =(Message) SocketService.getClientInput().readObject();
        List<Record> offLineMSG = (List<Record>) inMSG.getContent();
            System.out.println("接受到离线消息");
            for (Record record : offLineMSG) {
                System.out.println(record.toString());
            }

        return offLineMSG;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    return null;
    }
}
