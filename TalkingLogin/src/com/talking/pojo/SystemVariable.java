package com.talking.pojo;

import com.talking.thread.ReceiveThread;
import com.talking.tool.HeadImageToFile;

import java.util.Hashtable;
import java.util.List;

/**
 * 此类记录系统的公共变量
 * 例如当前的用户、好友列表、当前聊天对象
 */
public class SystemVariable {
    private static ReceiveThread receiveThread;
    private static User currentUser;            //我自己
    private static List<User> friendList;       //我的好友列表
    private static List<Group> groupList;       //群列列表
    private static CurrentChatObj currentChatObj;         //当前的聊天对象

    private static Hashtable<Integer,User> currentGroupNumbers;          //当前群成员

    public static ReceiveThread getReceiveThread() {
        return receiveThread;
    }

    public static void setReceiveThread(ReceiveThread receiveThread) {
        SystemVariable.receiveThread = receiveThread;
    }

    public static Hashtable<Integer, User> getCurrentGroupNumbers() {
        return currentGroupNumbers;
    }

    public static void setCurrentGroupNumbers(Hashtable<Integer, User> currentGroupNumbers) {
        SystemVariable.currentGroupNumbers = currentGroupNumbers;
    }

    public static List<Group> getGroupList() {
        return groupList;
    }

    public static void setGroupList(List<Group> groupList) {
        SystemVariable.groupList = groupList;
        for (Group group : groupList) {
            HeadImageToFile.toFile(group.getHead_source(),group.getGroup_id(),false);
        }
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        SystemVariable.currentUser = currentUser;
    }

    public static List<User> getFriendList() {
        return friendList;
    }

    public static void setFriendList(List<User> friendList) {
        SystemVariable.friendList = friendList;
        for (User user : friendList) {
            HeadImageToFile.toFile(user.getHead_source(),user.getUser_id(),true);
        }
    }

    public static CurrentChatObj getCurrentChatObj() {
        return currentChatObj;
    }

    public static void setCurrentChatObj(CurrentChatObj currentChatObj) {
        SystemVariable.currentChatObj = currentChatObj;
    }
}
