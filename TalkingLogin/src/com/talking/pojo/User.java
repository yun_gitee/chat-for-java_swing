package com.talking.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * 用户尸实体类
 */
public class User implements Serializable {
    private int user_id;
    private String user_mail;
    private String user_pwd;
    private String user_name;
    private String user_sex;
    private int user_age;
    private String user_describe;
    private int authority;
    private byte[] head_source;
    private Timestamp create_time;
    private Timestamp delete_time;
    private boolean status;
    private String ip;
    private String address;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_mail() {
        return user_mail;
    }

    public void setUser_mail(String user_mail) {
        this.user_mail = user_mail;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public int getUser_age() {
        return user_age;
    }

    public void setUser_age(int user_age) {
        this.user_age = user_age;
    }

    public String getUser_describe() {
        return user_describe;
    }

    public void setUser_describe(String user_describe) {
        this.user_describe = user_describe;
    }

    public int getAuthority() {
        return authority;
    }

    public void setAuthority(int authority) {
        this.authority = authority;
    }

    public byte[] getHead_source() {
        return head_source;
    }

    public void setHead_source(byte[] head_source) {
        this.head_source = head_source;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public Timestamp getDelete_time() {
        return delete_time;
    }

    public void setDelete_time(Timestamp delete_time) {
        this.delete_time = delete_time;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "user_id=" + user_id +
                ", user_mail='" + user_mail + '\'' +
                ", user_pwd='" + user_pwd + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_sex='" + user_sex + '\'' +
                ", user_age=" + user_age +
                ", user_describe='" + user_describe + '\'' +
                ", authority=" + authority +
                ", head_source=" + Arrays.toString(head_source) +
                ", create_time=" + create_time +
                ", delete_time=" + delete_time +
                ", status=" + status +
                ", ip='" + ip + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
