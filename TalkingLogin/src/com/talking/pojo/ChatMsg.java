package com.talking.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

public class ChatMsg implements Serializable {
    private boolean signChat;           //私聊信息，还是群聊信息
    private String sendTime;         //发送时间
    private User sender;                //发送者
    private Object receiver;              //接收者:用户，或者群
    private String content;             //内容

    public boolean isSignChat() {
        return signChat;
    }

    public void setSignChat(boolean signChat) {
        this.signChat = signChat;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public Object getReceiver() {
        return receiver;
    }

    public void setReceiver(Object receiver) {
        this.receiver = receiver;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ChatMsg{" +
                "signChat=" + signChat +
                ", sendTime=" + sendTime +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", content='" + content + '\'' +
                '}';
    }
}
