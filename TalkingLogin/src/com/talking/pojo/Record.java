package com.talking.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

public class Record implements Serializable {
    private int record_id;
    private String record_content;
    private int src_user;
    private int target_account;
    private Timestamp create_time;
    private int record_type;
    private boolean is_read;

    public int getRecord_id() {
        return record_id;
    }

    public void setRecord_id(int record_id) {
        this.record_id = record_id;
    }

    public String getRecord_content() {
        return record_content;
    }

    public void setRecord_content(String record_content) {
        this.record_content = record_content;
    }

    public int getSrc_user() {
        return src_user;
    }

    public void setSrc_user(int src_user) {
        this.src_user = src_user;
    }

    public int getTarget_account() {
        return target_account;
    }

    public void setTarget_account(int target_account) {
        this.target_account = target_account;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public int getRecord_type() {
        return record_type;
    }

    public void setRecord_type(int record_type) {
        this.record_type = record_type;
    }

    public boolean isIs_read() {
        return is_read;
    }

    public void setIs_read(boolean is_read) {
        this.is_read = is_read;
    }

    @Override
    public String toString() {
        return "Record{" +
                "record_id=" + record_id +
                ", record_content='" + record_content + '\'' +
                ", src_user=" + src_user +
                ", target_account=" + target_account +
                ", create_time=" + create_time +
                ", record_type=" + record_type +
                ", is_read=" + is_read +
                '}';
    }
}
