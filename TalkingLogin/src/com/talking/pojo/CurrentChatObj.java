package com.talking.pojo;

/**
 * 0为好友
 * 1群
 */
public class CurrentChatObj {
    private Object currentChatObj;
    private boolean isUser;

    public CurrentChatObj(Object currentChatObj,boolean isUser) {
        this.currentChatObj = currentChatObj;
        this.isUser = isUser;
    }

    public Object getCurrentChatObj() {
        return currentChatObj;
    }

    public boolean isUser() {
        return isUser;
    }
}
