package com.talking.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * 发送者（ID、名字、头像）
 * 接收者（ID）
 * 通知类型（好友添加通知，群邀请通知）
 * 群（ID、名字）
 */
public class Notice implements Serializable {
    private User sender;
    private List<User> receiver;
    private boolean userNotice;
    private Group group;
    private String remark;

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public List<User> getReceiver() {
        return receiver;
    }

    public void setReceiver(List<User> receiver) {
        this.receiver = receiver;
    }

    public boolean isUserNotice() {
        return userNotice;
    }

    public void setUserNotice(boolean userNotice) {
        this.userNotice = userNotice;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Notice{" +
                "sender=" + sender +
                ", receiver=" + receiver +
                ", userNotice=" + userNotice +
                ", group=" + group +
                ", remark='" + remark + '\'' +
                '}';
    }
}