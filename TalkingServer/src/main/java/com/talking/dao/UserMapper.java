package com.talking.dao;

import com.talking.pojo.User;

import java.util.List;

public interface UserMapper {
    //查找好友
    List<User> getUserByOneService(String user_id);

    //登录
    User getUserByIDAndPwd(User user);

    //获取用户全部信息
    User getUserMSGByID(int user_id);

    //获取好友列表
    List<User> getFriendList(int id);

    //获取成员
    List<User> getGroupNumbers(int group_id);

    //插入新用户
    int insertUser(User user);

    //更新用户信息,动态sql
    int updateUserMess(User user);

}
