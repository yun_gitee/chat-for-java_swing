package com.talking.dao;

import com.talking.pojo.Group;
import java.util.List;

public interface GroupMapper {

    //查找单个表
    Group getSignGroup(String group_id);

    //查找已加入群
    List<Group>  getUserGroup(int user_id);

    //插入表信息
    int insertNewGroup(Group group);

    //更新表信息
    int updateGroupMess(Group group);

    //查找最新插入的表的账号
    int getLastInsertID();

    //删除表
    int deleteGroupByID(int group_id);

}
