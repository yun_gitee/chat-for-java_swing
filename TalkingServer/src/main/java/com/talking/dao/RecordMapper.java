package com.talking.dao;

import com.talking.pojo.Record;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RecordMapper {
    //通过用户ID获取所有聊天记录
    List<Record> getRecordsByUserID(@Param("user_id") int user_id,@Param("is_read") boolean is_read);

    //通过群ID获取单个群聊天记录
    List<Record> getRecordsByGroupID(@Param("group_id") int group_id);

    //插入一条聊天记录
    int insertRecord(Record record);

    //标记聊天记录为已读
    int updateRecord(int user_id);

    //删除聊天记录
    int deleteRecord(int src_user,int target_user);
}
