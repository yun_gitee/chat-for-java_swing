package com.talking.dao;

import org.apache.ibatis.annotations.Param;

public interface RelationMapper {
    //获取两个账户之间的好友关系
    int getUserRelation(@Param("id1") int user1_id,@Param("id2") int user2_id,@Param("kind") int kind);

    //插入好友关系
    int insertUserRelation(@Param("id1") int user1_id,@Param("id2") int user2_id,@Param("relType") int relType);

    //删除好友关系
    int deleteUserRelation(@Param("id1") int user1_id,@Param("id2") int user2_id,@Param("kind") int kind);

}
