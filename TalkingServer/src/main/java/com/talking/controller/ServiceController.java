package com.talking.controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServiceController {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private volatile boolean isRunning;

    public ServiceController(){
        try {
            isRunning = true;
            serverSocket = new ServerSocket(9999);
            System.out.println("服务器已启动："+serverSocket.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void start(){
        while(isRunning){
            try {
                //监听
                clientSocket = serverSocket.accept();
                System.out.println("\n捕获到客户端连接："+clientSocket.getInetAddress());
                System.out.println("当前活跃线程数："+Thread.activeCount());

                new ServerThread(clientSocket).start();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
