package com.talking.controller;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Properties;

public class MailController {
    private String Address;
    private String kind;
    private String title;
    private String code;
    private String content;

    private String serverMail;
    private String mailKey;
    private BufferedReader reader;
    private StringBuffer strBuffer;

    public MailController(String address, String code) {
        Address = address;
        this.kind = "注册或者密码找回";
        this.title = "Talking邮箱验证码";
        this.code = code;
        strBuffer = new StringBuffer();

        Properties pro = new Properties();
        try {
            pro.load(new FileInputStream("src/main/resources/mail-config.properties"));
            serverMail = pro.getProperty("account");
            mailKey = pro.getProperty("key");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean sendMail(){
        boolean flag = false;

        try{
            reader = new BufferedReader(new FileReader("src/main/resources/mail.html"));
            String ch;

            while (((ch=reader.readLine())!=null)){
                strBuffer.append(ch);
            }
            String temp = strBuffer.toString();
            temp = temp.replace("<[#operation#]>",kind);
            temp = temp.replace("<[#code#]>",code);
            content = temp;

            Properties properties = new Properties();
            properties.put("mail.transport.protocol", "smtp");// 连接协议
            properties.put("mail.smtp.host", "imap.qq.com");// 主机名
            properties.put("mail.smtp.port", 25);// 端口号
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.ssl.enable", "false");// 设置是否使用ssl安全连接 ---一般都使用
            properties.put("mail.debug", "true");// 设置是否显示debug信息 true 会在控制台显示相关信息
            // 得到回话对象
            Session session = Session.getInstance(properties);
            // 获取邮件对象
            Message message = new MimeMessage(session);
            // 设置发件人邮箱地址
            message.setFrom(new InternetAddress(serverMail));
            // 设置收件人邮箱地址
            message.setRecipients(Message.RecipientType.TO,
                    new InternetAddress[] { new InternetAddress(Address) });
            //new InternetAddress();设置同时发送多个好友
            // 设置邮件标题
            message.setSubject(title);
            // 设置邮件内容
            message.setContent(content, "text/html;charset=UTF-8");
//            message.setText(content);
            // 得到邮差对象
            Transport transport = session.getTransport();
            // 连接自己的邮箱账户
            transport.connect(serverMail, mailKey);// 密码为QQ邮箱开通的stmp服务后得到的客户端授权码
            // 发送邮件
            int i=0;
            transport.sendMessage(message, message.getAllRecipients());
            System.out.println("成功！");
            transport.close();
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("邮箱是："+Address+"，验证码："+code);
        }
        return flag;
    }
}
