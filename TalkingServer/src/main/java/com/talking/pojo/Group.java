package com.talking.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * 群实体类
 */
public class Group implements Serializable {
    private int group_id;
    private String group_name;
    private int creator_id;
    private int count_user;
    private String group_describe;
    private Timestamp create_time;
    private Timestamp delete_time;
    private byte[] head_source;


    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public int getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(int creator_id) {
        this.creator_id = creator_id;
    }

    public int getCount_user() {
        return count_user;
    }

    public void setCount_user(int count_user) {
        this.count_user = count_user;
    }

    public String getGroup_describe() {
        return group_describe;
    }

    public void setGroup_describe(String group_describe) {
        this.group_describe = group_describe;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public Timestamp getDelete_time() {
        return delete_time;
    }

    public void setDelete_time(Timestamp delete_time) {
        this.delete_time = delete_time;
    }

    public byte[] getHead_source() {
        return head_source;
    }

    public void setHead_source(byte[] head_source) {
        this.head_source = head_source;
    }

    @Override
    public String toString() {
        return "Group{" +
                "group_id=" + group_id +
                ", group_name='" + group_name + '\'' +
                ", creator_id=" + creator_id +
                ", count_user=" + count_user +
                ", group_describe='" + group_describe + '\'' +
                ", create_time=" + create_time +
                ", delete_time=" + delete_time +
                ", head_source=" + Arrays.toString(head_source) +
                '}';
    }
}
