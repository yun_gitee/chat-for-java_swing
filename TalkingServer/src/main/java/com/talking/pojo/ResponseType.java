package com.talking.pojo;

public class ResponseType {
    public final static boolean FAIL = false;         //失败
    public final static boolean SUCCESS = true;         //成功

    public final static int PWD_ERROR = 0;         //密码错误
    public final static int BE_LOGIN = 1;         //用户已登录

    public final static int MAIL_USED = 2;         //邮箱已注册
    public final static int VERITY_ERROR = 3;         //验证码错误


}
