package com.talking.pojo;

/**
 * 用来记录发送的类型是什么
 */
public class MessageType {
    public final static int REGISTER = 0;         //注册
    public final static int RETRIEVE_PWD = 1;       //找回密码
    public final static int LOGIN = 2;            //登录

    public final static int GET_FRIENDS = 3;      //获取好友
    public final static int GET_HISTORY_MSG = 4;     //获取历史消息
    public final static int GE_OFFLINE_MSG = 5;      //获取未读消息
    public final static int GET_FRI_MSG = 6;      //获取好友信息
    public final static int CHAT = 7;               //聊天
    public final static int GET_NOTICE = 8;             //通知
    public final static int GET_OFFLINE_NOTICE = 9;      //离线通知
    public final static int CHANGE_USER_MESS = 10;      //更改信息
    public final static int OFFLINE = 11;               //退出系统
    public final static int LOGOUT = 12;                //注销
    public final static int SEARCH_FRI = 13;                //查找好友
    public final static int FRIEND_REQUEST = 14;                //好友添加申请
    public final static int ADD_FRIEND = 15;                //确认添加好友
    public final static int DELETE_FRI = 16;                //删除好友

    public final static int GET_GROUPS = 17;                //获得群列表
    public final static int GET_GROUP_NUMBERS = 18;                //获得群列表
    public final static int CREATE_GROUP = 19;                //获得群列表
    public final static int UPDATE_USER_MSG = 20;                //更新账户资料
    public final static int MAIL_IS_EXITED = 21;                //获取邮箱验证码
    public final static int GET_MAIL_CODE = 22;                //创建邮箱验证码
    public final static int VERIFY_CODE = 23;                //验证邮箱验证码
    public final static int INVITE_NUMS = 24;                //邀请加入群聊
    public final static int NOTICE = 25;                //邀请加入群聊
    public final static int UPDATE_GROUP_LIST = 26;                //更新群列表
    public final static int EXIT_GROUP = 27;                //退出群聊
}
