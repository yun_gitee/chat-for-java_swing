package com.talking.pojo;

import java.io.Serializable;

/**
 * 消息类
 */
public class Message implements Serializable {

    private int messageType;            //消息类型
    private boolean responseType;           //返回值
    private String responseContent;        //返回值内容
    private Object content;             //返回内容

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public boolean isResponseType() {
        return responseType;
    }

    public void setResponseType(boolean responseType) {
        this.responseType = responseType;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getResponseContent() {
        return responseContent;
    }

    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    @Override
    public String toString() {
        return "Message{" +
                "messageType=" + messageType +
                ", responseType=" + responseType +
                ", responseContent=" + responseContent +
                ", content=" + content +
                '}';
    }
}
