package com.talking.service;

import com.talking.controller.ReceiveThread;
import com.talking.dao.UserMapper;
import com.talking.pojo.*;
import com.talking.tools.MybatisTools;
import com.talking.tools.ThreadPool;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class UserService {
    private SqlSession sqlSession;
    private UserMapper userMapper;
    public UserService(){
        sqlSession = MybatisTools.getSqlSession();
        userMapper = sqlSession.getMapper(UserMapper.class);
    }

    //查找某一个好友
    public List<User> getUserByOneService(String userID){

        return userMapper.getUserByOneService(userID);
    }

    //根据用户id与密码查找用户
    public User getUserByIDAndPwdService(User user){
        return userMapper.getUserByIDAndPwd(user);
    }

    //通过ID获取全部信息
    public User getUserMSGByIDService(int user_id){
        return userMapper.getUserMSGByID(user_id);
    }

    //获取好友列表
    public List<User> getFriendListService(int userID){
        return userMapper.getFriendList(userID);
    };

    //获取群成员
    public List<User> getGroupNumbers(int group_id){
        return userMapper.getGroupNumbers(group_id);
    };

    //插入新用户
    public int insertUserService(User user){
        System.out.println("业务层：注册");
        int i = userMapper.insertUser(user);
        sqlSession.commit();
        return i;
    };

    //更新用户信息,动态sql
    public int updateUserMessService(User user){
        int i = userMapper.updateUserMess(user);
        sqlSession.commit();
        return i;
    };

    //聊天转发
    public boolean chatService(ChatMsg chatMsg){
        RecordService recordService = new RecordService();

        try {
            System.out.println("开始消息转发");
        ObjectOutputStream out ;
        Message message = new Message();
        Record record = new Record();
        record.setRecord_content(chatMsg.getContent());
        record.setSrc_user(chatMsg.getSender().getUser_id());
        record.setTarget_account(((User)chatMsg.getReceiver()).getUser_id());
        record.setRecord_type(1);

        //获取线程
        ReceiveThread receiver = ThreadPool.getThread(((User)chatMsg.getReceiver()).getUser_id());
        if (receiver!=null) {
            out = receiver.getOut();
            message.setMessageType(MessageType.CHAT);
            message.setContent(chatMsg);

            out.writeObject(message);
            System.out.println("消息转发完毕");
            record.setIs_read(true);
            }else{
            record.setIs_read(false);
            }
        new RecordService().insertRecord(record);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return false;
    }

    //通知转发
    public boolean noticeService(Notice notice){
        try {
            System.out.println("开始通知转发");
            ObjectOutputStream out;
            Message message = new Message();
            List<User> users = notice.getReceiver();

            ReceiveThread receiver = ThreadPool.getThread(users.get(0).getUser_id());
            if (receiver!=null) {
                out = receiver.getOut();
                message.setMessageType(MessageType.NOTICE);
                message.setContent(notice);

                out.writeObject(message);
            }
            System.out.println("通知转发完毕");

        }catch (Exception e){e.printStackTrace();}

        return false;
    }
}
