package com.talking.service;

import com.talking.controller.ReceiveThread;
import com.talking.dao.GroupMapper;
import com.talking.pojo.*;
import com.talking.tools.MybatisTools;
import com.talking.tools.ThreadPool;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class GroupService {
    private SqlSession sqlSession;
    private GroupMapper groupMapper;

    //一个sqlsession对应一个连接，每次的一个请求，都对应着一个sqlsession，每一次的
    public GroupService(){
        sqlSession = MybatisTools.getSqlSession();
        groupMapper = sqlSession.getMapper(GroupMapper.class);
    }

    //查找单个表
    public Group getSignGroup(String group_id){
        return groupMapper.getSignGroup(group_id);
    }

    //查找已加入群
    public List<Group> getUserGroup(int user_id){
        return groupMapper.getUserGroup(user_id);
    };

//    //插入表
    public int insertNewGroup(Group group){
        int i = groupMapper.insertNewGroup(group);
        sqlSession.commit();
        return i;
    };

    //更新表信息
    public int updateGroupMess(Group group){
        int temp = groupMapper.updateGroupMess(group);
        sqlSession.commit();
        return temp;
    }

    //删除某一个biao
    public int deleteGroup(Group group){
        int i = groupMapper.deleteGroupByID(group.getGroup_id());
        sqlSession.commit();
        return i;
    }



    //获取插入的群ID
    public int getLastID(){
        return groupMapper.getLastInsertID();
    }

    public void setSqlSession(SqlSession sqlSession){
        this.sqlSession = sqlSession;
        groupMapper = sqlSession.getMapper(GroupMapper.class);
    }

    //群聊
    public boolean groupChat(ChatMsg chatMsg, List<User> groupNumber){
        try {
            System.out.println("开始消息转发");
            ObjectOutputStream out ;
            Message message = new Message();

            //获取线程
            for (User user : groupNumber) {
                ReceiveThread receiver = ThreadPool.getThread(user.getUser_id());
                if (receiver!=null&&user.getUser_id()!=((User)chatMsg.getReceiver()).getUser_id()){
                    out = receiver.getOut();
                    message.setMessageType(MessageType.CHAT);
                    message.setContent(chatMsg);

                    out.writeObject(message);
                    System.out.println("消息转发完毕");
                }
            }
            System.out.println("消息转发完毕");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    //邀请加入群聊
    public boolean inviteNums(Notice receiveNotice){
        boolean flag = false;
        Message outMSG = new Message();
        Notice sendNotice = new Notice();
        ObjectOutputStream out;
        List<User> receivers = receiveNotice.getReceiver();

        sendNotice.setSender(receiveNotice.getSender());
        sendNotice.setGroup(receiveNotice.getGroup());
        sendNotice.setUserNotice(false);
        outMSG.setMessageType(MessageType.NOTICE);
        outMSG.setContent(sendNotice);

        try {
            for (User receiver : receivers) {
                ReceiveThread thread = ThreadPool.getThread(receiver.getUser_id());
                if (receiver!=null){
                    out = thread.getOut();
                    out.writeObject(outMSG);
                }
            }
            flag = true;
        }catch (Exception e){
            flag = false;
        }

        return flag;
    }

    //退出群聊
    public boolean exitGroup(Notice notice,ObjectOutputStream out){
        try {
            User user = notice.getSender();
            Group group = notice.getGroup();

            RelationService relation = new RelationService();
            int n = relation.deleteUserRelation(user.getUser_id(), group.getGroup_id(), false);
            if (n == 1) {
                Group tempGroup = this.getSignGroup(String.valueOf(group.getGroup_id()));
                if (tempGroup.getCount_user()==1){
                    int i = this.deleteGroup(group);
                }else {
                    group.setCount_user(tempGroup.getCount_user() - 1);
                    int i = this.updateGroupMess(group);
                }
                Message outMSG = new Message();
                outMSG.setMessageType(MessageType.UPDATE_GROUP_LIST);
                outMSG.setContent(this.getUserGroup(user.getUser_id()));
                outMSG.setResponseType(true);
                out.writeObject(outMSG);
            }
        }catch (Exception e){e.printStackTrace();}


        return true;
    }

}
