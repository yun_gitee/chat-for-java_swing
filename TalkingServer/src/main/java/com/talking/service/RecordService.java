package com.talking.service;

import com.talking.dao.RecordMapper;
import com.talking.dao.RelationMapper;
import com.talking.pojo.Record;
import com.talking.tools.MybatisTools;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class RecordService {
    private SqlSession sqlSession;
    private RecordMapper recordMapper;

    public RecordService(){
        sqlSession = MybatisTools.getSqlSession();
        recordMapper = sqlSession.getMapper(RecordMapper.class);
    }

    //通过用户ID获取所有聊天记录
    public List<Record> getRecordsByUserID(int user_id, boolean is_read){
        return recordMapper.getRecordsByUserID(user_id,is_read);
    };

//    //通过群ID获取单个群聊天记录
//    List<Record> getRecordsByGroupID(int group_id){
//
//    };
//
    //插入一条聊天记录
    public int insertRecord(Record record){
        int temp = recordMapper.insertRecord(record);
        sqlSession.commit();
        return temp;
    };

    //标记聊天记录为已读
    public int updateRecord(int user_id){
        int temp = recordMapper.updateRecord(user_id);
        sqlSession.commit();
        return temp;
    };
}
