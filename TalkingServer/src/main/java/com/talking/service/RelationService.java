package com.talking.service;

import com.talking.dao.RelationMapper;
import com.talking.dao.UserMapper;
import com.talking.tools.MybatisTools;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;

/**
 * 处理关系
 */
public class RelationService {
    private SqlSession sqlSession;
    private RelationMapper relationMapper;

    public RelationService(){
        sqlSession = MybatisTools.getSqlSession();
        relationMapper = sqlSession.getMapper(RelationMapper.class);
    }

    //获取是否存在关系
    public int getUserRelation(int user1_id,int user2_id,int kind){
        int count = relationMapper.getUserRelation(user1_id,user2_id,kind);
        return count;
    };

    //插入好友关系
    public int insertUserRelationService(int user1_id,int user2_id,boolean isUser){
        int count = relationMapper.insertUserRelation(user1_id,user2_id,isUser?1:0);
        sqlSession.commit();
        return count;
    };

    //删除好友关系
    public int deleteUserRelation(int user1_id,int user2_id,boolean isUser){
        int count = relationMapper.deleteUserRelation(user1_id,user2_id,isUser?1:0);
        sqlSession.commit();
        return count;
    };

    public void setSqlSession(SqlSession sqlSession){
        this.sqlSession = sqlSession;
        relationMapper = sqlSession.getMapper(RelationMapper.class);
    }
}
