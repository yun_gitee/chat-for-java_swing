package com.talking.service;

import com.talking.dao.GroupMapper;
import com.talking.dao.MailMapper;
import com.talking.tools.MailCode;
import com.talking.tools.MybatisTools;
import org.apache.ibatis.session.SqlSession;

/**
 * 用于处理邮箱的所有事情
 */
public class MailService {
    private SqlSession sqlSession;
    private MailMapper mailMapper;

    public MailService(){
        sqlSession = MybatisTools.getSqlSession();
        mailMapper = sqlSession.getMapper(MailMapper.class);
    }


    //邮箱是否存在
    public boolean mailIsExited(String user_mail){
        boolean flag = false;
        int count = mailMapper.mailIsExited(user_mail);

        if (count==1)
            flag = true;

        return flag;
    }

    //创建邮箱验证码
    public void createMailCode(String mail){
        //四位数字验证码
        StringBuffer code = new StringBuffer();

        for (int i = 0; i < 4; i++) {
            code.append((int)(Math.random()*10));
        }

        System.out.println("生成的随机验证码是："+code);

        MailCode.addCode(mail,code.toString());

    }

    //获取邮箱验证码
    public String getMailCode(String user_mail){
        String code = MailCode.getCode(user_mail);

        return code;
    }
}
