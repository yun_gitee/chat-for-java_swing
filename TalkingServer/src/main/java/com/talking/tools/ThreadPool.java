package com.talking.tools;

import com.talking.controller.ReceiveThread;

import java.util.Hashtable;

public class ThreadPool {
    private static Hashtable<Integer, ReceiveThread> threads = new Hashtable<>();
    private volatile static int countOfThread = 0;
    private static int maxCount = 200;

    public static Hashtable<Integer, ReceiveThread> getThreads(){
        return threads;
    }

    public static ReceiveThread getThread(Integer id){
        return threads.get(id);
    }

    public static void addThread(Integer id, ReceiveThread receiveThread){
        if (getCountOfThread()<maxCount-1&&getCountOfThread()>=0){
            addCount();
            threads.put(id, receiveThread);
        }
    }
    public static void removeThread(Integer id){
        threads.remove(id);
    }

    private static void addCount(){
        countOfThread++;
    }
    private static int getCountOfThread(){
        return countOfThread;
    }
}
