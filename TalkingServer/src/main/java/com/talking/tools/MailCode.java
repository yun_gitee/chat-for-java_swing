package com.talking.tools;

import java.util.Hashtable;

public class MailCode {
    private static Hashtable<String,String> mailCodePool = new Hashtable<>();

    public static void addCode(String key,String value){
        mailCodePool.put(key,value);
    }

    public static String getCode(String key){
        return mailCodePool.get(key);
    }
}
